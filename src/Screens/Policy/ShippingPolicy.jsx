import React from 'react'
import styles from './Policy.module.css';

export const ShippingPolicy = () => {
  return (
        <div className={styles.policy_container}>
            <div className={styles.policy_title}>
                <h1>Shipping policy</h1>
            </div>

            <div className={styles.policy_body}>
                <div className={styles.policy_body_inner}>
                    UrbanPlants ("we" and "us") is the operator of (https://urbanplants.co.in/)<br />
                    ("Website"). By placing an order through this Website you will be agreeing to<br />
                    the terms below. These are provided to ensure both parties are aware of and<br />
                    agree upon this arrangement to mutually protect and set expectations on our<br />
                    service.<br /><br/>
                    1. General<br />Subject to stock availability. We try to maintain accurate stock counts on our<br />
                    website but from time-to-time there may be a stock discrepancy and we will not<br />
                    be able to fulfill all your items at time of purchase. In this instance, we will<br />
                    fulfill the available products to you, and contact you about whether you would<br />
                    prefer to await restocking of the backordered item or if you would prefer for us<br />
                    to process a refund.<br /><br />
                    2. Shipping Costs<br />Shipping costs are calculated during checkout based on weight, dimensions and<br />
                    destination of the items in the order. Payment for shipping will be collected<br />with the purchase.<br />
                    This price will be the final price for shipping cost to the customer.<br /><br />
                    3. Returns<br />
                        3.1 Return Due To Change Of Mind<br />UrbanPlants will happily accept returns due to change of mind as long as a<br />
                        request to return is received by us within 7 days of receipt of item and are<br />
                        returned to us in original packaging, unused and in resellable condition.<br />
                        Return shipping will be paid at the customers expense and will be required to<br />
                        arrange their own shipping.<br />
                        Once returns are received and accepted, refunds will be processed to store<br />
                        credit for a future purchase. We will notify you once this has been completed<br />through email.<br />
                        (UrbanPlants) will refund the value of the goods returned but will NOT refund<br />
                        the value of any shipping paid.<br /><br />
                        3.2 Warranty Returns<br />UrbanPlants will happily honor any valid warranty claims, provided a claim is<br />
                        submitted within 90 days of receipt of items.<br />Customers will be required to pre-pay the return shipping, however we will<br />
                        reimburse you upon successful warranty claim.<br />Upon return receipt of items for warranty claim, you can expect UrbanPlants to<br />
                        process your warranty claim within 7 days.<br />Once warranty claim is confirmed, you will receive the choice of:<br />
                        (a) refund to your payment method<br />
                        (b) a refund in store credit<br />
                        (c) a replacement item sent to you (if stock is available)<br /><br />
                        4. Delivery Terms<br />4.1 Transit Time Domestically<br />In general, domestic shipments are in transit for 2 - 7 days<br /><br />
                        4.2 Transit time Internationally<br />Generally, orders shipped internationally are in transit for 4 - 22 days. This<br />
                        varies greatly depending on the courier you have selected. We are able to offer<br />
                        a more specific estimate when you are choosing your courier at checkout.<br /><br />
                        4.3 Dispatch Time<br />Orders are usually dispatched within 2 business days of payment of order<br />
                        Our warehouse operates on Monday - Friday during standard business hours, except<br />
                        on national holidays at which time the warehouse will be closed. In these<br />
                        instances, we take steps to ensure shipment delays will be kept to a minimum.<br /><br />
                        4.4 Change Of Delivery Address<br />For change of delivery address requests, we are able to change the address at<br />
                        any time before the order has been dispatched.<br /><br />
                        4.5 P.O. Box Shipping<br />UrbanPlants will ship to P.O. box addresses using postal services only. We are<br />
                        unable to offer couriers services to these locations.<br /><br />4.6 Military Address Shipping<br />
                        We are able to ship to military addresses using USPS. We are unable to offer<br />this service using courier services.<br /><br /><br />
                        4.7 Items Out Of Stock<br />
                        If an item is out of stock, we will wait for the item to be available before<br />
                        dispatching your order. Existing items in the order will be reserved while we<br />await this item.<br /><br />
                        4.8 Delivery Time Exceeded<br />If delivery time has exceeded the forecasted time, please contact us so that we<br />
                        can conduct an investigation.<br /><br />
                        5. Tracking Notifications<br />
                        Upon dispatch, customers will receive a tracking link from which they will be<br />
                        able to follow the progress of their shipment based on the latest updates made<br />
                        available by the shipping provider.<br /><br />
                        6. Parcels Damaged In Transit<br />
                        If you find a parcel is damaged in-transit, if possible, please reject the<br />
                        parcel from the courier and get in touch with our customer service. If the<br />
                        parcel has been delivered without you being present, please contact customer<br />
                        service with next steps.<br /><br />
                        7. Duties &amp; Taxes<br />
                        7.1 Sales Tax<br />
                        Sales tax has already been applied to the price of the goods as displayed on the<br />website<br /><br />
                        7.2 Import Duties &amp; Taxes<br />
                        Import duties and taxes for international shipments may be liable to be paid<br />
                        upon arrival in destination country. This varies by country, and UrbanPlants<br />
                        encourage you to be aware of these potential costs before placing an order with<br />us.<br/>
                        If you refuse to to pay duties and taxes upon arrival at your destination<br />
                        country, the goods will be returned to UrbanPlants at the customers expense, and<br />
                        the customer will receive a refund for the value of goods paid, minus the cost<br />
                        of the return shipping. The cost of the initial shipping will not be refunded.<br /><br />
                        8. Cancellations<br />
                        If you change your mind before you have received your order, we are able to<br />
                        accept cancellations at any time before the order has been dispatched. If an<br />order <br />
                        has already been dispatched, please refer to our refund policy.<br /><br />
                        9. Insurance<br />Parcels are insured for loss and damage up to the value as stated by the<br />courier.<br /><br />
                        9.1 Process for parcel damaged in-transit<br />
                        We will process a refund or replacement as soon as the courier has completed<br />
                        their investigation into the claim.<br /><br />
                        9.2 Process for parcel lost in-transit<br />We will process a refund or replacement as soon as the courier has conducted an<br />
                        investigation and deemed the parcel lost.<br /><br />
                        10. Customer service<br />For all customer service enquiries, please email us at contact@urbanplants.co.in
                </div>
            </div>
        </div>
    )
}
