import React from 'react'
import styles from './Policy.module.css';

export const RefuncPolicy = () => {
  return (
    <div className={styles.policy_container}>
        <div className={styles.policy_title}>
            <h1>Refund policy</h1>
        </div>

        <div className={styles.policy_body}>
            <div className={styles.policy_body_inner}>
                <p>We provide return in case of damaged product being delivered to the customer. Customer is expected to to make unboxing video immediately on the receipt of the parcel. For any damaged products/ dead plants, customer is expected to share unboxing video with us within 48 hours of the receipt. You’ll also need the receipt or proof of purchase. <br /><br />
                To start a return, you can contact us at <a href="mailto:contact@urbanplants.co.in">contact@urbanplants.co.in</a>. If your return is accepted, we’ll send you a return shipping label, as well as instructions on how and where to send your package. Items sent back to us without first requesting a return will not be accepted. Urban Plants has discretion to provide refund in place of replacement of the product.<br /><br />
                You can always contact us for any return question at <a href="mailto:contact@urbanplants.co.in">contact@urbanplants.co.in</a>.</p>
                <br />
                <p><strong>Damages and issues</strong> <br />Please inspect your order upon reception and contact us immediately if the item is defective, damaged or if you receive the wrong item, so that we can evaluate the issue and make it right.</p>
                <br />
                <p><strong>Exceptions / non-returnable items</strong> <br />Certain types of items cannot be returned, like perishable goods (such as food, flowers, or plants), custom products (such as special orders or personalized items), and personal care goods (such as beauty products). We also do not accept returns for hazardous materials, flammable liquids, or gases. Please get in touch if you have questions or concerns about your specific item. <br /><br />
                Unfortunately, we cannot accept returns on sale items or gift cards.</p>
                <br />
                <p><strong>Exchanges</strong> <br />The fastest way to ensure you get what you want is to return the item you have, and once the return is accepted, make a separate purchase for the new item.</p>
                <br />
                <p><strong>Refunds</strong> <br />We will notify you once we’ve received and inspected your return, and let you know if the refund was approved or not. If approved, you’ll be automatically refunded on your original payment method. Please remember it can take some time for your bank or credit card company to process and post the refund too.</p>
            </div>
        </div>
    </div>
  )
}
