import React from 'react';
import style from "./Blog.module.css";
import CoverImage from "../../../assets/images/homePage/blog-1.jpg"
import BlogImage2 from "../../../assets/images/homePage/blog-2.jpg"
import BlogImage3 from "../../../assets/images/homePage/blog-3.jpg"
import BlogImage4 from "../../../assets/images/homePage/blog-4.jpg"
const Blog = () => {

    return (
        <div className={style.blog_page}>
            <div className={style.display_title}>
                Blog
            </div>
            <div className={style.blog_container}>
                <div className={style.blog_cover_image}>
                    <img src={CoverImage} alt={"coverImage"}/>
                    <div className={style.blog_text_heading}>Choosing the right sprinkler for your garden</div>
                    <div className={style.blog_footer}>
                        JUL 11, 2021 by Samin Rizvi
                    </div>
                </div>
                <div className={style.blog_content_section}>
                    <div className={style.row_container}>
                        <div className={style.blog_image}>
                            <img src={BlogImage2} alt={"BlogImage"} />
                        </div>
                        <div className={style.blog_text}>
                            How to Grow Aromatic Rajnigandha?
                            <br />
                            <div className={style.blog_footer}>
                                JUL 11, 2021 by Samin Rizvi
                            </div>
                        </div>

                    </div>
                    <div className={style.display_blog}>
                        <div className={style.blog_image}>
                            <img src={BlogImage3} alt={"BlogImage"} />
                        </div>
                        <div className={style.blog_text}>
                            How to Grow Aromatic Rajnigandha?

                            <div className={style.blog_footer}>
                                JUL 11, 2021 by Samin Rizvi
                            </div>
                        </div>
                    </div>
                    <div className={style.row_container}>
                        <div className={style.blog_image}>
                            <img src={BlogImage4} alt={"BlogImage"} />
                        </div>
                        <div className={style.blog_text}>
                            How to Grow Aromatic Rajnigandha?
                            <div className={style.blog_footer}>
                                JUL 11, 2021 by Samin Rizvi
                            </div>
                        </div>
                    </div>
                </div >
            </div>
        </div>
    )
}

export default Blog;