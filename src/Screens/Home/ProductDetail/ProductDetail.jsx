import React from "react";
import style from "./ProductDetail.module.css";
import Slider from "../../../Components/Slider/Slider";
import { useNavigate } from "react-router-dom";

const ProductDetail = (props) => {
    const navigate = useNavigate();
    return (
        <>
            <div className={style.main_product}>
                <div className={style.carousel}>
                    <div className={style.mobile_card}>
                        <div className={style.product_title}>
                            {props.productName}
                        </div>
                        <div className={style.product_category}>
                            <u>{props.productSellerName}</u>
                        </div>
                        <div className={style.code_section}>
                            SKU : {props.hcodeContainer}
                        </div>
                    </div>
                    <div className={style.product_details_images}>
                        { 
                            Array.isArray(props.images) ? <Slider isHome={true} images={props.images} /> : props.images
                        }
                    </div>
                </div>
                <div className={style.product_details_container}>
                    <div className={style.web_card}>
                        <div className={style.product_title}>
                            {props.productName}
                        </div>
                        <div className={style.product_category}>
                            <u>{props.productSellerName}</u>
                        </div>
                        <div className={style.code_section}>
                            SKU : {props.hcodeContainer}
                        </div>
                    </div>
                    <div className={style.price_container_product}>
                        <div className={style.price}>
                            ₹ {props.productPrice !== undefined? props.productPrice: 'NA'}
                        </div>
                        <div className={style.old_price}>
                        ₹ {props.productActualPrice !== undefined ? props.productActualPrice: 'NA'}
                        </div>
                        <div className={style.discount}>
                            Save {props.saveProductPrice !== undefined ? props.saveProductPrice: 'NA'}
                        </div>
                    </div>
                    
                    <div className={style.shipping_checkout}>
                        <u>Shipping</u> calculated at checkout
                    </div>
                    {props.sizeContainer()}
                    {props.colorContainer()}
                    {props.addQuantity()}
                    <div className={style.product_view_more} onClick={(e)=>(navigate(`/product/${'100031'}`))}>
                        view more
                    </div>
                </div>
            </div>
            <div className={style.display_btns_container}>
                <div className={style.add_cart_btn}>
                    <button onClick={(e)=>props.handleAddToCart(e)} className={style.add_cart_btn}>ADD TO CART</button>
                </div>
                <div className={style.buy_now_btn}>
                    {props.colorSize && props.productVariant !== undefined ? 
                        <button onClick={(e)=>props.handleBuyNow(e)}>BUY NOW</button>
                    : 
                        <button>OUT OF STOCK</button>
                    } 
                </div>
            </div>
        </>

    )
}


export default ProductDetail;