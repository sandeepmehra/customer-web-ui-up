import React from "react";
import style from "./Home.module.css"
import Blog from "./Blog/Blog";
import ProductList from "../Product/ProductList";
import WebSearchBar from "../../Components/SearchBox/WebSearchBox";
import MobileSearchBar from "../../Components/SearchBox/MobileSearchBox";
import Loader from "../../Components/Loader/Loader";
import CategoryHome from "../Category/CategoryHome";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setError } from "../../store/reducer/errorReducer";
import { resHomeData } from "../../store/reducer/homeReducer";
import Product from "../Product/Product";

const HomePage = () => {
    const { plantsData, potsData, toolsData, isLoading, isError, error } = useSelector((state) => state.homeSlice);
    const dispatch = useDispatch();
    const productId = '100031';
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resHomeData(productId));
    },[dispatch]);
    return (
        <div className={style.home_container}>
            { isLoading ? <Loader /> : 
                <>
                    <MobileSearchBar />
                    <div className={style.home_section}>
                        <div className={style.cover_image}>
                            <WebSearchBar />
                        </div>
                        <div className={style.page_container}>
                            <CategoryHome isHome={true} />
                            <hr className={style.hr} />
                            {/* Display Product Sadabhara  */}
                            <Product isHome={true}/>
                            {(plantsData.length > 0) && <hr className={style.hr} />}
                            {/* Display Plants */}
                            {(plantsData.length > 0) && 
                                <ProductList 
                                    productTitle="Plants" 
                                    products_data_home={plantsData} 
                                    showCartIcon={true} 
                                    showViewAll={true}
                                />
                            }
                            
                            {(potsData.length > 0) && <hr className={style.hr} />}
                            {/* Dipslay Pots */}
                            {(potsData.length > 0) && 
                                <ProductList 
                                    productTitle="Pots" 
                                    products_data_home={potsData} 
                                    showCartIcon={true} 
                                    showViewAll={true} 
                                />
                            }
                            {(toolsData.length > 0) && <hr className={style.hr} />}
                            {/* Dipslay Tools */}
                            {(toolsData.length > 0) && 
                                <ProductList 
                                    productTitle="Accessories" 
                                    products_data_home={toolsData} 
                                    showCartIcon={true} 
                                    showViewAll={true} 
                                />
                            }
                            <hr className={style.hr} />
                            {/* Display Blog Section */}
                            <Blog />
                            <hr className={style.hr} /> 
                        </div>   
                    </div>
                </>
            }
        </div>
    )
}

export default HomePage;