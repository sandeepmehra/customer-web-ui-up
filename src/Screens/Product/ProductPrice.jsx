import React from 'react';
import style from "./ProductPrice.module.css";
import { Label, Title } from "../../Components/StyleComponents/StyledComponents";


const ProductPrice = ({ showOrderSummary = false, totalAmount, cartDiscountTotal }) => {
    return (
        <div className={style.cart_product_value}>
            {showOrderSummary ? (
                <div className={style.outline_order_summary}>
                    <div className={style.order_summary}>
                        <Title>Order Summary</Title>
                    </div>
                </div>
            ) : ''}

            <div className={style.outline}>
                
                <div className={style.price_details_name}>Price Details</div>
                <div className={style.price_details}>
                    <div className={style.price_details_card}>
                        {/* Product Price */}
                        <div className={style.row_container}>
                            <div className={style.price_label}>
                                <Label>Price</Label>
                            </div>
                            <div className={style.product_price}>
                                <Label>{Number(totalAmount + cartDiscountTotal) ? "₹" : ''}  {totalAmount + cartDiscountTotal} </Label>
                            </div>
                        </div>
                        {/* Product Discount Price */}
                        <div className={style.row_container}>
                            <div className={style.price_label}>
                                <Label>Discount</Label>
                            </div>
                            <div className={style.product_price}>
                                <Label>{Number(cartDiscountTotal !== '' && cartDiscountTotal !== undefined ? cartDiscountTotal: '0.00') ? "₹" : ''}  {cartDiscountTotal !== '' && cartDiscountTotal !== undefined ? cartDiscountTotal: '0.00'} </Label>
                            </div>
                        </div>
                        {/* Product Delivery Charges */}
                        <div className={style.row_container}>
                            <div className={style.price_label}>
                                <Label>Delivery Charges</Label>
                            </div>
                            <div className={style.product_price}>
                                <div className={style.cal_step}>Calculated at next step</div>
                            </div>
                        </div>
                        {/* Product Total Amount */}
                    </div>
                    <div className={`${style.row_container} ${style.common_style}`}>
                        {/* {console.log(item)} */}
                        <div className={style.price_label}>
                            <div className={style.totalAmount}>Total Amount</div>
                        </div>
                        <div className={style.product_price}>
                            <div className={style.product_price_style}>{totalAmount > 0 ? "₹" : ''}  {totalAmount} </div>
                        </div>
                    </div>
                    {/* {displaySave()} */}
                    {cartDiscountTotal !== '' && cartDiscountTotal !== undefined ? ( 
                    <div className={style.display_save}>
                        You will save ₹ {cartDiscountTotal} on this order
                    </div>): null}
                </div>
            </div>
            <div className={style.row_container}>
                <div className={style.discount_code_card}>
                    <input placeholder="DISCOUNT CODE" />
                </div>
                <div className={style.apply_btn}>
                    <button>Apply</button>
                </div>
            </div>
        </div>
    )
}

export default ProductPrice;