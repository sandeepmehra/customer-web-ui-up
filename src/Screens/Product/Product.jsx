import React, { useEffect, useState} from 'react';
import style from './Product.module.css';
import { AiOutlineCheckCircle, AiOutlinePlus, AiOutlineMinus, AiFillStar } from 'react-icons/ai';
import { BsFillCircleFill, BsGlobe } from 'react-icons/bs';
import { VscLock } from 'react-icons/vsc';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ImLeaf } from 'react-icons/im';
import Slider from '../../Components/Slider/Slider';
import Loader from '../../Components/Loader/Loader';
import { getProductsByIdDetail, ResetProductDetail } from '../../store/reducer/productReducer';
import { createCart, SetBuyNow } from '../../store/reducer/cartReducer';
import { setError } from '../../store/reducer/errorReducer';
import ProductDetail from '../Home/ProductDetail/ProductDetail';
import AlsoLikeProduct from './AlsoLikeProduct';
import TabsCard from '../../Components/Tabs/Tabs';


const Product = (props) => {
    const {productId} = useParams();
    const {product, productVariants, isError, isLoading, error} = useSelector(state => state.productSlice);
    const {isAuthenticated} = useSelector((state)=> state.auth);
    const cart = useSelector(state => state.cartSlice);
    const location = useLocation();
    const [quantity, setQuantity] = useState(1);
    let [color, setColor] = useState('');
    let [size, setSize] = useState('');
    const [currentColors, setCurrentColors] = useState([]);
    const [currentSizes, setCurrentSizes] = useState([]);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    
    useEffect(()=>{
        dispatch(setError(isError || cart.isError ? error || cart.error : null));
    }, [isError, error, dispatch, cart.isError, cart.error]);  
    useEffect(()=> {
        if(!(props.isHome)){
            dispatch(getProductsByIdDetail(productId));
        }
        return ()=>{
            setColor('');
            setSize('');
            dispatch(ResetProductDetail());
        }
    },[dispatch, productId, props.isHome]);
    useEffect(()=>{
        if(isLoading && color !== '' && size !== ''){
            setColor('');
            setSize('');
        }
    }, [color, isLoading, size]);
    useEffect(()=> {
        if(product !== undefined && productVariants !== undefined && product.colors_available !== undefined && (color === '' || size === '') ){
            setColor(productVariants[Object.keys(productVariants)[0]].color);
            setSize(productVariants[Object.keys(productVariants)[0]].measurements);
            
            let sub_arr = Object.keys(product.sizes_available[productVariants[Object.keys(productVariants)[0]].measurements]);
            setCurrentSizes(Object.keys(product.sizes_available).map(element => [true, element]));
            setCurrentColors(Object.keys(product.colors_available).map(element => [sub_arr.includes(element), element]));
        }
    },[product, productVariants, color, size]);
    if(isLoading){
        return <Loader />
    }
    let colorSize = color !== '' && size !== '';
    let productVariant = productVariants !== undefined && colorSize && productVariants[product?.colors_available[color][size]];
    const handleClickColor = (c) =>{
        setColor(c);

        let sub_arr = Object.keys(product.colors_available[c]);
        
        setCurrentColors(Object.keys(product.colors_available).map(element => [true, element]));

        setCurrentSizes(Object.keys(product.sizes_available).map(element => [sub_arr.includes(element), element]));
    }
    const handleClickSize = (s) =>{
        setSize(s);
        
        let sub_arr = Object.keys(product.sizes_available[s]);
        
        setCurrentSizes(Object.keys(product.sizes_available).map(element => [true, element]));

        setCurrentColors(Object.keys(product.colors_available).map(element => [sub_arr.includes(element), element]));
    }
    const handleAddToCart = async(e) => {
        e.preventDefault();
        let productVariantId = product.sizes_available[productVariant.measurements][productVariant.color];
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(createCart({productVariantId, quantity}));   
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    }
    const handleBuyNow = async(e) => {
        e.preventDefault();
        let productVariantId = product.sizes_available[productVariant.measurements][productVariant.color];
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(SetBuyNow({productVariantId, quantity, total_price:Number(productVariant.price) * Number(quantity), discount_price:Number(productVariant.discounted_price) }));
            navigate('/checkout');
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    }
    const priceContainer = () => (
        <div className={style.price_container_product}>
            <div className={style.price}>
                ₹ {colorSize && productVariant !== undefined? productVariant.price: 'NA'}
            </div>
            <div className={style.old_price}>
                ₹ {colorSize && productVariant !== undefined ?`${(Number(productVariant.price) + Number(productVariant.discounted_price))}`: 'NA'}
            </div>
            <div className={style.discount}>
                Save {colorSize && productVariant !== undefined ?`${(Number(productVariant.discounted_price)/(Number(productVariant.price) + Number(productVariant.discounted_price)) * 100).toFixed(2)}%`: 'NA'}
            </div>
        </div>
    )
    const shopContainer = () => (
        <div className={style.category_section}>
            <div className={style.category_name}>{colorSize && productVariant !== undefined ? product.shop_name : 'NA'}</div>
            <div className={style.verified}>
                Verified
            </div>
        </div>
    )
    const hcodeContainer = () => (
        <div className={style.code_section}>
            SKU : {colorSize && productVariant !== undefined? productVariant.sku: 'NA'} 
        </div>
    )
    const ratingContainer = () => (
        <div className={style.rating_section}>
            <div className={style.ratings}>
                <div>4.4</div>
                <AiFillStar fill='#ffff' color='#f3f3f3' />
            </div>
            <div className={style.rating_review}>
                1200 Ratings 120 Reviews
            </div>
        </div>
    )
    const shippingTextContainer = () => (
        <div className={style.shipping_text_card}>
            <u>Shipping</u> calculated at checkout
        </div>
    )
    const sizeContainer = () => (
        <div className={style.size_container}>
            <span className={style.variant_heading}>Variants</span>
            <div className={style.color_container}>
            { 
                colorSize && currentSizes.map((s, i) => (
                    
                        <button
                            className={s[0] ? (s[1] === size ? `${style.sizes} ${style.border_line}` :`${style.sizes}`): `${style.sizes} ${style.border_dotted}`}
                            key={i}
                            onClick={(e)=>handleClickSize(s[1])}
                        >
                            {s[1]}
                        </button>
                    
                    ))
            }
            </div>
        </div>
    )
    const colorContainer = () => (
        <div className={style.size_container}>
            <span className={style.color_heading}>Color</span>
            <div className={style.color_container}>
                {
                    colorSize && currentColors.map((c, i) => (
                        <div key={i} className={c[0] ? (c[1] === color? `${style.border_line} ${style.outer_layer}`: `${style.outer_layer}`): `${style.border_dotted} ${style.outer_layer}`}>
                        <div 
                            key={i}
                            className={style.circle_button} 
                            style={{"backgroundColor": `#${c[1]}`}}
                            onClick={(e)=>handleClickColor(c[1])} 
                        />
                        </div>
                    ))
                }
            </div>
        </div>
    )
    const availabilityContainer = () => (
        <>
            <label className='margin-top'> Check Availability</label>
            <div className={style.availability_container}>
                <input placeholder={'Enter your pincode'} className={style.input_check_availability}/>
                <div className={style.btn_check_availability}>
                    <button>CHECK</button>
                </div>
            </div>
        </>

    )
    const handleMinus = () => {
        if(quantity > 1) setQuantity(quantity - 1)
    }
    const addQuantity = () => (
        <div className={style.product_count}>
            <span className={style.minus} onClick={handleMinus}>
                <AiOutlineMinus fill='#1f553e' />
            </span>
            <span className={style.counter}>{quantity}</span>
            <span className={style.plus} onClick={()=>setQuantity(quantity + 1)}>
                <AiOutlinePlus fill='#1f553e' />
            </span>
        </div>
    )
    const displayinfo = () => (
        <div className={style.quality_card}>
            <div className={style.quality_card}>
                <label><BsGlobe /> Free shipping</label>
            </div>
            <div className={style.quality_card}>
                <label><AiOutlineCheckCircle /> Brand Quality</label>
            </div>
            <div className={style.quality_card}>
                <label><ImLeaf /> Sustainably made </label>
            </div>
            <div className={style.quality_card}>
                <label><VscLock /> Secure payments</label>
            </div>
            <div className={style.display_quantity}>
                <label><BsFillCircleFill size={8} /> Only 10 items in stock</label>
            </div>

        </div>
    )

    const isValidImages = colorSize && productVariant !== undefined  && productVariant.images !== undefined && productVariant.images.length !== 0;
   
    if(props.isHome){
        return (
            <ProductDetail 
                productName={colorSize && productVariant !== undefined ? product.product_name : 'NA'}
                productSellerName={colorSize && productVariant !== undefined ? product.shop_name : 'NA'}
                hcodeContainer={colorSize && productVariant !== undefined? productVariant.sku: 'NA'}
                productPrice={colorSize && productVariant !== undefined? productVariant.price: 'NA'}
                productActualPrice={colorSize && productVariant !== undefined ?`${(Number(productVariant.price) + Number(productVariant.discounted_price))}`: 'NA'}
                saveProductPrice={colorSize && productVariant !== undefined ?`${(Number(productVariant.discounted_price)/(Number(productVariant.price) + Number(productVariant.discounted_price)) * 100).toFixed(2)}%`: 'NA'}
                priceContainer={priceContainer}
                colorContainer={colorContainer}
                sizeContainer={sizeContainer}
                addQuantity={addQuantity}
                images={isValidImages ? productVariant.images : 'Images not Found'}
                handleAddToCart={handleAddToCart}
                colorSize={colorSize}
                productVariant={productVariant}
                handleBuyNow={handleBuyNow}
            />
        )
    }
   
    return (
        <div className={style.product_details_container}>
            <div className={style.product_detail_container}>
                <div className={style.details_section} >
                    <div className={style.product_images_container}>
                        <div className={style.product_card}>
                            <div className={style.product_cover_image}>  
                            {
                               isValidImages ? <Slider images={productVariant.images} /> : 'Images not Found' 
                            }    
                            </div>
                        </div>
                        <div className={style.webview_button}>
                            <div className={style.add_cart_btn}>
                                <button onClick={(e)=>handleAddToCart(e)}>ADD TO CART</button>
                            </div>
                            <div className={style.buy_now_btn}>
                            {colorSize && productVariant !== undefined ? 
                                <button onClick={(e)=>handleBuyNow(e)}>BUY NOW</button>
                                : 
                                <button>OUT OF STOCK</button>
                            } 
                            </div> 
                        </div>
                    </div>
                    <div className={style.product_details}>
                        <div className={style.product_name}>{colorSize && productVariant !== undefined ? product.product_name : 'NA'}</div>
                        {/* Rating container */}
                        {ratingContainer()}
                        {/* Category Conatiner */}
                        {shopContainer()}
                        {/* HSN code Container  */}
                        {hcodeContainer()}
                        {/* Price container */}
                        {priceContainer()}
                        {/* Shipping text container */}
                        {shippingTextContainer()}
                        {/* Size Container */}
                        {sizeContainer()}
                        {/* Color Section */}
                        {colorContainer()}

                        {/* Add quantity */}
                        {addQuantity()}
                        {/* availability conatainer */}
                        {availabilityContainer()}
                    </div>
                    <div className={style.display_info}>
                        {displayinfo()}
                    </div>
                    <div className={style.mobileview_button}>
                        <div className={style.add_cart_btn}>
                            <button onClick={(e)=>handleAddToCart(e)}>ADD TO CART</button>
                        </div>    
                        <div className={style.buy_now_btn}>
                            {colorSize && productVariant !== undefined ? 
                                <button onClick={(e)=>handleBuyNow(e)}>BUY NOW</button>
                                : 
                                <button >OUT OF STOCK</button>
                            }   
                        </div>
                    </div>
                </div>
                <TabsCard 
                    productDescription={colorSize && productVariant !== undefined ? product.description : 'NA'}
                />
                <div className={style.alsoLikeContainer}>
                    <AlsoLikeProduct />
                </div>
                
            </div>
            
        </div>
    )
   
}

export default Product;




