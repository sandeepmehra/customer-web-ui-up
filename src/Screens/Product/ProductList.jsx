import React, { useEffect } from 'react';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import style from "./ProductList.module.css";
import { Title } from "../../Components/StyleComponents/StyledComponents";
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { createCart } from '../../store/reducer/cartReducer';
import { setError } from '../../store/reducer/errorReducer';

const ProductList = ({ productTitle, products_data_home}) => {
    const {isAuthenticated} = useSelector((state)=> state.auth);
    const {isError, error} = useSelector(state => state.cartSlice);
    const location = useLocation();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const addToCart = async(e, productVariantId) => {
        e.preventDefault();
        e.stopPropagation();
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(createCart({productVariantId, quantity: 1}));
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    };
    // const clickHandle = (productId) => {
    //     navigate(`/product/${productId}`);
    //     onClick={()=>clickHandle(product.id)}
    // }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);  
    return (
        <div className={style.product_list_page}>
            <div className={style.title_section}>
                <div className={style.display_title}>
                    <Title>{productTitle}</Title>
                </div>
                <div className={style.display_viewall}>
                    <Link to={`/category/${productTitle}`}>View All</Link>
                </div>
            </div>
            <div className={style.list_scroll}>
                <div className={`${style.home_product_section}`}>
                    {products_data_home.map((product, i) => (
                    <Link to={`/product/${product.id}`} className={style.card_container} key={i} >
                        <div className={style.product_image_container}>
                            <img src={product.image} alt={"product"}/>
                        </div>
                        <div className={style.card_content}>
                            <div className={style.card_price_container}>
                                <label className={style.price}> ₹{product.price}</label>
                                <label className={style.product_old_price}> <strike>₹{Number(product.price) + Number(product.discounted_price)} </strike></label>
                            </div>
                            <label className={style.card_discount}>
                                Save {(Number(product.discounted_price)/(Number(product.price) + Number(product.discounted_price)) * 100).toFixed(2)}%
                            </label>
                            <div className={style.product_name}>
                                <p>{product.product_name}</p>
                            </div>
                            <div className={style.product_shop_name}>
                                {product.shop_name}
                            </div>
                            <div className={style.card_btns} onClick={(e)=>addToCart(e, product.product_variant_id)}>
                                <button className={style.cart_btn}>
                                    <ShoppingCartOutlinedIcon />
                                </button>
                            </div>
                        </div>
                    </Link>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default ProductList;