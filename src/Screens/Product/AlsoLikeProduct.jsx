import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import Card from '../../Components/Card/Card';
import style from './Product.module.css';
import { Header } from '../../Components/StyleComponents/StyledComponents';
import { addToWishlistItem, removeWishlistItem } from '../../store/reducer/userReducer';

const AlsoLikeProduct = (props) => {
    const { categoryProduct } = useSelector((state) => state.categorySlice);
    const dispatch = useDispatch();
    const handleWishlist = (e, variantId, category)=>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(addToWishlistItem({id: variantId, category: category, isProductDetail: true}));
    }
    const handleRemoveWishlist = (e, variantId, category)=>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(removeWishlistItem({id: variantId, category: category, isProductDetail: true}));
    }
    if(props.isCart){
        return  (
            <div className={style.you_may_also_like}>
                <div>
                { (props.data !== null && props.data !== undefined && props.data.length !== 0 && props.data.detail_list !== null && props.data.detail_list !== undefined && props.data.detail_list.length !== 0 )? 
                    <div>
                        <Header className={style.header_name}>You may also like</Header>
                            <div className={style.products_section}>
                                {props.data.detail_list.map((product, i) => (
                                    <Card 
                                        product={product} 
                                        key={i} 
                                        index={i} 
                                        handleWishlist={handleWishlist}
                                        handleRemoveWishlist={handleRemoveWishlist}
                                    />
                                ))}
                            </div>
                    </div>: null
                }
                </div>
            </div>
        )
    }
    return (
        <div className={style.you_may_also_like}>
            <div>
            { (categoryProduct !== null && categoryProduct !== undefined && categoryProduct.length !== 0 && categoryProduct.detail_list !== null && categoryProduct.detail_list !== undefined && categoryProduct.detail_list.length !== 0 )? 
                <div>
                    <Header className={style.header_name}>You may also like</Header>
                        <div className={style.products_section}>
                            {categoryProduct.detail_list.map((product, i) => (
                                <Card 
                                    product={product} 
                                    key={i} 
                                    index={i} 
                                    handleWishlist={handleWishlist}
                                    handleRemoveWishlist={handleRemoveWishlist}
                                />
                            ))}
                        </div>
                </div>: null
            }
            </div>
        </div>
    )
}

export default AlsoLikeProduct;