import ImagesGallery from "./ImagesGallery";
import style from './Review.module.css';
import { useState } from "react";
import PlusImages from "../../assets/images/review/camera_plus.png";

const PreviewMultipleImages=({setImages, images})=>{
    const [imagesUrl, setImagesUrl] = useState([]);
    const handleMultipleImages = (e)=>{
        e.preventDefault();
        const selectedImageFiles = [...images];
        const selectedFiles = [...imagesUrl];
        [...e.target.files].forEach((file)=>{
            selectedImageFiles.push(file);
            selectedFiles.push(URL.createObjectURL(file));
        });
        setImagesUrl([...selectedFiles]);
        setImages(selectedImageFiles);
    }
    const onHandleDelete = async(e, url, index)=>{
        e.preventDefault();
        const removedImagesUrl = imagesUrl.filter((imageUrl, i)=> { return imageUrl !== url && i !== index });
        const removeImages = images.filter((_, i)=>i !== index);
        setImagesUrl([...removedImagesUrl]);
        setImages([...removeImages]);
    }
    return(
    <>
        <label htmlFor="file-upload" className={style.file_upload}>
            <div className={`${style.dashed_outline}`}>
                <input type="file" multiple id="file-upload" onChange={handleMultipleImages} accept="image/*"/>
                <img src={PlusImages} alt={'upload_images'} className={style.bscamera} />
            </div>
        </label>
        <ImagesGallery imagesUrl={imagesUrl} onHandleDelete={onHandleDelete}/>
    </>
    )
}
export default PreviewMultipleImages;