import { AiFillStar } from "react-icons/ai";
import style from "./StarRating.module.css";

export const StarRating = ({rating, hover, setRating, setHover}) => { 
    return (
      <div className={style.star_rating}>
        {[...Array(5)].map((_, index) => {
          index += 1;
          return (
            <AiFillStar
                key={index}
                className={index <= (hover || rating) ? `${style.on}` : `${style.off}`}
                onClick={() => setRating(index)}
                onMouseEnter={() => setHover(index)}
                onMouseLeave={() => setHover(rating)}
                size={28}
            />
          );
        })}
      </div>
    );
  };