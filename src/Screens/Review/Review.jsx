import React from 'react';
import { Header, Label, Title } from "../../Components/StyleComponents/StyledComponents";
import { reviewInfo } from '../../Components/data/review';
import { ReviewChild } from './ReviewChild';
import style from './Review.module.css';

const ReviewPage = () => {
    return (
        <div className='main-container'>
            <div className={`${style.review_page}`}>
                <div className={style.row_container}>
                    <div className={style.display_info}> 
                        <div className={style.display_card}>
                            <div style={{width:"80%", margin: "auto", marginTop: "1rem"}}>
                                <Title>What makes a good review</Title>
                            </div>
                            {reviewInfo.map((data, i) => (
                                <div key={i}>
                                    <hr />
                                    <div style={{width:"80%", margin: "auto"}}>
                                        <div className={style.review_header}>{data.header}</div>
                                        <div className={style.review_label}>{data.label}</div>
                                    </div>
                                </div>
                            ))}
                            <br />
                        </div>
                    </div>
                    <div className={style.submit_review}>
                        <ReviewChild />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReviewPage;