import style from './Review.module.css';
import { TiDelete } from "react-icons/ti";

function ImagesGallery({imagesUrl, onHandleDelete}){
    return(
        <div className={style.row}>
        {
            imagesUrl.map((url, index)=>{
                return (
                    <div className={style.col} key={index}>
                        <TiDelete onClick={(e)=>onHandleDelete(e, url, index)} className={style.remove_icon} />
                        <div className={style.selected_image_card}>
                            <img src={url} alt={'images'}/>
                        </div>
                    </div>
                )
            })
        }
        </div>
    )
}

export default ImagesGallery;