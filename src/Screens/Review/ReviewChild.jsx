import React, { useEffect, useState } from 'react';
import { Label, Title } from "../../Components/StyleComponents/StyledComponents";
import { Link, useNavigate, useParams } from 'react-router-dom';
import { StarRating } from './StarRating';
import { useDispatch, useSelector } from 'react-redux';
import style from './Review.module.css';
import { AiOutlineLeft } from 'react-icons/ai';
import PreviewMultipleImages from './Images';
import { createRating, createRatingOrImages } from '../../store/reducer/ratingReducer';
import { toast } from 'react-toastify';
import { setError } from '../../store/reducer/errorReducer';

export const ReviewChild = () => {
    const {isError, isLoading, error} = useSelector(state => state.ratingSlice);
    const {orderItemId} = useParams();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [rating, setRating] = useState(0);
    const [hover, setHover] = useState(0);
    const [review, setReview] = useState('');
    const [images, setImages] = useState([]);
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    const submitReview = async(e) => {
        e.preventDefault();
        const MAX_FILE_SIZE = 10240 // 10MB
        let size=0;
        let formData = new FormData();
        for(let i = 0; i < images.length; i++) {
            formData.append("files", images[i]);
        }
        formData.append("review", review);
        formData.append("rating", String(rating));
        images.forEach((image)=>{
            return size += image.size
        })
        const fileSizeKiloBytes = size / 1024
        if(fileSizeKiloBytes > MAX_FILE_SIZE){
            toast.error("File size is greater than maximum limit");
            return
        }
        if(images.length > 3){
            toast.error("Files are greater than maximum limit");
            return
        }
        try {
            if(orderItemId !== '' && orderItemId !== undefined && images !== [] && review !== '' && rating !== ''){
                const reviewCreated = await dispatch(createRatingOrImages({orderItemId: String(orderItemId), formData})).unwrap();
                if(reviewCreated.status === 201){
                    navigate("/orders");
                }
            }else if(orderItemId !== '' && review !== '' && rating !== ''){ 
                const reviewCreated = await dispatch(createRating({orderItemId: String(orderItemId), review, rating: String(rating)})).unwrap();
                if(reviewCreated.status === 201){
                    navigate("/orders");
                }
            }else{
                toast.error(`${rating !== [] ? 'Rating, ': ''}${images !== [] ? 'Images, ': ''}${review !== [] ? 'Review, ': ''} are empty`);
            }
          } catch (rejectedValueOrSerializedError) {
            toast.error('Review is not added. Try Again');
          }
        
    }
    return (
        <div className={style.child_card}>
            <div style={{width:"80%", margin: "auto", marginTop: "1rem"}}>
                <label className={style.display_web_title}><Title>RATINGS & REVIEWS</Title></label>
                <div className={style.bck_page_icon}>
                    <Link to='/orders'>
                        <AiOutlineLeft />
                    </Link>
                    <label className={style.display_mobile_title}><b>Create a review </b></label>
                </div>
                
                <br />
                <label>Rate this Product</label>
                <StarRating rating={rating} hover={hover} setRating={setRating} setHover={setHover} />
            </div>
            <hr />
            <div style={{width:"80%", margin: "auto"}}>
                <Title>Review this Product</Title>
                <br />
                <Label>Add a photo</Label>
                <PreviewMultipleImages images={images} setImages={setImages}/>
                <br />
                Review (1500)
                <div className={`${style.input_container} ${style.review_body}`}>
                    <textarea 
                        type={"text"}
                        value={review}
                        onChange={(e)=>setReview(e.target.value)}
                        placeholder={'Write your review here'} 
                        rows="4" 
                    />
                </div>
            </div>
            <hr />
            <div className={style.review_btn} style={{margin:'auto', marginBottom: "1rem"}}>
                <button onClick={(e)=>submitReview(e)}>{!isLoading ? 'Submit Review' : '...'}</button>
            </div>

    </div>
  )
}
