import React from 'react'
import { useNavigate } from 'react-router-dom';
import style from './NotFound.module.css';

export const Suspended = () => {
  const navigate = useNavigate();
  return (
        <div className={style.container}>
        <div className={style.oops}>Oops!</div>

        <div className={style.suspend_heading}>Your account has been suspended. Please contact the admin.</div>

        <div className={style.paragraph}><u>contact@urbanplants.co.in</u></div>

        <button className={style.backTohome} onClick={()=>navigate("/")}>
            Back to home
        </button>
        </div>
    )
}
