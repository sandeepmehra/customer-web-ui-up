import Cookies from 'js-cookie';
import { useCallback, useEffect } from 'react';
import {
    useSelector,
    useDispatch
} from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ResetAddressError } from '../../store/reducer/addressReducer';
import { ResetAuth, ResetError } from '../../store/reducer/authReducer';
import { ResetCartError } from '../../store/reducer/cartReducer';
import { ResetCategoryError } from '../../store/reducer/categoryReducer';
import { ResetComplaintError } from '../../store/reducer/complaintReducer';
import { hideError } from '../../store/reducer/errorReducer';
import { ResetHomeError } from '../../store/reducer/homeReducer';
import { ResetProductError } from '../../store/reducer/productReducer';
import { ResetReviewError } from '../../store/reducer/ratingReducer';
import { ResetUserError } from '../../store/reducer/userReducer';


const ErrorBoundary = ({children}) => {
    const {error, isOpen} = useSelector((state) => state.error);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const clearError = useCallback(() => {
        dispatch(ResetError());
        dispatch(ResetCartError());
        dispatch(ResetProductError());
        dispatch(ResetCategoryError());
        dispatch(ResetHomeError());
        dispatch(ResetAddressError());
        dispatch(ResetReviewError());
        dispatch(ResetUserError());
        dispatch(ResetComplaintError());
        dispatch(hideError());
    },[dispatch])
    useEffect(()=>{
        console.log("error in errorBoundary: ", error, 'isOpen: ', isOpen);
        if(isOpen && error){
            if(error.statusCode === 0 || error.statusCode === undefined){
                toast.error(error.statusCode === 0 ?  error.message:"Network Error. Please check your connection!!!");
            }else if(error.statusCode === 400){
                toast.error(error.message);
            }else if(error.statusCode === 403 && error.message === 'Account suspended'){
                if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
                    Cookies.remove('at');
                    Cookies.remove('rt');
                }
                dispatch(ResetAuth()); 
                navigate('/suspended-account');
            } else if(error.statusCode === 401 || error.statusCode === 403 ){
                if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
                    Cookies.remove('at');
                    Cookies.remove('rt');
                }
                dispatch(ResetAuth());  
            }else if(error.statusCode === 404){
                navigate(`/404`);
            }else if(error.statusCode >= 500){
                toast.error(error.message);
                navigate('/something-went-wrong');
            }else{
                toast.error("Something went wrong. Server Error!!!");
                navigate('/something-went-wrong');
            }
            clearError();
        }

    },[clearError, dispatch, error, isOpen, navigate])
    
    
    return children; 
}

export default ErrorBoundary;