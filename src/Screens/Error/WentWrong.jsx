import React from 'react'
import { useNavigate } from 'react-router-dom';
import style from './NotFound.module.css';

export const WentWrong = () => {
  const navigate = useNavigate();
  return (
    <div className={style.container}>
      <div className={style.oops}>Oops!</div>

      <div className={style.heading}>Error - Something went wrong.</div>

      <div className={style.paragraph}>The page you are looking for might have been removed, had its name changed or its temporarily not available.</div>

      <button className={style.backTohome} onClick={()=>navigate("/")}>
        Back to home
      </button>
    </div>
  )
}
