import React, { useEffect} from 'react';
import { Header } from '../../Components/StyleComponents/StyledComponents';
import Image from "../../assets/images/product/product.jpg";
import style from "./Cart.module.css";
import { AiOutlinePlus } from 'react-icons/ai';
import { AiOutlineMinus } from 'react-icons/ai';
import { useNavigate, useLocation, Link } from 'react-router-dom';
import ProductPrice from '../Product/ProductPrice';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../Components/Loader/Loader';
import { calculateTotals, decreaseCartItem, deleteACartItem, getAllCartItem, increaseCartItem, ResetBuyNowOrAddress,  } from '../../store/reducer/cartReducer';
import { setError } from '../../store/reducer/errorReducer';
import AlsoLikeProduct from '../Product/AlsoLikeProduct';
import { getAlsoLikeCartProducts } from '../../store/reducer/categoryReducer';


const Cart = (props) => {
    const navigate = useNavigate();
    const location = useLocation();
    const {cartData, buyNow, isError, isLoading, error, totalAmount, cartDiscountTotal} = useSelector(state => state.cartSlice);
    const categoryData = useSelector(state => state.categorySlice);
    const dispatch = useDispatch();
    const addClick = (e, product_variant_id) => {
        e.preventDefault();
        e.stopPropagation();
        dispatch(increaseCartItem(product_variant_id));
    }
    const subClick = (e, product_variant_id) => {
        e.preventDefault();
        e.stopPropagation();
        dispatch(decreaseCartItem(product_variant_id));
    }
    const deleteCart = (e, id) =>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(deleteACartItem(id));
    }
    const handleCheckout = () => {
        if(Object.keys(buyNow).length !== 0){
            dispatch(ResetBuyNowOrAddress());
        }
        navigate('/checkout', {state:{ from: location }, replace: true} );
    }
    // const clickHandle = (e,productId) => {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     navigate(`/product/${productId}`);
    // }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(getAllCartItem());
        dispatch(getAlsoLikeCartProducts());
    },[dispatch]);
    useEffect(()=>{
        if(cartData !== undefined && cartData.length > 0){
            dispatch(calculateTotals());
        } 
    },[dispatch, cartData]);
    if (props.onlyCartPrice) {
        return (
            <>
                {cartData !== undefined && (
                <ProductPrice showOrderSummary={false} totalAmount={totalAmount} cartDiscountTotal={cartDiscountTotal} />
                )}
            </>
        );
    }
    return (
        <div className={style.cart_page}>
            {isLoading ? <Loader /> :
            <div className={style.cart_container}>
                <Header className={style.header_name}>MY CART</Header>
                {cartData !== undefined && cartData.length > 0 ?
                <div className={style.my_cart}>
                    <div className={style.details_container}>
                        <div className={style.cart_products}>
                            {cartData !== undefined ? cartData?.map((element, i) => (
                                <div className={style.cart_product} key={i}>
                                    <div className={`${style.row_container} ${style.row_detail}`}>
                                        <Link to={`/product/${element.product_id}`} className={style.product_image}>
                                            <img src={element.image !== '' ? element.image : Image} alt={"cart_image"} />
                                        </Link>
                                        <div className={style.details}>
                                            <div className={style.row_container}>
                                                <div className={style.product_name}>
                                                    {element.product_name}
                                                </div>
                                            </div>
                                            <div className={style.price_section}>
                                                <div className={style.price}> ₹ {element.price}</div>
                                                <div className={style.old_price}> ₹ {(Number(element.price) + Number(element.discounted_price))}</div>
                                                <div className={style.discount}> Save {`${(Number(element.discounted_price)/(Number(element.price) + Number(element.discounted_price)) * 100).toFixed(2)}%`}</div>
                                            </div>
                                            <div className={style.quantity_delete_card}>
                                                <div className={style.product_count}>
                                                    <button className={style.minus} onClick={(e)=>subClick(e, element.product_variant_id)}>
                                                        <AiOutlineMinus fill='#1f553e' />
                                                    </button>
                                                    <div className={style.counter}>{element.quantity}</div>
                                                    <button className={style.plus} onClick={(e)=>addClick(e, element.product_variant_id)}>
                                                        <AiOutlinePlus fill='#1f553e' />
                                                    </button>
                                                </div>
                                                <div className={style.remove_icon} onClick={(e)=>deleteCart(e, element.id)}>
                                                    Remove
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )): null}
                            {/* Checkout Button Web*/}
                            <div className={style.checkout_web}>
                                <button onClick={handleCheckout}>
                                    Check Out
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className={style.price_container}>
                        <ProductPrice showOrderSummary={false} totalAmount={totalAmount} cartDiscountTotal={cartDiscountTotal}  />
                    </div>
                    <div className={style.checkout_mobile}>
                        <button onClick={handleCheckout}>
                            Check Out
                        </button>
                    </div>
                </div>: "No Cart-item Added"}
                <div>
                    {!categoryData.isLoading ? <AlsoLikeProduct data={categoryData.alsoLikeInCartProduct} isCart={true} />: <Loader />}
                </div>
            </div>}
        </div>
    )
}

export default Cart;