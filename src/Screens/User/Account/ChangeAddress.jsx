import React, { useEffect, useState } from 'react'
import { AiOutlineLeft } from 'react-icons/ai';
import style from "./AddressList.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import AddressForm from '../../../Components/Address/AddressForm';
import { Title } from '../../../Components/StyleComponents/StyledComponents';
import { getAllAddressList, updateAddress } from '../../../store/reducer/addressReducer';
import { diff } from '../../../utils/CompareTwoObject';
import Loader from '../../../Components/Loader/Loader';

const ChangeAddress = () => {
    const dispatch = useDispatch();
    const {id} = useParams();
    const navigate = useNavigate();
    const {addressInfo, isLoading} = useSelector((state) => state.addressSlice);
    const [addressData, setAddressData] = useState({
        name: '',
        mobile_number: '',
        city: '',
        street: '',
        state: '',
        pincode: ''
    });
    const inputHandler = (e) => {
        setAddressData({
            ...addressData,
            [e.target.name] : e.target.value, 
        });
    }
    
    const handleClick = async(e, updatedDetail) => {
        e.preventDefault();
        if(updatedDetail === 'Please reload the page') {
            return toast.error('Please reload the page');
        }
        let newObj = {};
        for(let key in updatedDetail){ 
            if(key === 'updated'){
                for(let attribute in updatedDetail.updated){
                    if(attribute){
                        newObj[`${attribute}`] = updatedDetail.updated[`${attribute}`];
                    }
                }
            }
        }
        if(Object.entries(newObj).length !== 0){
            const res = await dispatch(updateAddress({value: newObj, id})).unwrap();
            if(res.status === 204){
                navigate('/account/address/all');
            }
        } else {
            toast.error('Nothing to update');
        }
    }
    const handleBackbutton = () => {
        navigate(-1);
    }
    useEffect(()=>{
        dispatch(getAllAddressList());
    }, [dispatch]);
    
    if(isLoading){
        return <Loader />
    }
    return (
        <div className={style.change_address_container}>
             <div className={style.web_title}>
                <Title>CHANGE ADDRESS</Title>
            </div>
            <div className={style.mobile_card}>
                <div className={style.bck_page_icon}>
                    <AiOutlineLeft
                        onClick={()=>navigate(-1)}
                    />
                </div>
                <div className={style.mobile_title}>
                    <Title>CHANGE ADDRESS</Title>
                </div>
            </div>
            <AddressForm
                addressData={addressData}
                inputHandler={inputHandler}
                handleClick={handleClick}
                diff={diff}
                handleBackbutton={handleBackbutton} 
                addressInfo={addressInfo}
                id={id}
                setAddressData={setAddressData}
            />
        </div>
    )
}

export default ChangeAddress;