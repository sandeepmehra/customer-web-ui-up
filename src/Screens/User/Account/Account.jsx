import React, { useEffect } from 'react';
import { Title } from '../../../Components/StyleComponents/StyledComponents'; 
import ProfileOptions from './ProfileOption';
import style from "./Account.module.css";
import { CgProfile as ProfileIcon } from "react-icons/cg";
import { Link, Outlet, useLocation, useNavigate } from 'react-router-dom';
import { AiOutlineLeft } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../Components/Loader/Loader';
import { getAccoutDetailInfo } from '../../../store/reducer/userReducer';
import { setError } from '../../../store/reducer/errorReducer';


const AccountPage = () => {
    let location = useLocation();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {name} = useSelector((state)=> state.auth);
    const {isError, isLoading, error} = useSelector(state => state.userSlice);
    // const isLoading = false;
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=> {
        dispatch(getAccoutDetailInfo());
    }, [dispatch])
    if(isLoading){
        return <Loader />
    }
    return (
        <div className={style.main_container}>
                <div className={style.page_container}>
                    <div className={style.account_container}>
                        <div className={style.profile_container}>
                            <div className={style.web_profile_img}>
                                <div className={`${style.profile_img} ${style.outline}`}>
                                    <div className={style.profile_card}>
                                        <Link to={"/account"}>
                                            <ProfileIcon size='3rem' color='#1f553e' />
                                        </Link>
                                        <label className={style.name}>Hi! {name !== undefined ? name.split(" ")[0] : 'There'}</label>
                                    </div>
                                </div>
                                <div className={style.webview_profileOptions}>
                                    <ProfileOptions />
                                </div>
                            </div>
                            {/* Mobile Profile Image  column-container'*/}
                            {location.pathname === '/account' && (
                            <div className={style.mobile_profile_img} onClick={()=>navigate('/')} >
                                <div className={style.mobile_card}>
                                    <div className={style.bck_page_icon}>
                                        <AiOutlineLeft />
                                    </div>
                                    <div className={style.mobile_title}>
                                        <Title>My Account</Title>
                                    </div>
                                </div>
                                <div>
                                    <ProfileIcon size='8rem' color='#1f553e' />
                                </div>
                            </div>)}
                        </div>
                        <div className={style.display_option}>
                            <Outlet />
                        </div>
                    </div>
                </div >
        </div>
    )
}

export default AccountPage;