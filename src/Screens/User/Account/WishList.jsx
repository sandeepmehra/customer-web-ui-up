import React, { useEffect } from 'react';
import WishListOrderCard from '../../Orders/WishlistCard';
import { Title } from '../../../Components/StyleComponents/StyledComponents'; 
import style from '../../Orders/WishlistCard.module.css';
import { getWishlistLists, removeWishlistItem } from '../../../store/reducer/userReducer';
import Loader from '../../../Components/Loader/Loader';
import { useDispatch, useSelector } from 'react-redux';
import { AiOutlineLeft } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';

function WishList() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {wishlist, isLoading} = useSelector((state)=> state.userSlice);
    const handleRemoveWishlist = (e, categoryObj)=>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(removeWishlistItem({id: categoryObj.id, category: categoryObj.category, isProductDetail: false}));
    }
    useEffect(()=> {
        dispatch(getWishlistLists());
    }, [dispatch])
    if(isLoading){
        return <Loader />
    }
    return (
        <div style={{display:'flex', flexDirection:'column', alignItems: 'center', justifyContent:'center'}}>
            <div className={style.web_title}>
                <div>MY WISHLIST</div>
            </div>
            <div className={style.mobile_card}>
                <div className={style.bck_page_icon}>
                    <AiOutlineLeft
                        onClick={()=>navigate(-1)}
                    />
                </div>
                <div className={style.mobile_title}>
                    <Title>MY WISHLIST</Title>
                </div>
            </div>
            {wishlist !== undefined && wishlist.length !== 0 ? wishlist.map((wishlistItem, i)=>(
                <WishListOrderCard 
                    product={wishlistItem} 
                    key={i} 
                    index={i}
                    handleRemoveWishlist={handleRemoveWishlist}
                />
            )): 'No wishlist Item here...'}
        </div>
    )
}

export default WishList;