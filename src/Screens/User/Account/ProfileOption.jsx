import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useLocation, useParams } from 'react-router-dom';
import { logoutAction } from '../../../store/Services/authService';
import FAQ from './Faq';
import { AiOutlineRight } from 'react-icons/ai';
import style from './Option.module.css';

const ProfileOptions = () => {
    const {id} = useParams();
    let location = useLocation();
    const dispatch = useDispatch();
    const clickHandler = () => {
        dispatch(logoutAction());
    }
    const [faq, setFaq] = useState(false)
    return (
        <div className={style.profile_option_container}>
            <br />
            <div className={style.profile_options_web}>
                <div className={style.profile_outline}>
                    <nav className={style.nav}>
                        <div className={`${location.pathname === '/account/wishlist'? `${style.option} ${style.active}`: `${style.option}`} `}>
                            <Link to="wishlist" className={`${location.pathname === '/account/wishlist'? `${style.active}`: `${style.link}`}`}>
                                <div>
                                    My Wishlist
                                </div>
                            </Link>
                        </div>
                        <div className={`${location.pathname === '/account/change/password'? `${style.option} ${style.active}`: `${style.option}`} `}>
                            <Link to="change/password" className={`${location.pathname === '/account/change/password'? `${style.active}`: `${style.link}`}`}>
                                <div>
                                    Change Password
                                </div>
                            </Link>
                        </div>
                        <div className={`${location.pathname === '/account/address/all' || location.pathname === `/account/change/address/${id}` || location.pathname === `/account/address/add` ? `${style.option} ${style.active}`: `${style.option}`} `}>
                            <Link to="/account/address/all" className={`${location.pathname === '/account/address/all' || location.pathname === `/account/change/address/${id}` || location.pathname === `/account/address/add` ? `${style.active}`: `${style.link}`}`}>
                                <div>
                                    Change Address
                                </div>
                            </Link> 
                        </div>
                        <div className={style.option} onClick={()=> clickHandler()}>
                            Log Out
                        </div>
                        <div className={style.faq_option} onClick={() => setFaq(!faq)}>
                            FAQ
                        </div>

                        {faq && (
                            <div>
                                <FAQ />
                            </div>
                        )}
                    </nav>
                </div>
            </div>
            <div className={style.profile_options_mobile}>
                    <div className={style.profile_outline}>
                        <nav className={style.nav}>
                            <div className={`${location.pathname === '/account/wishlist'? `${style.option_mobile} ${style.active}`: `${style.option_mobile}`} `}>
                                <Link to="wishlist" className={`${location.pathname === '/account/wishlist'? `${style.active}`: `${style.link}`}`}>
                                    <div className={style.nav_mobile_card}>
                                        <div>
                                            My Wishlist
                                        </div>
                                        <AiOutlineRight />
                                    </div>
                                </Link>
                            </div>
                            <div className={`${location.pathname === '/account/change/password'? `${style.option_mobile} ${style.active}`: `${style.option_mobile}`} `}>
                                <Link to="change/password" className={`${location.pathname === '/account/change/password'? `${style.active}`: `${style.link}`}`}>
                                    <div className={style.nav_mobile_card}>
                                        <div>
                                            Change Password
                                        </div>
                                        <AiOutlineRight />
                                    </div>
                                </Link>
                            </div>
                            <div className={`${location.pathname === '/account/address/all' || location.pathname === `/account/change/address/${id}` || location.pathname === `/account/address/add` ? `${style.option_mobile} ${style.active}`: `${style.option_mobile}`} `}>
                                <Link to="/account/address/all" className={`${location.pathname === '/account/address/all' || location.pathname === `/account/change/address/${id}` || location.pathname === `/account/address/add` ? `${style.active}`: `${style.link}`}`}>
                                    <div className={style.nav_mobile_card}>
                                        <div>
                                            Change Address
                                        </div>
                                        <AiOutlineRight />
                                    </div>
                                </Link> 
                            </div>
                            <div className={style.option_mobile} onClick={()=> clickHandler()}>
                                <div className={style.nav_mobile_card}>Log Out</div>
                            </div>
                        </nav>
                    </div>
                </div>
        </div >
    )
}

export default ProfileOptions;