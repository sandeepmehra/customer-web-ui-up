import React from 'react'
import { AiOutlineLeft } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import { Title } from '../../../Components/StyleComponents/StyledComponents';
import AddressPage from '../../Checkout/Address/AddAddress';
import style from "./AddressList.module.css";

const AddNewAddress = () => {
  const navigate = useNavigate();
  return (
    <div className={style.change_address_container}>
      <div className={style.web_title}>
        <Title>ADD NEW ADDRESS</Title>
        </div>
        <div className={style.mobile_card}>
            <div className={style.bck_page_icon}>
                <AiOutlineLeft
                    onClick={()=>navigate(-1)}
                />
            </div>
            <div className={style.mobile_title}>
                <Title>ADD NEW ADDRESS</Title>
            </div>
        </div>
        <AddressPage
            isAddNewAddress={true}
        />
    </div>
  )
}

export default AddNewAddress;