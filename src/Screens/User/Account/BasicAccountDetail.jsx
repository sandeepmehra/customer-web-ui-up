import React, { useEffect, useState } from 'react';
import FAQ from './Faq';
import ProfileOptions from './ProfileOption';
import style from "./Account.module.css";
import { diff } from '../../../utils/CompareTwoObject';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { changeAccountDetailInfo } from '../../../store/reducer/userReducer';

const AccountDetail = () => {
    const dispatch = useDispatch();
    const {userInfo, isUpdateLoading} = useSelector((state) => state.userSlice);
    const [currentData, setCurrentData] = useState({
        name: '',
        mobile_number: '',
        email: ''
    });
    
    const handleUpdateClick = (updatedDetail) => {
        let newObj = {};
        for(let key in updatedDetail){ 
            if(key === 'updated'){
                for(let attribute in updatedDetail.updated){
                    if(attribute){
                        newObj[`${attribute}`] = updatedDetail.updated[`${attribute}`];
                    }
                }
            }
        }
        if(Object.entries(newObj).length !== 0){
            dispatch(changeAccountDetailInfo(newObj));
        } else {
            toast.info('Nothing to update');
        }
    }
    const inputHandler = (e) => {
        setCurrentData({
            ...currentData,
            [e.target.name] : e.target.value, 
        });
    }
   
    useEffect(()=>{
        setCurrentData(userInfo);
    },[userInfo]);
    return (
            <>
                <div className={`${style.my_account}`}>
                    <div className={style.web_title}>
                        <div>My Account</div>
                    </div>               
                    <div className={style.account_details}>
                        <div className={style.account_inputs}>
                            <div>*Name</div>
                            <input 
                                type={"text"}
                                name={"name"}
                                defaultValue={userInfo.name}
                                placeholder={'Name'}
                                onChange={inputHandler} 
                            />
                        </div>
                        <div className={style.account_inputs}>
                            <div>*Mobile number</div>
                            <input
                                type={"text"} 
                                name={"mobile_number"}
                                pattern={"[0-9]*"}
                                defaultValue={userInfo.mobile_number} 
                                onChange={inputHandler} 
                                placeholder={'Mobile Number'}
                            />
                        </div>
                    </div>
                    <div className={style.account_details}>
                        <div className={style.account_inputs}>
                            <div>*Email ID</div>
                            <input 
                                type={"email"}
                                name={"email"}
                                defaultValue={userInfo.email}
                                placeholder={'Email id'}
                                onChange={inputHandler} 
                            />
                        </div>
                    </div>
                    <div className={style.account_button}>
                        <button 
                            disabled={isUpdateLoading}
                            onClick={()=>handleUpdateClick(diff(userInfo, currentData))}
                        >{isUpdateLoading ? '...': 'Update'}</button>
                    </div>

                    <div className={style.web_display_faq}>
                        <b>FAQ</b>
                        <FAQ />
                        <div>
                            <b>Deactivate Account</b>
                        </div>
                    </div>
                </div>
            <div className={style.mbview_profileOptions}>
                {ProfileOptions()}
            </div>
            
        </>
    )
}

export default AccountDetail;