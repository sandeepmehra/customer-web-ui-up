import React, { useEffect } from 'react';
import style from "./AddressList.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getAllAddressList, ResetAddress } from '../../../store/reducer/addressReducer';
import { Title } from '../../../Components/StyleComponents/StyledComponents';
import { AiOutlineLeft } from 'react-icons/ai';
import Loader from '../../../Components/Loader/Loader';

const ListAllAddress = () => {
    const {addressInfo, isLoading} = useSelector((state) => state.addressSlice);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleUpdate = (id) => {
        navigate(`/account/change/address/${id}`);
    }
    const handleChangeOrAddAddress = () => {
        navigate('/account/address/add', {replace: true});
    }
    useEffect(()=>{
        dispatch(getAllAddressList());
        return ()=>{
            dispatch(ResetAddress());
        }
    }, [dispatch]);
    if(isLoading){
        return <Loader />
    }
    return (
        <div style={{display:'flex', flexDirection:'column', alignItems: 'center', justifyContent:'center', position: 'relative'}}>
            <div className={style.web_title}>
                <div>ADDRESSES</div>
            </div>
            <div className={style.mobile_card}>
                <div className={style.bck_page_icon}>
                    <AiOutlineLeft
                        onClick={()=>navigate(-1)}
                    />
                </div>
                <div className={style.mobile_title}>
                    <Title>ADDRESSES</Title>
                </div>
            </div>
            
            <div className={style.container_address}>
                {addressInfo !== undefined && addressInfo.lenght !== 0 ? addressInfo.map((element, i)=>(
                    <div key={i} className={style.address_card_container}>
                        <div className={style.address_information_card}>
                            <div>{element.name}</div>
                            <div>
                                <span>{element.street}, {element.city},</span><br />
                                <span>{element.state}-{element.pincode}</span>
                            </div>
                            <div>
                                +91 {element.mobile_number}
                            </div>
                        </div>
                        <div className={style.edit_button} onClick={() => handleUpdate(element.id)}>Edit</div>
                    </div> 
                    )): <div style={{'textAlign': 'center'}} className={style.address_card_container}>
                            <div>Address is not found</div>
                        </div>
                    }
                </div>
            
            <div className={style.address_btn}>
                <button onClick={handleChangeOrAddAddress}>Add New Address</button>
            </div>
        </div>
    )
}

export default ListAllAddress;