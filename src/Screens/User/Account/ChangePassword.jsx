import React, { useState } from 'react';
import { AiFillEye, AiFillEyeInvisible, AiOutlineLeft } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Title } from '../../../Components/StyleComponents/StyledComponents'; 
import style from "./Password.module.css";
import { toast } from 'react-toastify';
import { changeAccountPassword } from '../../../store/reducer/userReducer';

const ChangePassword = () => {
    const navigate = useNavigate();
    const {isUpdateLoading} = useSelector((state) => state.userSlice);
    const [passwordType, setPasswordType] = useState("password");
    const [passwordReType, setPasswordReType] = useState("password");
    const [password, setPassword] = useState({
        newPassword : '',
        confirmPassword: ''
    });
    const togglePassword =()=>{
        if(passwordType==="password"){
            setPasswordType("text")
            return;
        }
        setPasswordType("password")
    }
    const togglePasswordConfirm = () => {
        if(passwordReType==="password"){
            setPasswordReType("text")
            return;
        }
        setPasswordReType("password")
    }
    const dispatch = useDispatch();
    const clickHandle = async(newPwd, confirmPwd) => {
        if(newPwd === '' || confirmPwd === ''){
            return toast.error('Password are empty.');
        }
        if(newPwd !== confirmPwd){
            return toast.error('Password are not same.');
        }
        if(newPwd !== undefined && newPwd === confirmPwd){
            await dispatch(changeAccountPassword({password: newPwd}));
            setPassword({newPassword : '', confirmPassword: ''});
            return navigate("/account");
        }
    }
    return (
        <div className={style.change_password_card}>
            <div className={style.web_title}>
                <div>CHANGE PASSWORD</div>
            </div>
            <div className={style.mobile_card}>
                <div className={style.bck_page_icon}>
                    <AiOutlineLeft
                        onClick={()=>navigate(-1)}
                    />
                </div>
                <div className={style.mobile_title}>
                    <Title>CHANGE PASSWORD</Title>
                </div>
            </div>
            <div className={style.change_password_container}>
                <div className={style.change_password} >
                    <p className={style.change_password_heading}>*New Password</p>
                    <div className={style.input_group}>
                        <input 
                            type={passwordType} 
                            name={"newPassword"}
                            autoComplete="off"
                            placeholder='new password'
                            value={password.newPassword}
                            onChange={(e)=>setPassword({...password, [e.target.name]: e.target.value})}    
                        />
                        <span onClick={togglePassword} className={style.eye_style}>
                            {passwordType === "password" ? <AiFillEyeInvisible size={28}/> : <AiFillEye size={28}/>}
                        </span>
                    </div>
                    <p className={style.change_password_heading}>*Retype Password</p>
                    <div className={style.input_group}>
                        <input 
                            type={passwordReType}
                            name={"confirmPassword"}
                            autoComplete="off"
                            placeholder='confirm password'
                            value={password.confirmPassword}
                            onChange={(e)=>setPassword({...password, [e.target.name]: e.target.value})}
                        />
                        <span onClick={togglePasswordConfirm} className={style.eye_style}>
                            {passwordReType === "password" ? <AiFillEyeInvisible size={28}/> : <AiFillEye size={28}/>}
                        </span>
                    </div>
                    <div className="change_password_btn">
                        <button 
                            type='submit'   
                            disabled={isUpdateLoading || password.confirmPassword === '' || password.newPassword === ''}
                            onClick={()=>clickHandle(password.newPassword, password.confirmPassword)}
                        >{isUpdateLoading ? '...': 'Change Password'}</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ChangePassword;