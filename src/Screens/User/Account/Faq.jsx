import React from 'react';
import { accountFaq } from '../../../Components/data/account';
const FAQ = () => {
    return (
        accountFaq.map((item, i) => {
            return (
                <div style={{"marginTop": "1rem"}} key={i}>
                    <b>{item.question}</b>
                    <br />
                    {item.answer}
                </div >
            )
        })
    )
}

export default FAQ;