import React, { useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Common from './Common';
import { loginMobileNumber, userSignUpByEmail } from '../../../store/reducer/authReducer';
import { FcGoogle } from 'react-icons/fc';
import { MdCall } from 'react-icons/md';
import { MediumLabel, Title } from '../../../Components/StyleComponents/StyledComponents';
import style from "./Auth.module.css";
import { setError } from '../../../store/reducer/errorReducer';
import { baseURL } from '../../../api';

const SignUp = () => {
    let navigate = useNavigate(); 
    let location = useLocation();
    let from = location.state?.from?.pathname || "/account";
    const {error, isLoading, isAuthenticated, isError} = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const [values, setValues] = useState({
        email: '',
        password: '',
        username: ''
    });
    const [mobile, setMobile] = useState('');
    const { email, password, username } = values
    const submitMobileNumber = (e, mobile) => {
        e.preventDefault();
        if(mobile !== ''){
            dispatch(loginMobileNumber({mobile_number: mobile}));
            navigate("/verify/mobile", {state:{data: mobile}});
        }
    }
    const handleClick = (e) => {
        e.preventDefault();
        if(email !== '' && password !== '' && username){
            dispatch(userSignUpByEmail({email, password, name: username}));
        }
        setValues({email: '', password: '', username: ''});
    }

    const handleChange = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value })

    }
    const google = () => {
        window.open(`${baseURL}/auth/google`);
    };
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    
    useEffect(() => {
        if (isAuthenticated) {
            navigate(from, { replace: true });
        }
    }, [navigate, isAuthenticated, from]); 
    return (
        <Common> 
            <div className={style.signin_form}>
                <div className={style.signin_form_card}>
                    <Title>Sign Up</Title>
                    <div style={{"marginTop": "1rem"}}>
                        <label>Sign up via email</label>
                    </div>
                    <form onSubmit={handleClick}>
                        <input 
                            type="text" 
                            className={style.input} 
                            id="name" 
                            name="username" 
                            autoComplete='off'
                            placeholder="Name" 
                            onChange={handleChange("username")} 
                            value={username} 
                        />
                        <input 
                            type="email" 
                            className={style.input}
                            id="email" 
                            name="email" 
                            autoComplete='off'
                            placeholder="Email Id" 
                            onChange={handleChange("email")} 
                            value={email} 
                        />
                        <input 
                            type="password" 
                            className={style.input} 
                            id="password" 
                            name="password"
                            autoComplete='off' 
                            placeholder="Password" 
                            onChange={handleChange('password')} 
                            value={password} 
                        />
                        <button type='submit' className={style.btn}>{isLoading !== 'loading' ? "Sign up": " ... "}</button>
                    </form>
                    <div className={style.or}>
                        <hr />
                        <label>OR</label>
                        <hr />
                    </div>
                    <div>
                        <MediumLabel className={style.margin_top}>Sign up via Mobile number</MediumLabel>
                        <div className={style.mobile_container}>
                            <MdCall />
                            <input 
                                type="text" 
                                name="mobile"
                                pattern="[0-9]*" 
                                onChange={(e)=> setMobile(e.target.value)}
                                value={mobile}
                            />
                        </div>
                        <button 
                            className={style.btn} 
                            onClick={(e)=>submitMobileNumber(e, mobile)} 
                            disabled={true}
                        >
                            Sign up via WhatsApp
                        </button>
                    </div>
                    <div className={style.google_sign_btn} onClick={google}>
                        <div className={style.google_img}>
                            <FcGoogle size={'28'}/>
                        </div>
                        <button>Google Sign Up</button>
                    </div>
                    <div className={style.label_urban}>
                        <hr />
                        <label>
                            Already have an account?
                        </label>
                        <hr />
                    </div>
                    <div>
                        <Link to="/login">
                            <button className={style.new_account}>Login</button>
                        </Link>
                    </div>
                </div>
            </div >
        </Common>
    )
}

export default SignUp;