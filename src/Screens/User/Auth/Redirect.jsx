import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getUser } from "../../../store/Services/authService";


export function RedirectAuth(){
    const navigate = useNavigate();
    const dispatch = useDispatch();
    useEffect(() => {
      getUser(dispatch);
      navigate('/');     
    }, [navigate, dispatch]);
  
    return (
      <div> logging you in </div>
    )
}