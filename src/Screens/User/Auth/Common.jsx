import React from 'react';
import { ReactComponent as WelcomeIcon } from '../../../assets/images/customer_welcome_page.svg';
import style from "./Auth.module.css";

const Common = ({children }) => {
    return (
        <div className={style.main_container}>
            <div className={`${style.page_container} ${style.margin_top}`}>
                <div className={style.outline}>
                    {children}
                    <div className={style.line_container}>
                        <div className={style.line_vertical}></div>
                    </div>
                    <div className={style.welcome_container}>
                        <div className={style.welcome_heading}>
                            Welcome To Urban Plants!
                        </div>
                        <div>
                            <WelcomeIcon className={style.welcome_logo} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Common;