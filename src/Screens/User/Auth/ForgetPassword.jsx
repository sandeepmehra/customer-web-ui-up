import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Title } from  '../../../Components/StyleComponents/StyledComponents';
import style from "./Auth.module.css";
import { toast } from 'react-toastify';
import Common from './Common';
import { emailLoginVerifyAction } from '../../../store/reducer/authReducer';

const ForgetPassword = () =>{
    let navigate = useNavigate(); 
    let location = useLocation();
    let email = location.state?.data;

    const dispatch = useDispatch();
    const [password, setPassword] = useState({
        newPassword : '',
        confirmPassword: '',
        otp: ''
    });
    const clickHandle = async(newPwd, confirmPwd, verifyotp) => {
        if(newPwd !== confirmPwd){
            toast.error('Password are not same! try again');
        }
        if(email !== '' && email !== undefined && verifyotp !== '' ){
            await dispatch(emailLoginVerifyAction({email: email, password: newPwd, otp: verifyotp}));
            navigate("/login");
        }
    }
    if(email === ''){
        return null
    }
    return (
        <Common>
            <div className={`${style.signin_form} ${style.verify}`}>    
                <div className={style.change_password}>
                    <h3>Forget Password</h3>
                    <div className={style.forget_title}>New Password</div>
                    <input 
                        type={"text"} 
                        name={"newPassword"}
                        value={password.newPassword}
                        placeholder="new password"
                        onChange={(e)=>setPassword({...password, [e.target.name]: e.target.value})}
                    />
                    <div className={style.margin_top_2}>
                        <div className={style.forget_title}>Retype Password</div>
                        <input 
                            type={"text"}
                            name={"confirmPassword"}
                            placeholder="confirm password"
                            value={password.confirmPassword}
                            onChange={(e)=>setPassword({...password, [e.target.name]: e.target.value})} 
                        />
                    </div>
                    <div className={style.margin_top_2}>
                        <div className={style.forget_title}>OTP</div>
                        <input 
                            type={"text"}
                            name={"otp"}
                            placeholder="otp"
                            value={password.otp}
                            onChange={(e)=>setPassword({...password, [e.target.name]: e.target.value})} 
                        />
                    </div>
                    <div>
                        <button 
                            type='submit' 
                            className={`${style.btn} ${style.margin_top_2} ${style.width}`} 
                            onClick={()=>clickHandle(password.newPassword, password.confirmPassword, password.otp)}
                        >confirm</button>
                    </div>
                </div> 
            </div>           
        </Common>      
    );
};
export default ForgetPassword;
 