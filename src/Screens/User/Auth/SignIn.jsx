import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Common from './Common';
import { forgetPasswordEmail, loginMobileNumber, userLoginByEmail } from '../../../store/reducer/authReducer';
import { FcGoogle } from 'react-icons/fc';
import { MdCall } from 'react-icons/md';
import { MediumLabel, SmallLabel, Title } from '../../../Components/StyleComponents/StyledComponents';
import { Link } from 'react-router-dom';
import style from "./Auth.module.css";
import { setError } from '../../../store/reducer/errorReducer';
import { baseURL } from '../../../api';

const SignIn = () => {
    let navigate = useNavigate();
    let location = useLocation();
    let from = location.state?.from?.pathname || "/account";
    const dispatch = useDispatch();
    const {error, isLoading, isAuthenticated, isError} = useSelector((state) => state.auth);
    const [values, setValues] = useState({
        email: '',
        password: ''
    });
    const [mobile, setMobile] = useState('');
    const { email, password } = values;  
    const submitMobileNumber = (e, mobile) => {
        e.preventDefault();
        if(mobile !== ''){
            dispatch(loginMobileNumber({mobile_number: mobile}));
            navigate("/verify/mobile", {state:{data: mobile}});
        }
    }
    const handleClick = async(e) => {
        e.preventDefault();
        if(email !== '' && password !== ''){
            await dispatch(userLoginByEmail({email, password}));
        }
        setValues({email: '', password: ''});
    }
    const handleForgetPassword = (e, email) =>{
        e.preventDefault();
        if(email !== ''){
            dispatch(forgetPasswordEmail({email: email}));
            navigate("/login/reset-password", {state: {data: email}});
        }
    }
    const handleChange = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value })
    }
    const google = async() => {
        window.open(`${baseURL}/auth/google`);
    };
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        if (isAuthenticated) {
            navigate(from, { replace: true });
        }
    }, [navigate, isAuthenticated, from]);    
    return (
        <Common>
            <div className={style.signin_form}>
                <div className={style.signin_form_card}>
                <Title>Sign In</Title>
                <div style={{"marginTop": "1rem"}}>
                    <label>Sign in via email</label>
                </div>
                <form onSubmit={handleClick}>
                    <input 
                        type="email" 
                        className={style.input}
                        id="email" 
                        name="email" 
                        autoComplete='off'
                        placeholder="Email Id" 
                        onChange={handleChange("email")} 
                        value={email} 
                    />
                    <input 
                        type="password" 
                        className={style.input} 
                        id="password" 
                        name="password"
                        autoComplete='off' 
                        placeholder="Password" 
                        onChange={handleChange('password')} 
                        value={password} 
                    />
                
                    <div className={style.forget}>
                        <SmallLabel onClick={(e)=>handleForgetPassword(e, email)}>
                            <u> 
                                Forgot Password? 
                            </u>
                        </SmallLabel>
                    </div>
            
                    <button type='submit' className={style.btn}>{isLoading !== 'loading' ? "Login": " ... "}</button>
                </form>
                <div className={style.or}>
                    <hr />
                    <label>OR</label>
                    <hr />
                </div>
                <div>
                    <MediumLabel className={style.margin_top}>Sign in via Mobile number</MediumLabel>
                    <div className={style.mobile_container}>
                        <MdCall />
                        <input 
                            type="text" 
                            name="mobile"
                            pattern="[0-9]*" 
                            onChange={(e)=> setMobile(e.target.value)}
                            value={mobile}
                        />
                    </div>
                    <button 
                        className={style.btn} 
                        onClick={(e)=>submitMobileNumber(e, mobile)} 
                        disabled={true}
                    >
                        Login via WhatsApp
                    </button>
                </div>
                <div className={style.google_sign_btn} onClick={google}>
                    <div className={style.google_img}>
                        <FcGoogle size={'28'}/>
                    </div>
                    <button>Google Login</button>
                </div>

                <div className={style.label_urban}>
                    <hr />
                    <label>
                        New to Urban Plants?
                    </label>
                    <hr />
                </div>
                
                <div>
                    <Link to="/signup" >
                        <button className={style.new_account}>Create your account</button>
                    </Link>           
                </div>
                </div>
            </div >
        </Common>
    )
}

export default SignIn;