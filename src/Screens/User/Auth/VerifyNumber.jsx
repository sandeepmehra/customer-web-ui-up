import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Label, Title } from '../../../Components/StyleComponents/StyledComponents';
import { loginMobileNumber } from '../../../store/reducer/authReducer';
import style from "./Auth.module.css";
import Common from './Common';

const VerifyNumber = () =>{
    const {isAuthenticated} = useSelector((state) => state.auth);
    let navigate = useNavigate();
    let location = useLocation();
    let mobile = location.state?.data;
    let from = location.state?.from?.pathname || "/account"; 
    const dispatch = useDispatch();
    const [otp, setOtp] = useState('');
    const handleClickOTP = (e) => {
        e.preventDefault();
        if(mobile !== '' && otp !== ''){
            dispatch(loginMobileNumber({mobile_number: mobile, otp: otp}))
        }
    }
    useEffect(() => {
        if (isAuthenticated) {
            navigate(from, { replace: true });
        }
    }, [navigate, isAuthenticated, from, dispatch]);
    if(mobile === ''){
        return null;
    }
    return (
        <Common>
            <div className={`${style.signin_form} ${style.verify}`}>
                <Title>Verification</Title>
                <div className={style.margin_top}>
                    <Label>Please enter the verification code sent to whatsapp number</Label>
                    <form onSubmit={handleClickOTP}>
                        <div className={style.mobile_container}>
                            <input 
                                type="text" 
                                name="otp"
                                onChange={(e)=> setOtp(e.target.value)}
                                value={otp}
                            />
                        </div>
                        <button type='submit' className={style.btn}>Verify</button>
                    </form>
                </div>
            </div>
        </Common>  
    )
};
export default VerifyNumber;