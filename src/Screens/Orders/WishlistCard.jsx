import React from 'react';
import style from './WishlistCard.module.css';
import { Label } from "../../Components/StyleComponents/StyledComponents";
import {AiOutlineDelete} from 'react-icons/ai';
import { useLocation, useNavigate } from 'react-router-dom';
import { createCart, SetBuyNow } from '../../store/reducer/cartReducer';
import { useDispatch, useSelector } from 'react-redux';

const WishListOrderCard = ({product, index, handleRemoveWishlist }) => {
    const navigate = useNavigate();
    const location = useLocation();
    const dispatch = useDispatch();
    const {isAuthenticated} = useSelector((state)=> state.auth);
    const addToCart = async(e, productVariantId) => {
        e.preventDefault();
        e.stopPropagation();
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(createCart({productVariantId, quantity: 1}));  
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    };
    const handleBuyNow = async(e, productVariantId) => {
        e.preventDefault();
        e.stopPropagation();
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(SetBuyNow({productVariantId, quantity: 1}));
            navigate('/checkout');
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    }
    const clickHandle = (productId) => {
        navigate(`/product/${productId}`);
    }
    console.log("product", product)
    return (
        <div className={style.my_wishlist} key={index} onClick={()=>clickHandle(product.product_variant_id)}>
            <div className={style.row}>
                <div className={style.product_image}>
                    <img src={product.image} alt={'product_image'}/>
                </div>
                <div className={`${style.column} ${style.product_details}`}>
                    <div className={style.product_title_card}>
                        <Label>{product.product_name}</Label>
                    <div>
                        {product.wishlist_id !== "undefined" &&
                            <AiOutlineDelete 
                                color='red'
                                size={24}
                                className={style.delete_btn} 
                                onClick={(e)=>handleRemoveWishlist(e, {id: product.id, category: product.category})}
                            />
                        }</div>
                    </div>    
                    <div className={style.shop_name}>
                        {product.shop_name}
                    </div>

                    <div className={style.card}>
                        <div>
                            <b>₹ {product.price}</b>
                        </div>
                        <div className={style.product_old_price}>
                            <del>₹ {Number(product.price) + Number(product.discounted_price)}</del>
                        </div>
                        <div className={style.card_discount_web}>
                            {/*  Discount % = (Discount/List Price) × 100 */}
                            Save {(Number(product.discounted_price)/(Number(product.price) + Number(product.discounted_price)) * 100).toFixed(2)}%
                        </div>
                    </div>
                    <div className={style.card_discount_mobile}>
                        {/*  Discount % = (Discount/List Price) × 100 */}
                        Save {(Number(product.discounted_price)/(Number(product.price) + Number(product.discounted_price)) * 100).toFixed(2)}%
                    </div>
                    <div className={style.card_btns}>
                        <button onClick={(e)=>addToCart(e, product.product_variant_id)}>Add to Cart</button>
                        <button onClick={(e)=>handleBuyNow(e, product.product_variant_id)}>Buy now</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WishListOrderCard;