import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Title } from "../../Components/StyleComponents/StyledComponents";
import style from './Orders.module.css'
import OrdersCard from './OrderCard';
import { useDispatch, useSelector } from 'react-redux';
import { AiOutlineLeft } from 'react-icons/ai';
import Loader from '../../Components/Loader/Loader';
import { getAllOrders } from '../../store/reducer/orderReducer';
import { setError } from '../../store/reducer/errorReducer';
const OrdersPage = () => {
    const { orders, isLoading, isError, error } = useSelector((state) => state.orderSlice);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const clickToBack = () =>{
        navigate(-1);
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    
    useEffect(()=>{
        dispatch(getAllOrders());
    }, [dispatch]);  
    return (
        <div className={style.main_container}>
            {isLoading ? <Loader /> :
                <div className={style.page_container}>
                    <div className={style.orders_page}>
                        {
                        false ? 
                            <div className={style.success_msg}>
                                <span className={style.success_line}>Your order has been placed successfully!</span>
                                <br />
                                <span>You can track the status of your order in the <b>My Orders </b> section</span>
                                <br />
                                <div className={style.btn}>
                                    <button onClick={()=>navigate('/')}>Continue Shopping</button>
                                </div>
                            </div> : ''
                        }
                        <div className={style.my_orders_section}>
                            <div className={style.header_card}>
                                <AiOutlineLeft className={style.left_arrow} onClick={clickToBack}/>
                                <Title>MY ORDERS</Title>
                            </div>
                            <hr className={style.display_hr} />
                            <OrdersCard data={orders} showDetails={false} />
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}


export default OrdersPage;