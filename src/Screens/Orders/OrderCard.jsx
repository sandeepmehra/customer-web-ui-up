import React from 'react';
import style from "./OrderCard.module.css";
import { AiOutlineRight } from 'react-icons/ai';
import { Navigate, useNavigate } from 'react-router-dom';
const OrderCard = ({
    showComplaint = false,
    data
}) => {
    const navigate = useNavigate();
    const clickComplaintPage = (showComplaint) => {
        return (
            showComplaint && (
                <Navigate to='/complaint' />
            )
        )
    }
  
    const clickHandle = (orderId) => {
        navigate(`/order/${orderId}`)
    }
  
    const displayrefund = () => (
        <div className={style.refund_section}>
            <div className={style.outline}>
                <div className={style.comments_card}>
                    <div className={style.status}>
                        Refund Completed

                    </div>
                    <div className={style.id}>
                        Refund ID: XXXXXXXXXX
                    </div>
                </div>
                <div className={style.comments}>
                    We have completed your request for refund. ₹299.00 has been transferred to your bank account on September 20.
                    For any questions, please contact your bank with reference number XXXXXXXXXX.
                </div>
            </div>
        </div>
    )
    return (
    <>
        {(data !== undefined &&  Object.keys(data).length !== 0 && data.orders !== null) ? data.orders.map((item, i) => (
        <div className={style.orders_card} key={i} onClick={() => clickHandle(item.id)}>
                <div className={style.orders_container}>
                    <div className={style.product_image}>
                        <img src={item.image} alt={'order_image'} />
                    </div>
                    <div className={style.product_details}>
                        <div className={style.order_name}>
                            <div className={style.product_name}>
                                {item.product_name}
                            </div>
                            <div className={style.seller_card}>
                                <span className={style.seller}> Seller: </span>
                                <span className={style.shop_name}>{item.shop_name}</span>
                            </div>
                        </div>
                        <label className={style.price}><b>₹ {item.total_price}</b></label>
                        <label className={style.status}>
                            <p className={style.status_name}>{item.status_text}</p>
                            <p className={style.status_description}>Your order has been {item.status_text}</p>
                        </label>
                    </div>
                    <div className={style.right_arrow_card}>
                        <AiOutlineRight className={style.right_arrow} onClick={() => clickHandle(item.id)} />
                    </div>
                </div>
            
            {/* Display refund  */}
            {showComplaint ? displayrefund() : ''}
            {clickComplaintPage(showComplaint)}
        </div>)): <div>Please Order some plants!</div>}
    </>
    )
}

export default OrderCard;