import React, { useEffect } from 'react';
import { Label, Title } from "../../Components/StyleComponents/StyledComponents";
import style from './OrderDetail.module.css';
import DisplayStatus from '../../Components/Status/Status';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AiOutlineLeft } from 'react-icons/ai';
import Loader from '../../Components/Loader/Loader';
import { getOrderDetail } from '../../store/reducer/orderReducer';
import { setError } from '../../store/reducer/errorReducer';
import customerApi from '../../api';
import { saveAs } from 'file-saver';

const OrderDetailsPage = () => {
    const { orderDetail, isLoading, isError, error } = useSelector((state) => state.orderSlice);
    const {orderId} = useParams();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const clickToBack = () =>{
        navigate(-1);
    }
    const handleReview = (reviewId) =>{
        navigate(`/review/${reviewId}`)
    }
    const downloadPdf = async(e, orderId) => {
        e.preventDefault();
        try{
            const response = await customerApi.get(`/order/${orderId}/payment/invoice/download`, { responseType: 'blob' });
            console.log("res", response); 
            const pdfBlob = new Blob([response.data], { type: 'application/pdf' });
            console.log("pdfBlob: ", pdfBlob);
            saveAs(pdfBlob, 'invoice.pdf');
        }catch(error){
            console.log("error in api", error);
        }
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        dispatch(getOrderDetail(orderId))
    }, [dispatch, orderId]);
    const webUibtns = () => (
        <>
            <div className={style.buttons}>
                <div className={style.help_btn}>
                    {!orderDetail?.review_action_taken ?<button onClick={()=>handleReview(orderDetail.id)}>Rate this Order</button>:null}
                </div>
                <div className={style.help_btn}>
                    <button onClick={()=>navigate('/help-center')}>Need help?</button>
                </div>
            </div>
        </>
    )
    const mobileUibtns = () => (
        <div className={style.mobileUicard}>
            <div className={style.buttons_mobile}>
                <button  onClick={()=>navigate('/help-center')}>Need help?</button>
            </div>
            <div className={style.buttons_mobile}>
                <button onClick={()=>handleReview(orderDetail.id)}>
                    Rate this Order
                </button>
            </div>
        </div>
    )

    return (
        <div className={style.main_container}>
            {isLoading ? <Loader /> :
                <div className={style.order_container}>
                    <div className={style.order_m_b}>
                        <div className={style.title}>
                            <AiOutlineLeft className={style.left_arrow} onClick={clickToBack}/>
                            <Title>ORDER DETAILS</Title>
                        </div>
                        <hr className={style.display_hr} />
                        {orderDetail !== undefined && (
                            <div className={style.order_row_container}>
                                <div className={style.delivery_address}>
                                    <div>
                                        <b>Delivery Address</b>
                                        <div style={{marginTop: "1rem"}}>
                                            {orderDetail.address?.name}
                                        </div>
                                        <div>
                                            <span>{orderDetail.address?.street},</span>
                                            <span>{` ${orderDetail.address?.city}`},</span>
                                            <br /> 
                                            <span>Phone number : {orderDetail.address?.mobile_number}</span>
                                            <br />
                                            <span>{orderDetail.address?.state}-{orderDetail.address?.pincode}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={style.delivery_address}>
                                    <b>More actions</b>
                                    <div className={style.order_more_action_btn}>
                                        <button onClick={(e)=>downloadPdf(e, orderDetail.id)}>Download invoice</button>       
                                    </div>
                                </div>
                            </div>
                        )}
                            
                    </div>
                    <div className={style.order_details}>
                        <div className={style.order_card}>
                            {/* Product Image */}
                            <div className={style.product_image}>
                                {orderDetail !== undefined && (
                                    <img src={orderDetail.image} alt={"order_image"} />
                                )}
                            </div>
                            {/* Product Name */}
                            <div className={style.product_details}>
                                <div className={style.product_detals_info}>
                                    <div className={style.product_name}>
                                        {orderDetail !== undefined && (orderDetail.product_name)}
                                    </div>
                                    <div className={style.product_seller_name}>
                                        Seller: {orderDetail !== undefined && (orderDetail.shop_name)}
                                    </div>
                                </div>
                            </div>
                            <div className={style.product_details_price}>
                                {orderDetail !== undefined && (<b>₹ {orderDetail.total_price}</b>)}
                            </div>
                            {webUibtns()}
                        </div>
                        <div className={style.product_details_price_mobile}>
                            <div>{orderDetail !== undefined && (<b>₹ {orderDetail.total_price}</b>)}</div>
                            <div>Quantity: 1</div>
                        </div>
                        {/* Order Status */}
                        <div className={style.status}>
                            <DisplayStatus status={orderDetail !== undefined && orderDetail.status_text}/>
                        </div>
                        <hr className={style.hr} />
                        <div className={style.display_msg}>
                            Your item has been {orderDetail !== undefined && orderDetail.status_text}
                        </div>
                        {mobileUibtns()}
                    </div>
                </div>
            }
        </div>
    )
}

export default OrderDetailsPage;