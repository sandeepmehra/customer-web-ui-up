import React, { useEffect, useState } from 'react';
import HelpCenter from '../../assets/images/help_center.png';
import { Title } from "../../Components/StyleComponents/StyledComponents";
import { Link, Outlet, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import style from './HelpCenter.module.css';
import { AiOutlineLeft} from 'react-icons/ai';
import Loader from '../../Components/Loader/Loader';
import { FaqItem } from './FaqItem';
import moment from 'moment';
import { Accordion } from '../../Components/Accordion';
import { getAllComplaints } from '../../store/reducer/complaintReducer';
import { setError } from '../../store/reducer/errorReducer';

const HelpCenterPage = () => {
    const { complaints, isLoading, isError, error } = useSelector((state) => state.complaintSlice);
    console.log("complaints: ", complaints);
    const [isOpen, setIsOpen] = useState([]);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(getAllComplaints());
    }, [dispatch]);
    const raiseAComplaint = () => {
        navigate(`/complaint`);
    }
    return (
        <div className={style.main_container}>
            { isLoading ? <Loader /> :
                <div className={style.page_container}> 
                    <div className={style.help_page}>
                        <div className={style.help_title}>
                            <Title>HELP CENTRE</Title>
                        </div>
                        <div className={style.help_container}>
                            <div className={style.display_info}>
                                <div className={style.support_icon}>
                                    <img src={HelpCenter} alt="help_center_image" />
                                </div>
                                <label className={style.msg}>Get quick customer support</label>
                                <div className={style.bck_page_icon}>
                                    <Link to='/'>
                                        <AiOutlineLeft />
                                    </Link>
                                </div>
                            </div>
                            <div className={style.display_btn}>
                            <button onClick={raiseAComplaint}>Raise a new complaint</button>
                            </div>
                        </div>
                        <div className={style.orders_section_container}>                        
                            <div className={style.prev_complaints}>
                                {complaints !== undefined && Object.keys(complaints).length !== 0 && complaints?.in_progress?.map((element) => 
                                    <div className={style.complaint} key={element.id}>
                                        <div className={style.complaint_card}>
                                            <div className={style.complaint_info}>
                                                <label className={style.complaint_msg}>{element.subject}</label>
                                                <small>{moment(element.created_on).format('DD/MM/YYYY, h:mm A')}</small>
                                            </div>
                                            
                                            <div className={style.complaint_action}>
                                                <div className={`${style.complaint_status}`} style={{border: "1px solid red"}}>
                                                    In_progress
                                                </div>
                                                <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id} className={style.nxt_page_icon}/>
                                            </div>
                                        </div>
                                        <div className={isOpen.includes(element.id)? `${style.show}`: `${style.hide}`}>
                                        {
                                            isOpen.includes(element.id) && ( 
                                                <div className={style.detail_shown}>
                                                    <Outlet />
                                                </div>
                                        )}  
                                        </div>
                                    </div>
                                )}
                                {complaints !== undefined && Object.keys(complaints).length !== 0 && complaints?.resolved?.map((element) => 
                                    <div className={style.complaint} key={element.id}>
                                        <div className={style.complaint_card}>
                                            <div className={style.complaint_info}>
                                                <label className={style.complaint_msg}>{element.subject}</label>
                                                <small>{moment(element.closed_on).format('DD/MM/YYYY, h:mm A')}</small>
                                            </div>
                                            <div className={style.complaint_action}>
                                                <div className={`${style.complaint_status}`} style={{border: "1px solid #1f553e"}}>
                                                    Resolved
                                                </div>
                                                <div className={style.nxt_page_icon_resolved}>
                                                    <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={isOpen.includes(element.id)? `${style.show}`: `${style.hide}`}>
                                        {
                                            isOpen.includes(element.id) && ( 
                                                <div className={style.detail_shown}>
                                                    <Outlet />
                                                </div>
                                        )}  
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <FaqItem title={"What issues are you facing?"} isHelpPage={true}/>
                    </div>
                </div>
            }
        </div>
    )
}


export default HelpCenterPage;