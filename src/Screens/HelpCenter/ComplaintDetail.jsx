import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import style from './ComplaintDetail.module.css';
import Loader from '../../Components/Loader/Loader';
import { getComplaintDetail } from '../../store/reducer/complaintReducer';
import Response from './Response';
import { setError } from '../../store/reducer/errorReducer';
import ReadMore from './ReadMore';

export const ComplaintDetail = () => { 
    const { complaint, isDetailLoading, isError, error } = useSelector((state) => state.complaintSlice);
    let {id} = useParams();
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        dispatch(getComplaintDetail(id)); 
    }, [dispatch, id]);
  return (
    <div className={style.main_container}>
       {isDetailLoading ? <Loader /> : 
            <>
                <div className={style.detail_card}>
                    <div className={style.first_col} >
                        <div className={` ${style.cd_row}  ${style.cd_pt}`}>
                            <div className={`${style.cd_th}`}>Subject:</div>
                            <div className={style.cd_td}>{complaint.subject}</div>
                        </div>
                                            
                        <div className={`${style.cd_th} ${style.cd_pt}`}>Description:</div>
                        <div className={`${style.cd_description_box}`}>
                            {complaint.description}
                        </div>
                    </div>              
                    { complaint.open && <Response complaintId={complaint.id} />}    
                </div>
                {complaint.responses !== undefined && <div className={style.response_card}>        
                    <div className={`${style.cd_th}`}>{complaint.responses !== undefined && 'Responses:'}</div>
                    {complaint.responses !== undefined && complaint?.responses.map((element, index)=>(
                        <div className={`${style.cd_row} ${style.cd_pt}`} key={index}>
                            <div className={style.cd_w}>
                                <div className={`${style.cd_td} ${style.cd_text_underline}`}>{element.from}</div>
                                <div className={`${style.cd_td} ${style.cd_text_underline}`}>({moment(element.datetime).format('DD/MM/YYYY, h:mm A')})</div>  
                            </div>
                            <div className={`${style.cd_td} ${style.cd_italic}`}>
                            <ReadMore>{element.response}</ReadMore>
                            </div>
                        </div>   
                    ))}
                </div> }
            </> 
            }          
    </div>

  )
}
