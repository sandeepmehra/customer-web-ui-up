import React from 'react';
import { AiOutlineRight } from 'react-icons/ai';
import style from './HelpCenter.module.css';

export const FaqItem = ({title, isHelpPage}) => {
  return (
    <div className={style.help_msg}>
        {isHelpPage ? (<div className={style.issue_label}>{title}</div>): (<div className={style.issue_label_faq}>{title}</div>)}
        <div className={style.help_center_messages}>
            {[5,3,3,4].map((_, i)=>(
                <div className={style.help_center_messages_child} key={i}>
                    <div className={style.issue_message}>
                        <label><strong>I want to track my order</strong></label>
                        <br />
                        <label>check order status and call the delivery agent</label>
                    </div>
                    <div className={style.nxt_page_icon}>
                        <AiOutlineRight />
                    </div>
                </div>
            ))}
        </div>
    </div>
    )
}
