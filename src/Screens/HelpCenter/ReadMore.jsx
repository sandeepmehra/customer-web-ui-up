import { useState } from "react";
import style from './ReadMore.module.css';
import useScreenSize from '../../Hooks/ScreenSize';

const ReadMore = ({ children }) => {
    const [isReadMore, setIsReadMore] = useState(true);
    const { winWidth } = useScreenSize();
    const toggleReadMore = () => {
      setIsReadMore(!isReadMore);
    };
    return (
      <>
        {isReadMore ? `${winWidth <= 720 ? children.slice(0, 100):children.slice(0, 180)}`: children}
        {winWidth <= 720 && children.length >= 100 ?<span onClick={toggleReadMore} className={style.read_or_hide}>
            {isReadMore ? "...more" : " less"}
        </span>:null}
        {winWidth > 720 && children.length >= 180 ?<span onClick={toggleReadMore} className={style.read_or_hide}>
            {isReadMore ? "...more" : " less"}
        </span>:null}
      </>
    );
};
export default ReadMore;