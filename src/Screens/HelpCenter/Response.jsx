import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { createResponse } from '../../store/reducer/complaintReducer';
import style from './ComplaintDetail.module.css';
import useScreenSize from '../../Hooks/ScreenSize';

const Response = ({complaintId}) => {
    const [response, setResponse] = useState('');
    const { winWidth } = useScreenSize();
    const dispatch = useDispatch();
    const handleReply = async(id) =>{
        if(response !== '' && id !== undefined){
            await dispatch(createResponse({id, resValue:response}));
            setResponse('');
        }else {
            toast.error('Response is not valid');
        }
    }
    
    return (
        <div className={`${style.second_col}`}>
            <div className={style.replyCard}>
                <div className={style.cd_input_box}>
                    <textarea 
                        className={style.cd_input}
                        type={'text'}
                        onChange={(e) => setResponse(e.target.value)}
                        value={response}
                        placeholder={'Give response here...'}
                        rows={winWidth <= 720 ? 3:8}
                        style={{width: "100%"}}             
                    />        
                </div> 
            </div>    
            <div className={style.reply_btn}>
                <button 
                    onClick={()=>handleReply(complaintId)}  
                    className={`${style.submit_btn}`} 
                >Submit</button>
            </div>
        </div>
    )
}

export default Response;