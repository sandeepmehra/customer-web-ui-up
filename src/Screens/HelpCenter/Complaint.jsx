import React, { useState } from 'react';
import { Title } from "../../Components/StyleComponents/StyledComponents";
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import style from './Complaint.module.css';
import { AiOutlineLeft } from 'react-icons/ai';
import { FaqItem } from './FaqItem';
import { toast } from 'react-toastify';
import { createComplaint } from '../../store/reducer/complaintReducer';


const ComplaintPage = () => {
    let navigate = useNavigate();
    const dispatch = useDispatch();
    const [complaint, setComplaint] = useState({
        subject: '',
        description: ''
    });
    const inputHandler = (e) => {
        setComplaint({
            ...complaint,
            [e.target.name] : e.target.value, 
        });
    }

    const submitComplaint = async() => {
        if(complaint.subject !== '' && complaint.description !== ''){
            await dispatch(createComplaint(complaint));
            navigate('/help-center');
        }else{
            toast.error('Subject or description are empty');
        }   
    }
    return (
        <div className={style.main_container}>
            <div className={style.heading_container}>
                <div className={style.bck_page_icon}>
                    <AiOutlineLeft onClick={()=>navigate('/help-center')} />
                </div>
                <Title className={style.complaint_title}>COMPLAINTS</Title>
            </div>
            <div className={style.complaint_card_container}>
                <div className={style.complaint_page}>
                    <div className={style.complaint_form}>
                        <label className={style.title}>Complaint Title</label>
                        <div className={style.input_container}>
                            <input 
                                type={"text"}
                                name={"subject"}
                                value={complaint.subject}
                                className={style.input}
                                onChange={inputHandler}
                                placeholder={'Give your complaint a title'}  
                            />
                        </div>
        
                        <label className={style.title}>Complaint Body</label>
                        <div className={style.input_container}>
                            <textarea 
                                type={"text"}
                                name={"description"}
                                className={style.textarea}
                                value={complaint.description}
                                onChange={inputHandler}
                                placeholder={'Write your comment here'} 
                                rows={4}
                            />
                        </div>

                        <div className={style.submit_btn}>
                            <button onClick={submitComplaint}>Submit</button>
                        </div>

                    </div> 
                    <div className={style.faq_container_web}>
                        <FaqItem title={"FAQs"} />
                    </div>
                </div>
            </div>
            <div className={style.faq_container_mobile}>
                <FaqItem title={"FAQs"} />
            </div>
        </div>
    )
}

export default ComplaintPage;