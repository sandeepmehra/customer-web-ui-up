import React, { useEffect } from "react";
import style from "./Category.module.css";
import Card from "../../Components/Card/Card";
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import Loader from "../../Components/Loader/Loader";
import { getProductsByCategory } from "../../store/reducer/categoryReducer";
import { addToWishlistItem, removeWishlistItem } from "../../store/reducer/userReducer";
import { setError } from "../../store/reducer/errorReducer";

const Category = () => {
    const {categoryName} = useParams(); 
    const { categoryProduct, isLoading, isError, error } = useSelector((state) => state.categorySlice);
    console.log("categoryProduct: ", categoryProduct);
    const cart = useSelector(state => state.cartSlice);
    const dispatch = useDispatch();
    const handleWishlist = (e, variantId, category)=>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(addToWishlistItem({id: variantId, category: category, isProductDetail: false}));
    }
    const handleRemoveWishlist = (e, variantId, category)=>{
        e.preventDefault();
        e.stopPropagation();
        dispatch(removeWishlistItem({id: variantId, category: category, isProductDetail: false}));
    }
    useEffect(()=>{
        dispatch(setError(isError || cart.isError ? error || cart.error : null));
    }, [isError, error, dispatch, cart.isError, cart.error]);
    
    useEffect(()=>{
        dispatch(getProductsByCategory(categoryName));
    }, [dispatch, categoryName]);
    if(isLoading){
        return <Loader />
    }
    return (
        <div className={style.home_container}>
            { (categoryProduct !== null && categoryProduct !== undefined && categoryProduct.length !== 0 ) ? 
                <div className={`${style.category_home} ${style.home_section }`}>
                    <div className={style.cover_image}>
                        <img src={categoryProduct.banner_url} alt="HomeCoverImage"/>
                        <p className={style.category_name}>{categoryName}</p>
                    </div>
                    <div className={style.product_length}>
                        <p>{categoryProduct.detail_list !== null && categoryProduct.detail_list !== undefined && categoryProduct.detail_list.length !== 0 ? categoryProduct.detail_list.length + ' products': null}</p>
                    </div>
                    <div className={style.page_container}>
                        <div className={style.products_section}>
                            {categoryProduct.detail_list !== null && categoryProduct.detail_list !== undefined && categoryProduct.detail_list.length !== 0 && categoryProduct.detail_list.map((product, i) => (
                                <Card 
                                    product={product} 
                                    key={i} 
                                    index={i} 
                                    handleWishlist={handleWishlist}
                                    handleRemoveWishlist={handleRemoveWishlist}
                                />
                            ))}
                        </div>
                    </div>
                </div>
                : <> Category doesn't get Product List</>
            }
        </div>
    )
}

export default Category;
