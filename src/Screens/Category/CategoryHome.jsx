import React, { useEffect } from "react";
import style from "./CategoryHome.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";
import Loader from "../../Components/Loader/Loader";
import { setError } from "../../store/reducer/errorReducer";
import { getAllCategories } from "../../store/reducer/categoryReducer";
import { Title } from "../../Components/StyleComponents/StyledComponents";



const CategoryHome = (props) => {
    const { categories, isLoading, isError, error } = useSelector((state) => state.categorySlice);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleClick = (category_name) => {
        navigate(`/category/${String(category_name)}`)
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    
    useEffect(()=>{
        dispatch(getAllCategories());
    }, [dispatch]);
    if(isLoading){
        return <Loader />
    }
    if(props.isHome){
        return (
            <div className={style.heading_home}>
                <Title>COLLECTIONS</Title>
                <div className={style.categories_home}>
                    {categories !== undefined && categories.category_list !== undefined && categories.category_list.length !== 0 ? categories.category_list?.map((element, i) => (
                        <div key={i} className={style.category_card_home} onClick={()=>handleClick(element.category_name)}>
                            <div className={style.category_image_home}>
                                <img src={element.category_image} alt={element.category_image} />
                            </div>
                            <div className={style.category_name_home}>{element.category_name}</div>
                        </div>
                    )): 'No Category Found'}
                </div>
            </div>
        )
    }
    return (
        <div className={style.main_container}>
                <div className={style.categories_page}>
                    <div className={style.title_section}>
                        COLLECTIONS
                    </div>
                    <div className={style.page_container}>
                        {categories !== undefined && categories.category_list !== undefined && categories.category_list.length !== 0 ? categories.category_list?.map((element, i) => (
                            <div key={i} className={style.category_card} onClick={()=>handleClick(element.category_name)}>
                                <div className={style.category_image}>
                                    <img src={element.category_image} alt={element.category_image} />
                                </div>
                                <div className={style.category}>{element.category_name}</div>
                            </div>
                        )): 'No Category Found'}
                    </div>
                </div>
        </div>
    )
}


export default CategoryHome;