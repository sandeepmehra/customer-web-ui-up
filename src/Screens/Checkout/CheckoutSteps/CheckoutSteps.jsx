import React from 'react';
import { Step, StepLabel, Stepper } from '@material-ui/core'

const CheckoutSteps = (props) => {
  const steps = [
    {
      label: "Info"
    },
    {
      label: "Shipping"
    },
    {
      label:"Payment"
    }
  ];
  return (
    <Stepper activeStep={props.activeStep} alternativeLabel>
        {steps.map((item, i)=> (
            <Step key={i}>
                <StepLabel>{item.label}</StepLabel>
            </Step>
        ))}
    </Stepper>
  )
}

export default CheckoutSteps;