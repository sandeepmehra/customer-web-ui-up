import React, { useEffect, useState } from 'react';
import { FaLessThan } from 'react-icons/fa';
import style from "./Address.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { diff } from '../../../utils/CompareTwoObject';
import CheckoutSteps from '../CheckoutSteps/CheckoutSteps';
import { toast } from 'react-toastify';
import Cart from '../../Cart/Cart';
import { setError } from '../../../store/reducer/errorReducer';
import { updateAddress } from '../../../store/reducer/addressReducer';

const UpdateAddressPage = () => {
    const dispatch = useDispatch();
    const {id} = useParams();
    const navigate = useNavigate();
    const {buyNow} = useSelector(state => state.cartSlice);
    const {addressInfo, isError, error} = useSelector((state) => state.addressSlice);  
    const [addressData, setAddressData] = useState({
        name: '',
        mobile_number: '',
        city: '',
        street: '',
        state: '',
        pincode: ''
    });
    const inputHandler = (e) => {
        setAddressData({
            ...addressData,
            [e.target.name] : e.target.value, 
        });
    }
    
    const handleClick = async(e, updatedDetail) => {
        e.preventDefault();
        if(updatedDetail === 'Please reload the page') {
            return toast.error('Please reload the page');
        }
        let newObj = {};
        for(let key in updatedDetail){ 
            if(key === 'updated'){
                for(let attribute in updatedDetail.updated){
                    if(attribute){
                        newObj[`${attribute}`] = updatedDetail.updated[`${attribute}`];
                    }
                }
            }
        }
        try{
            if(Object.entries(newObj).length !== 0){
                const updatedAddress = await dispatch(updateAddress({value: newObj, id, addressData:addressData})).unwrap();
                console.log("updated Address", updatedAddress);
                if(updatedAddress.status === 204){
                    navigate(-1);
                }
            } else {
                toast.error('Nothing to update');
            }
        } catch(error){
            toast.error('Error in update Address');
        }
    }
    const handleBackbutton = () => {
        navigate(-1);
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    console.log("addressInfo: ", addressInfo);
    useEffect(()=>{
        if(addressInfo === undefined || addressInfo.length === 0){
            navigate(`/address/change/add`);
        }
        setAddressData(addressInfo.find(item => item.id === id))
    }, [addressInfo, dispatch, id, navigate]);
    return (
        <div className={style.address_page}>
            <CheckoutSteps activeStep={0}/>
            <div className={style.main_card}>
                <div className={style.cart_container}>
                    <div className={style.details_container}>
                        <div className={style.add_address_section}>
                            <div className={style.address_label_div}>
                                <div className={style.back_btn} onClick={handleBackbutton}>
                                    <FaLessThan />
                                </div>
                                <label className={style.address_label}> Edit Delivery Address</label>
                            </div>
                            <div className={style.address_form}>
                                <div className={style.address_form_inputs}>
                                    <input 
                                        placeholder={'Name'}
                                        type={"text"}
                                        name={"name"}
                                        defaultValue={addressData.name}
                                        autoComplete="off"
                                        onChange={inputHandler} 
                                        required 
                                    />
                                    <input 
                                        placeholder={'Phone Number'}
                                        type={"text"}
                                        name={"mobile_number"}
                                        defaultValue={addressData.mobile_number}
                                        autoComplete="off"
                                        onChange={inputHandler} 
                                        required 
                                    />
                                </div>
                                <div className={style.address_form_inputs}>
                                    <textarea 
                                        rows="4"   
                                        className='width-50' 
                                        placeholder={'Address'}
                                        type={"text"}
                                        name={"street"}
                                        defaultValue={addressData.street}
                                        autoComplete="off"
                                        onChange={inputHandler} 
                                        required 
                                    />
                                    <div className={style.address_form_inputs_column}>
                                        <input 
                                            placeholder={'City/District/Town'}
                                            type={"text"}
                                            name={"city"}
                                            defaultValue={addressData.city}
                                            autoComplete="off"
                                            onChange={inputHandler} 
                                            required 
                                            className={style.input_address} 
                                        />
                                        <input 
                                            placeholder={'State'}
                                            type={"text"}
                                            name={"state"}
                                            defaultValue={addressData.state}
                                            autoComplete="off"
                                            onChange={inputHandler} 
                                            required 
                                            className={style.input_address}
                                        />
                                    </div>
                                </div>
                                <div className={style.address_form_inputs}>
                                    <input 
                                        placeholder={'PIN Code'}
                                        type={"text"}
                                        name={"pincode"}
                                        defaultValue={addressData.pincode !== '0' ? addressData.pincode : ''}
                                        autoComplete="off"
                                        onChange={inputHandler} 
                                        required 
                                    />
                                </div>
                                <div className={style.btn_div}>
                                    <div className={style.update_btn}>
                                        <button 
                                            onClick={(e)=>handleClick(e, diff(addressInfo.find(item => item.id === id), addressData))}
                                        >Save</button>
                                    </div>
                                    <div className={style.cancel_btn}>
                                        <button onClick={handleBackbutton}>Cancel</button>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className={style.price_container}>
                        {Object.keys(buyNow).length !== 0 ? 
                            <Cart onlyBuyNowPrice={true} totalAmount={buyNow.total_price} cartDiscountTotal={buyNow.discount_price} />
                            :<Cart onlyCartPrice={true} />
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UpdateAddressPage;