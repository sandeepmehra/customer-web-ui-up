import React, { useEffect, useState } from 'react';
import { FaLessThan } from 'react-icons/fa';
import style from "./Address.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import CheckoutSteps from '../CheckoutSteps/CheckoutSteps';
import { toast } from 'react-toastify';
import { createAddress } from '../../../store/reducer/addressReducer';
import Cart from '../../Cart/Cart';
import { setError } from '../../../store/reducer/errorReducer';

const AddressPage = (props) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {buyNow} = useSelector(state => state.cartSlice);
    const {isError, error} = useSelector(state => state.addressSlice);  
    const [addressData, setAddressData] = useState({
        name: '',
        mobile_number: '',
        city: '',
        street: '',
        state: '',
        pincode: ''
    });
    const inputHandler = (e) => {
        setAddressData({
            ...addressData,
            [e.target.name] : e.target.value, 
        });
    }
    const handleClick = async(e) => {
        e.preventDefault();
        try{
            if(addressData.name !== '' && addressData.mobile_number !== '' && addressData.city !== '' && addressData.street !== '' && addressData.state !== '' && addressData.pincode !== ''){
                const addAddress = await dispatch(createAddress(addressData)).unwrap();
                if(addAddress.status === 201){
                    navigate(props.isAddNewAddress ? '/account/address/all' : '/address/change/add');
                }
            }else {
                toast.error('Nothing to save');
            }
        }catch(error){
            toast.error('Address is not Created. Try again!!!');
        }
    }
    const handleBackbutton = () => {
        navigate(-1);
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    const addressform = () => (
        <div className={style.address_form}>
            <div className={style.address_form_inputs}>
                <input 
                    placeholder={'Name'}
                    type={"text"}
                    name={"name"}
                    value={addressData.name}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
                <input 
                    placeholder={'Phone Number'}
                    type={"text"}
                    name={"mobile_number"}
                    value={addressData.mobile_number}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
            </div>
            <div className={style.address_form_inputs}>
                <textarea 
                    rows="4"   
                    className='width-50' 
                    placeholder={'Address'}
                    type={"text"}
                    name={"street"}
                    value={addressData.street}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
                <div className={style.address_form_inputs_column}>
                    <input 
                        placeholder={'City/District/Town'}
                        type={"text"}
                        name={"city"}
                        value={addressData.city}
                        autoComplete="off"
                        onChange={inputHandler} 
                        required 
                        className={style.input_address} 
                    />
                    <input 
                        placeholder={'State'}
                        type={"text"}
                        name={"state"}
                        value={addressData.state}
                        autoComplete="off"
                        onChange={inputHandler} 
                        required 
                        className={style.input_address}
                    />
                </div>
            </div>
            <div className={style.address_form_inputs}>
                <input 
                    placeholder={'PIN Code'}
                    type={"text"}
                    name={"pincode"}
                    value={addressData.pincode}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
            </div>
            <div className={style.btn_div}>
                <div className={style.shipping_btn}>
                    <button onClick={(e) =>handleClick(e)}>Save Address</button>
                </div>
                <div className={style.cancel_btn}>
                    <button onClick={handleBackbutton}>Cancel</button>
                </div>
                
            </div>
        </div>
    )
    if(props.isAddNewAddress){
        return addressform();
    }
    return (
        <div className={style.address_page}>
            <CheckoutSteps activeStep={0}/>
            <div className={style.main_card}>
                <div className={style.cart_container}>
                    <div className={style.details_container}>
                        <div className={style.add_address_section}>
                            <div className={style.address_label_div}>
                                <div className={style.back_btn} onClick={handleBackbutton}>
                                    <FaLessThan />
                                </div>
                                <label className={style.address_label}>Add Delivery Address</label>
                            </div>
                            {/* Add Adress Form */}
                            {addressform()}
                        </div>
                    </div>
                    <div className={style.price_container}>
                        {Object.keys(buyNow).length !== 0 ? 
                            <Cart onlyBuyNowPrice={true} totalAmount={buyNow.total_price} cartDiscountTotal={buyNow.discount_price} />
                            :<Cart onlyCartPrice={true} />
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddressPage;