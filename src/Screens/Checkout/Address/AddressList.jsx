import React, { useEffect } from 'react';
import { FaLessThan } from 'react-icons/fa';
import style from "./Address.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import CheckoutSteps from '../CheckoutSteps/CheckoutSteps';
import { getAllAddressList } from '../../../store/reducer/addressReducer';
import { SetSelectedAddress } from '../../../store/reducer/cartReducer';
import Cart from '../../Cart/Cart';
import Loader from '../../../Components/Loader/Loader';

const GetAllAddress = () => {
    const {addressInfo, isLoading} = useSelector((state) => state.addressSlice);
    const {selectedAddress, buyNow} = useSelector(state => state.cartSlice);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleUpdate = (id) => {
        navigate(`/address/update/${id}`);
    }
    const handleClick = () => {
        navigate('/address')
    }
    const handleSelectedAddress = (e) => {
        e.preventDefault();
        const chooseAddressInfo = addressInfo.filter(element => element.id === e.target.value);
        dispatch(SetSelectedAddress(chooseAddressInfo[0]));
    }
    const handleDeliverAddress = (e) => {
        navigate('/checkout');
    }
    const handleBackbutton = () => {
        navigate('/checkout');
    }
    useEffect(()=>{
        dispatch(getAllAddressList());
    }, [dispatch]);
    
    const add_address_card = () => (
        <div className={style.container_address}>
            {addressInfo !== undefined && addressInfo.lenght !== 0 ? addressInfo.map((element, i)=>(
                <div key={i} className={style.address_card_container}>
                    <div className={style.radio_button}>
                        <input type={"radio"} value={element.id} checked={selectedAddress.id === element.id} onChange={handleSelectedAddress} />
                    </div>
                    <div className={style.address_information_card}>
                        <div>{element.name}</div>
                        <div>
                            <span>{element.street}, {element.city},</span><br />
                            <span>{element.state}-{element.pincode}</span>
                        </div>
                        <div>
                            +91 {element.mobile_number}
                        </div>
                        
                    </div>
                    <div className={style.edit_button} onClick={() => handleUpdate(element.id)}>Edit</div>
                </div>
            )): null}
            <div className={style.add_buttons}>
                <button className={style.add_address_btn} onClick={handleClick}>Add a new address</button>
                <button className={style.deliver_btn} onClick={(e)=>handleDeliverAddress(e)}>Deliver here</button> 
            </div>
        </div>
    )

    return (<>
        {isLoading ? <Loader /> : 
        <div className={style.address_page}>
            <CheckoutSteps activeStep={0}/>
            <div className={style.main_card}>
                <div className={style.cart_container}>
                    <div className={style.details_container}>
                        <div className={style.add_address_section}>
                            <div className={style.address_label_div}>
                                <div className={style.back_btn} onClick={handleBackbutton}>
                                    <FaLessThan />
                                </div>
                                <label className={style.address_label}>Select Address (1)</label>

                            </div>
                            
                            <div className={style.add_address_link} onClick={handleClick} >Add a new address</div>
                            
                            {/* GET Adress*/}
                            {add_address_card()}

                        </div>
                    </div>
                    <div className={style.price_container}>
                        {Object.keys(buyNow).length !== 0 ? 
                            <Cart onlyBuyNowPrice={true} totalAmount={buyNow.total_price} cartDiscountTotal={buyNow.discount_price} />
                            :<Cart onlyCartPrice={true} />
                        }
                    </div>
                </div>
            </div>
        </div>}
        </>
    )
}

export default GetAllAddress;