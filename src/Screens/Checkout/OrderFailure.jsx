import { useNavigate } from 'react-router-dom';
import style from './OrderSuccess.module.css'

const OrderFailure = () => {
    const navigate = useNavigate();
    const handleShopping = () =>{
        navigate('/');
    }
    return (
        <>
            <div className={style.success_msg}>
                <p className={style.p_heading}  style={{color: 'red'}}>Sorry, Your order has been Failed!, Try Again </p>
                <br />
                <div className={style.btn}>
                    <button onClick={handleShopping}>Continue Shopping</button>
                </div> 
            </div>
        </>
    )
}

export default OrderFailure;