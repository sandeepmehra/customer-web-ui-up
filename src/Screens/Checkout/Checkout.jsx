import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import style from './Checkout.module.css';
import { getAllAddressList } from '../../store/reducer/addressReducer';
import { SetSelectedAddress } from '../../store/reducer/cartReducer';
import { toast } from 'react-toastify';
import CommonPage from './Common';
import Loader from '../../Components/Loader/Loader';

const CheckoutPage = () => {
    const {addressInfo, isLoading} = useSelector((state) => state.addressSlice);
    console.log("isLoading: ", addressInfo);
    const {cartData, selectedAddress, buyNow} = useSelector(state => state.cartSlice);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleShipping = () => {
        if(selectedAddress.street !== '' && selectedAddress.city !== '' && selectedAddress.state !== '' && selectedAddress.pincode !== '' && selectedAddress.mobile_number !== ''){
            navigate('/shipping', { replace: true });
        }else{
            toast.error("Please fill your Address");
        }
    }
    const handleReturntToCart = () => {
        navigate('/cart', { replace: true });
    }
    const handleChangeOrAddAddress = () => {
        navigate('/address/change/add', {replace: true});
    }
    console.log("cart.length buyNow: Checkeout page ", cartData, buyNow);
    useEffect(()=>{
        if(cartData === undefined && Object.keys(buyNow).length === 0){
            navigate('/')
        }
    }, [buyNow, cartData, dispatch, navigate]);
    useEffect(()=>{
        dispatch(getAllAddressList());
    }, [dispatch]);
    useEffect(()=>{
        if(addressInfo !== undefined && addressInfo[0] !== undefined && Object.keys(selectedAddress).length === 0){
            dispatch(SetSelectedAddress(addressInfo[0]));
        }
    },[addressInfo, dispatch, selectedAddress]);
    if(isLoading){
        return <Loader />
    }
    return (
        <CommonPage >
            <div className={style.outline}>
                <div className={style.details}>
                    { (selectedAddress !== undefined && Object.keys(selectedAddress).length !== 0)?
                        <>
                            {selectedAddress.mobile_number !== '' && selectedAddress.name !== '' && <div>
                                {selectedAddress.name}
                            </div>}
                            {selectedAddress.street !== '' && selectedAddress.city !== '' && selectedAddress.state !== '' && selectedAddress.pincode !== '' &&<div>
                                {selectedAddress.street}, {selectedAddress.city}, {selectedAddress.state}, {selectedAddress.pincode}
                            </div>}
                            {selectedAddress.mobile_number !== '' &&<div>
                                +91 {selectedAddress.mobile_number}
                            </div>}
                            <div className={style.address}>
                                <button onClick={handleChangeOrAddAddress}>Change or Add Address</button>
                            </div>
                        </>:<>
                            <div>No Address Found</div>
                            <div className={style.address}>
                                <button onClick={()=>navigate('/address')}>Add Address</button>
                            </div>
                        </>    
                    }
                </div>
            </div>
            <div className={style.display_btns}>
                <div className={style.shipping_btn}>
                    <button onClick={handleShipping}>Continue to Shipping</button>
                </div>
                <div className={style.return_btn}>        
                    <button onClick={handleReturntToCart}>
                        Return to cart
                    </button>
                </div>
            </div >
        </CommonPage>
    )
}

export default CheckoutPage;