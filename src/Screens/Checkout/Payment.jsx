import React, { useEffect } from 'react';
import { Title } from "../../Components/StyleComponents/StyledComponents";
import style from "./Payment.module.css"
import { useNavigate } from 'react-router-dom';
import PaytmImg from "../../assets/images/checkout/paytm.svg";
import PayUIndia from "../../assets/images/checkout/payu_india.svg";
import Meastro from "../../assets/images/checkout/maestro.svg";
import Meastro2 from "../../assets/images/checkout/maestro2.svg";
import Visa from "../../assets/images/checkout/visa.svg";
import { useDispatch, useSelector } from 'react-redux';
import customerApi from '../../api';
import Cookies from 'js-cookie';
import { ResetBuyNowOrAddress } from '../../store/reducer/cartReducer';
import CommonPage from './Common';

const PaymentsPage = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {selectedAddress, buyNow} = useSelector(state => state.cartSlice);
    let data = {
        "shipping_address_id": selectedAddress.id,
        "product_detail": buyNow
    }
    const handleChange = () => {
        console.log("change clicked")
    }
   
    function isDate(val) {
        return Object.prototype.toString.call(val) === '[object Date]'      // Cross realm comptatible
    }
  
    function isObj(val) {
        return typeof val === 'object'
    }
      
    function stringifyValue(val) {
        if (isObj(val) && !isDate(val)) {
            return JSON.stringify(val)
        } else {
            return val
        }
    }
      
    function buildForm({action, params}) {
        const form = document.createElement('form')
        form.setAttribute('method', 'post')
        form.setAttribute('action', action)
        Object.keys(params).forEach(key => {
            const input = document.createElement('input')
            input.setAttribute('type', 'hidden')
            input.setAttribute('name', key)
            input.setAttribute('value', stringifyValue(params[key]))
            form.appendChild(input)
        });
        return form;
    }
      
    function post(details) {
        const form = buildForm(details)
        document.body.appendChild(form)
        form.submit()
        form.remove()
    }
    const makePayment = async(e) => {
        e.preventDefault();
        try{
            let response = await customerApi.post(
                '/place-order', 
                JSON.stringify(data), 
                {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}}
                );
            const processData = await response.data;
            console.log("processData: ", processData);
            var information = {
                action: "https://securegw-stage.paytm.in/order/process",
                params: processData
            }
            console.log("information: ", information)
            post(information);
            dispatch(ResetBuyNowOrAddress());
        }catch(error){
            console.log("error: ", error);
        }
    }
    const handleUpdate = (id) => {
        navigate(`/address/update/${id}`);
    } 
    const handleReturntToShipping = () => {
        navigate('/shipping')
    }
    useEffect(()=>{
        if(selectedAddress !== undefined && Object.keys(selectedAddress).length === 0){    
            navigate('/cart');
        }
    }, [navigate]);
    const displayButton = () => (
        <div className={style.button_section}>
            <div className={style.shipping_btn}>
                <button onClick={(e)=>makePayment(e)}>
                    Complete Order
                </button>
            </div>
            <div className={style.return_btn}>
                <button onClick={handleReturntToShipping}>
                    Return to Shipping
                </button>
            </div>

        </div>
    )

    const shippingDisplay = (label, value) => (
        <div className={`${style.shipping_display} ${style.outline}`}>
            {/* Shipping contact number */}
            <div className={style.shipping_card}>
                <div className={style.row_container}>
                    <div className={style.label_white}>{label}</div>
                    <div className={style.change_link} onClick={()=>handleUpdate(selectedAddress.id)}>Change</div>
                </div>
                <label className={style.p_value}>+91 {selectedAddress.mobile_number}</label>
            </div>
            <hr className={style.hr}/>
            {/* Shipping Address */}
            <div className={style.shipping_card}>
                <div className={style.row_container}>
                    <div className={style.label_white}>Ship to</div>
                    <div className={style.change_link} onClick={()=>handleUpdate(selectedAddress.id)}>Change</div>
                </div>
                <label className={style.p_value}>{selectedAddress.street}, {selectedAddress.city}, {selectedAddress.state}, {selectedAddress.pincode}</label>
            </div>
            <hr className={style.hr}/>
            <div className={style.shipping_card}>
                <div className={style.row_container}>
                    <div className={style.label_white}>Method</div>
                    <div className={style.change_link}>Change</div>
                </div>
                <label className={style.p_value}>Free Shipping</label>
            </div>
        </div>
    )

    const selectPayment = () => (
        <div className={style.payment_option}>
            <label><Title>Payment</Title></label>
            <div className={`${style.shipping_display} ${style.outline}`}>
                {/* Shipping contact number */}
                <div className={style.shipping_card}>
                    <div className={style.payment_row}>
                        {/* <input type="radio" checked="checked" name="radio" value="same_as_shipping" /> */}
                        <div className={style.payment_paytm}>
                            <img src={PaytmImg} alt="paytm" />
                            <div className={style.payment_inner_row}>
                                <img src={Visa} alt="visa" />
                                <img src={Meastro} alt="Meastro" />
                                <img src={Meastro2} alt="Meastro" />
                                <div className={style.label_white}>and more...</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr className={style.hr}/>
                {/* Shipping Address */}
                <div className={style.shipping_card}>
                    <div className={style.payment_message}>
                        After clicking <b>Complete Order,</b> you will be redirected to payment page to complete your purchase securely
                    </div>
                </div>
                <hr className={style.hr}/>
                <div className={style.shipping_card}>
                    <div className={style.payment_payu_row}>
                        {/* <input type="radio" checked="checked" name="radio" value="same_as_shipping" /> */}
                        <div className={style.payment_payu}>
                            <img src={PayUIndia} alt="PayUIndia" />
                            <div className={style.payment_inner_row}>
                                <img src={Visa} alt="visa" />
                                <img src={Meastro2} alt="Meastro" />
                                <div className={style.label_white}>and more...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
    const selectAddress = () => (
        <>
            <div className={style.address_section}>
                <Title>Billing Address</Title>
                <p>Select the biling address that matches your card or payment method</p>
                <div className={style.outline}>
                    <div className={style.address_row_container}>
                        <input type="radio" checked="checked" name="radio" value="same_as_shipping" className='width-50' />
                        <p>Same as shipping address</p>
                    </div>
                    <hr className={style.hr}/>
                    <div className={style.address_row_container}>
                        <input type="radio" name="radio" value="newaddress" disabled />
                        <p>Use a different billing address</p>
                    </div>
                </div>
            </div>
            <div className={style.buttons_webview}>
                {displayButton()}
            </div>
        </>
    );
    return (
        <CommonPage>
            {shippingDisplay("Contact", "+91 XXXX XXXX XX")}
            {selectPayment()}
            {selectAddress()}
        </CommonPage>             
    )
}

export default PaymentsPage;