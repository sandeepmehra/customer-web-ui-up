import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Title } from "../../Components/StyleComponents/StyledComponents";
import CommonPage from './Common';
import style from "./Shipping.module.css";

const ShippingPage = () => {
    const navigate = useNavigate();
    const {selectedAddress, buyNow,} = useSelector(state => state.cartSlice);
    const handleUpdateAddress = (id) => {
        navigate(`/address/update/${id}`);
    }
    const handlePaymentLink = () => {
        navigate("/payment")
    }
    const handleReturntToInformation = () => {
        navigate('/checkout')
    }
    console.log("selectedAddress: ", selectedAddress);
    useEffect(()=>{
        if(selectedAddress !== undefined && Object.keys(selectedAddress).length === 0){    
            navigate('/address');
        }
    }, [navigate, selectedAddress]);
    const shippingDisplay = (label) => (
        <div className={style.shipping_display}>
            {/* Shipping contact number */}
            <div className={style.shipping_card}>
                <div className={style.shipping_row}>
                    <div className={style.label_white}>{label}</div>
                    <div className={style.change_link} onClick={()=>handleUpdateAddress(selectedAddress.id)}>Change</div>
                </div>
                <label>+91 {selectedAddress.mobile_number}</label>
            </div>
            <hr className={style.hr}/>
            {/* Shipping Address */}
            <div className={style.shipping_card}>
                <div className={style.shipping_row}>
                    <div className={style.label_white}>Ship to</div>
                    <div className={style.change_link} onClick={()=>handleUpdateAddress(selectedAddress.id)}>Change</div>    
                </div>
                <label>{selectedAddress.street}, {selectedAddress.city}, {selectedAddress.state}, {selectedAddress.pincode}</label>
            </div>
            <hr className={style.hr}/>
            <div className={style.shipping_card}>
                <div><Title>Shipping Method</Title></div>
                <div className={style.shipping_method_card}>
                    <div className={style.shipping_method_col}>
                        <div>
                            <input type="radio" checked="checked" name="radio" value="free" readOnly/>
                        </div>
                        <div className={style.free_shipping}>
                            Free Shipping
                        </div>
                    </div>
                    <div className={style.free}>
                        Free
                    </div>     
                </div>
            </div>
        </div>
    )

    return (
        <CommonPage >
            <div className={style.outline}>
                {/* DIsplay Deyails */}
                {shippingDisplay("Contact")}
            </div>
            <div className={style.buttons_webview}>
                <div className={style.button_section}>
                    <div className={style.shipping_btn}>
                        <button onClick={handlePaymentLink}> 
                            Continue to Payment
                        </button>
                    </div>
                    <div className={style.return_btn}>
                        <button onClick={handleReturntToInformation}>
                            Return to information
                        </button>
                    </div>
                </div>
            </div>
            <div className={style.buttons_mobileview}>
            <div className={style.button_section}>
                <div className={style.shipping_btn}>
                    <button onClick={handlePaymentLink}> 
                        Continue to Payment
                    </button>
                </div>
                <div className={style.return_btn}>
                    <button onClick={handleReturntToInformation}>
                        Return to information
                    </button>
                </div>
            </div>
        </div>
        </CommonPage>
    )
}

export default ShippingPage;