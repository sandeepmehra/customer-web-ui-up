import { useNavigate } from 'react-router-dom';
import style from './OrderSuccess.module.css'

const OrderSuccess = () => {
    const navigate = useNavigate();
    const handleShopping = () =>{
        navigate('/');
    }
    return (
        <>
            <div className={style.success_msg}>
                <p className={style.p_heading}>Your order has been placed successfully!</p>
                <br />
                <p className={style.p_message}>You can track the status of your order using the link sent over email.</p>
                <br />
                <div className={style.btn}>
                    <button onClick={handleShopping}>Continue Shopping</button>
                </div> 
            </div>
        </>
    )
}

export default OrderSuccess;