import React from 'react';
import {  useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import style from './Common.module.css';
import CheckoutSteps from './CheckoutSteps/CheckoutSteps';
import Cart from '../Cart/Cart';
import ProductPrice from '../Product/ProductPrice';


const CommonPage = ({children}) => {
    const location = useLocation();
    const {buyNow} = useSelector(state => state.cartSlice);
    console.log("Common Page buyNow: ", buyNow);
    return (
        <div className={style.main_container}>
            <CheckoutSteps activeStep={location.pathname === '/checkout' ? 0 : location.pathname === '/shipping' ? 1 : location.pathname === '/payment'? 2: null }/>
            <div className={style.checkout_page}>
                <div className={style.page_container}>
                    <div className={style.cart_container}>
                        <div className={style.details_container}>
                            {children}  
                        </div>
                        <div className={style.price_container}>
                            { Object.keys(buyNow).length === 0 ? 
                                <Cart onlyCartPrice={true} /> :
                                <ProductPrice showOrderSummary={false} totalAmount={buyNow.total_price} cartDiscountTotal={buyNow.discount_price} />
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CommonPage;