export const diff = (oldObj, newObj) => {
    const updated = {};
    if(JSON.stringify(Object.keys(oldObj)) !== JSON.stringify(Object.keys(newObj))) return 'Please reload the page';
    for (let key in oldObj) {
        if (oldObj[key] !== newObj[key]) {
            updated[key] = newObj[key];
        }
    }
    return {
        updated
    };
}