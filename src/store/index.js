import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import authReducer from './reducer/authReducer';
import errorReducer from './reducer/errorReducer';
import userReducer from './reducer/userReducer';
import categoryReducer from './reducer/categoryReducer';
import orderReducer from './reducer/orderReducer';
import complaintReducer from './reducer/complaintReducer';
import productReducer from './reducer/productReducer';
import cartReducer from './reducer/cartReducer';
import addressReducer from './reducer/addressReducer';
import ratingReducer from './reducer/ratingReducer';
import homeReducer from './reducer/homeReducer';

export const store = configureStore({
    reducer: {
        error: errorReducer,
        auth: authReducer,
        homeSlice: homeReducer,
        userSlice: userReducer,
        categorySlice: categoryReducer,
        orderSlice: orderReducer,
        complaintSlice: complaintReducer,
        productSlice: productReducer,
        cartSlice: cartReducer,
        addressSlice: addressReducer,
        ratingSlice: ratingReducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});
setupListeners(store.dispatch)
