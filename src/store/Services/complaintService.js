import Cookies from 'js-cookie';
import customerApi from '../../api/index';


const allComplaints = async () => {
    const { data } = await customerApi({method: 'get', url: '/complaints', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const fetchComplaint = async (complaintId) => {
    const { data } = await customerApi({method: 'get', url: `/complaint/${complaintId}`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const postAComplaint = async (complaintObj) => {
    return await customerApi.post('/complaint', complaintObj, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const postAResponse = async (responseId, responseObj) => {
    return await customerApi.post(`/response/${responseId}`, {response: responseObj}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const complaintService = { 
  allComplaints,
  fetchComplaint,
  postAComplaint,
  postAResponse,
}

export default complaintService;
