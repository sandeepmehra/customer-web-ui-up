import axios from 'axios';
import { baseURL } from "../../api";
const fetchProductsById = async (productId) => {
    const { data } = await axios({method: 'get', url: `${baseURL}/product/${productId}`, headers: {'Content-Type': 'application/json'}});       
    return data;
}

const productService = { 
  fetchProductsById
}

export default productService;
