import { baseURL } from '../../api/index';
import Cookies from "js-cookie";
import { ResetAuth, SetAuth } from '../reducer/authReducer';
import { toast } from 'react-toastify';
import axios from 'axios';

const userLogin = async (formValue) => {
    return await  axios.post(`${baseURL}/login`, formValue, {headers: {'Content-Type': 'application/json'}});
}
const userSignUp = async (formValue) => {
    return await axios.post(`${baseURL}/signup/email`, formValue, {headers: {'Content-Type': 'application/json'}});
}
const forgetPassword = async (email) => {
    return await axios.put(`${baseURL}/password/reset`, email, {headers: {'Content-Type': 'application/json'}});
}

const verifyEmail = async (formValue) => {
    return await axios.put(`${baseURL}/verify/email`, formValue, {headers: {'Content-Type': 'application/json'}});
}

const loginWithMobile = async (mobileNumber) => {
    return await axios.post(`${baseURL}/auth/mobile`, mobileNumber, {headers: {'Content-Type': 'application/json'}});
}
const mobileOTPVerify = async (formValue) => {
    return await axios.post(`${baseURL}/verify/otp`, formValue, {headers: {'Content-Type': 'application/json'}});
}

export const logoutAction = () => {
    return (dispatch) => {
        if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
            Cookies.remove('at');
            Cookies.remove('rt');
        }
        return dispatch(ResetAuth());    
    }
}

export const getUser = (dispatch) => {
    console.log("get User called ");
    fetch(`${baseURL}/auth/login/google/success`, {
        method: "GET",
        credentials: "include",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true
        }  
    })
    .then(async (response) => {
        console.log("repsonse: ", response);
        console.log("repsonse.status: ", response.status);
        if (response.status !== 200) throw new Error("authentication has been failed!");
        const data = await response.json();
        console.log("data: ", data)
        Cookies.set('at', response.headers.get('authorization').split(' ')[1],  { sameSite: 'strict', secure: true });
        Cookies.set('rt', data['refresh_token'], { sameSite: 'strict', secure: true });
        dispatch(SetAuth({ at: response.headers.get('authorization').split(' ')[1] }));
    })
    .catch((error) => {
        console.log("error: ", error);
        toast.error((error.response && error.response.data && error.response.data.message) || error.message || error.toString()|| 'Google authentication has been failed');
    });
};


const authService = {
    userLogin,
    userSignUp,
    forgetPassword,
    verifyEmail,
    loginWithMobile,
    mobileOTPVerify 
} 

export default authService;