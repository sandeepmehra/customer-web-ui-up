import axios from 'axios';
import { baseURL } from "../../api";

const getHomeData = async () => {
  let endpoints = [
    '/products/category/Plants/5',
    '/products/category/Pots/5',
    '/products/category/Accessories/5'
  ];
  
  const response = await Promise.allSettled(endpoints.map((endpoint) => axios.get(baseURL+endpoint, {'Content-Type': 'application/json'})));
  return response;
}

const homeService = { 
    getHomeData
}

export default homeService;
