import customerApi from '../../api/index';
import Cookies from "js-cookie";

const allOrders = async () => {
  const { data } = await customerApi({method: 'get', url: '/orders', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
  return data;
}

const oneOrderDetail = async (order_item) => {
  const { data } = await customerApi({method: 'get', url: `/order/${order_item}`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
  return data;
}

const orderService = { 
  allOrders,
  oneOrderDetail
}

export default orderService;

