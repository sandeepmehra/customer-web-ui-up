import { baseURL } from '../../api/index';
import axios from 'axios';
import Cookies from 'js-cookie';

const allCategories = async () => {
  const { data } = await axios({method: 'get', url: `${baseURL}/products/category`, headers:{'Content-Type': 'application/json'}});
  return data;
}
const fetchProductsByCategory = async (category, isAuth) => {
  const { data } = await axios({method: 'get', url: `${baseURL}/products/category/${category}`, headers: {'Authorization': `${ isAuth ?  `Bearer ${Cookies.get('at')}`: undefined}`,'Content-Type': 'application/json'}});
  return data;
}
const fetchProductsByCategoryWithoutVariant = async (category, isAuth) => { 
  const { data } = await axios({
    method: 'get', 
    url: `${baseURL}/products/category/alsolike/${category}`,
    headers: isAuth ? `${{'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}}`: {'Content-Type': 'application/json'}
  }); 
  return data; 
}
const categoryService = { 
  allCategories,
  fetchProductsByCategory,
  fetchProductsByCategoryWithoutVariant
}

export default categoryService;
