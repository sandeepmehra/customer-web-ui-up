import customerApi from '../../api/index';
import Cookies from "js-cookie";

const allCartItem = async () => {
    const { data } = await customerApi({method: 'get', url: '/cart', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const postCartItem = async (productVariantId, quantity) => {
    const data = await customerApi.post(`/cart/${String(productVariantId)}`, {quantity: String(quantity)}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const editAddItem = async (productVariantId) => {
    return await customerApi.put(`/cart/${productVariantId}/add`, {}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const editSubtractItem = async (productVariantId) => {
    return await customerApi.put(`/cart/${productVariantId}/sub`, {}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const deleteCartItem = async (productVariantId) => {
    return await customerApi.delete(`/cart/removeItem/${productVariantId}`, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}


const cartService = { 
    allCartItem,
    postCartItem,
    editAddItem,
    editSubtractItem,
    deleteCartItem
}

export default cartService;