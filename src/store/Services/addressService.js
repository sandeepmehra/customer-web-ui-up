import customerApi from '../../api/index';
import Cookies from "js-cookie";

const allAddress = async () => {
    const { data:{address_info} } = await customerApi({method: 'get', url: '/account/address', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return address_info;
}

const postAddress = async (value) => {
    return await customerApi.post('/account/address', value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const changeAddress = async (value, id) => {
    return await customerApi.put(`/account/${id}/address`, value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}



const addressService = { 
    allAddress,
    postAddress,
    changeAddress,
}

export default addressService;