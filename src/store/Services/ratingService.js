import customerApi from '../../api/index';
import Cookies from "js-cookie";

const postARating = async (order_item_reveiw_id, review, rating) => {
    return await customerApi.post(`/review/${order_item_reveiw_id}`, {review: review, rating: rating}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const postAImage = async (order_item_reveiw_id, formData) => {
    return await customerApi.post(`/uploadfile/${order_item_reveiw_id}`, formData, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const postRatingOrImage = async (order_item_reveiw_id, formData) => {
    return await customerApi.post(`/review/${order_item_reveiw_id}`, formData, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const ratingService = { 
  postARating,
  postAImage,
  postRatingOrImage,
}

export default ratingService;