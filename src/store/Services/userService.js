import customerApi from '../../api/index';
import Cookies from "js-cookie";

const getAccoutDetail =  async() => {
    const { data:{user_info} } = await customerApi({method: 'get', url: '/account', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return user_info;
};
const changeAccountDetail = async(value) =>{
    return await customerApi.put('/account', value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, "Content-Type": "application/json"}});
};
const changePassword =  async(formValue) => {
    return await customerApi.put("/password", formValue, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`,"Content-Type": "application/json"}});
};
const getWishlist =  async() => {
    const data = await customerApi({method: 'get', url: '/wishlist', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
};
const addToWishlist = async (product_variant_id) => {
    return await customerApi.post(`/wishlist/${product_variant_id}`, {}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const removeToWishlist = async (wishlist_id) => {
    console.log("wishlist_id: ", wishlist_id);
    return await customerApi({method: 'delete', url: `/wishlist/${wishlist_id}`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const userService = { 
    getAccoutDetail,
    changeAccountDetail,
    changePassword,
    getWishlist,
    addToWishlist,
    removeToWishlist
}
  
export default userService;