import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import categoryService from '../Services/categoryService'
import homeService from '../Services/homeService';
import productService from '../Services/productService';
import { SetProductDetail } from './productReducer';

// Get home page data
export const resHomeData = createAsyncThunk(
  'home',
  async (productId, thunkAPI) => {
    try {
      const data = await categoryService.allCategories();
      thunkAPI.dispatch(SetCategories(data));
      const response = await productService.fetchProductsById(productId);
      thunkAPI.dispatch(SetProductDetail(response));
      return await homeService.getHomeData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

const initialState = {
  categories: [],
  plantsData:[],
  potsData:[],
  toolsData: [],
  isError: false,
  isSuccess: false,
  isLoading: true,
  error: null,
}

export const homeReducer = createSlice({
  name: 'home',
  initialState,
  reducers: {
    SetCategories(state, action){
      state.categories = action.payload
    },
    ResetHomeError(state){
      state.error = null;
      state.isError = false
      state.isSuccess = false
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resHomeData.pending, (state) => {
        state.isLoading = true
      })
      .addCase(resHomeData.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        action.payload.forEach((item, i)=>{
          const category = {
            0: 'plantsData',
            1: 'potsData',
            2: 'toolsData'
          }
          if(item.status === 'fulfilled'){
            state[category[i]] = item.value.data
          }else{
            state[category[i]] = [];
          }
        })
      })
      .addCase(resHomeData.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
  },
})

export const { SetCategories, ResetHomeError } = homeReducer.actions;
export default homeReducer.reducer;