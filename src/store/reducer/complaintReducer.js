import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { toast } from 'react-toastify';
import complaintService from '../Services/complaintService'

// Get all complaints list
export const getAllComplaints = createAsyncThunk(
    'complaint/all',
    async (_, thunkAPI) => {
        try {
            return await complaintService.allComplaints();
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }
    }
)

// Get a complaint-detail
export const getComplaintDetail = createAsyncThunk(
    'complaint/one',
    async (order_item, thunkAPI) => {
        try {
            return await complaintService.fetchComplaint(order_item);
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj)
        }
    }
)

// post a complaint
export const createComplaint = createAsyncThunk(
    'complaint/create',
    async (value, thunkAPI) => {
      try {
        const response = await complaintService.postAComplaint(value);
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// post a Response
export const createResponse = createAsyncThunk(
    'complaint/create/response',
    async ({id, resValue}, thunkAPI) => {
        try {
            const response = await complaintService.postAResponse(id, resValue);
            if(response.status !== 201){
                throw Error("Response is not added");
            }
            return await complaintService.fetchComplaint(id);
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj)
        }
    }
)

const initialState = {
    complaints: [],
    complaint: {},
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}
export const complaintReducer = createSlice({
    name: 'complaint',
    initialState,
    reducers: {
        ResetComplaintError(state){
            state.error = null;
            state.isError = false
            state.isSuccess = false
        },
        ResetComplaint(state){
            state.complaint = {}
        }
    },
    extraReducers: (builder) => {
        builder
        .addCase(getAllComplaints.pending, (state) => {
            state.isLoading = true
        })
        .addCase(getAllComplaints.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.complaints = action.payload
        })
        .addCase(getAllComplaints.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
        .addCase(getComplaintDetail.pending, (state) => {
            state.isDetailLoading = true
        })
        .addCase(getComplaintDetail.fulfilled, (state, action) => {
            state.isDetailLoading = false
            state.isSuccess = true
            state.complaint = action.payload
        })
        .addCase(getComplaintDetail.rejected, (state, action) => {
            state.isDetailLoading = false
            state.isError = true
            state.message = action.payload
        })
        .addCase(createComplaint.fulfilled, (state, action) => {
            toast.success('Complaint added');
        })
        .addCase(createComplaint.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
        .addCase(createResponse.pending, (state) => {
            state.isCreateResponseLoading = true
        })
        .addCase(createResponse.fulfilled, (state, action) => {
            state.isCreateResponseLoading = false
            state.isSuccess = true
            state.complaint = action.payload
            toast.success('Response added');
        })
        .addCase(createResponse.rejected, (state, action) => {
            state.isCreateResponseLoading = false
            state.isError = true
            state.error = action.payload
        })
    },
})

export const { ResetComplaintError, ResetComplaint } = complaintReducer.actions;
export default complaintReducer.reducer;