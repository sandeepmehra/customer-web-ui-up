import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { toast } from 'react-toastify';
import cartService from '../Services/cartService';

// Get all cart Item
export const getAllCartItem = createAsyncThunk(
    'cart/all',
    async (_, thunkAPI) => {
        try {
            return await cartService.allCartItem();
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }
    }
)

// post a cart
export const createCart = createAsyncThunk(
    'cart/create',
    async ({productVariantId, quantity}, thunkAPI) => {
      try {
        const response = await cartService.postCartItem(productVariantId, quantity); 
        if(response.status === 200){
            toast.success('Product already in the cart.');
            return await cartService.allCartItem();
        }
        toast.success('Product added');
        return await cartService.allCartItem();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Increase cart-item by one
export const increaseCartItem = createAsyncThunk(
    'cart/increase-item',
    async (productVariantId, thunkAPI) => {
      try {
        const response = await cartService.editAddItem(productVariantId);
        if(response.status !== 204){
            throw Error("Cart-item is not increased");
        }
        return await cartService.allCartItem();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// decrease cart-item by one
export const decreaseCartItem = createAsyncThunk(
    'cart/decrease-item',
    async (productVariantId, thunkAPI) => {
      try {
        const response = await cartService.editSubtractItem(productVariantId);
        if(response.status !== 204){
            throw Error("Cart-item is not decreased");
        }
        return await cartService.allCartItem();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Delete cart-item by one
export const deleteACartItem = createAsyncThunk(
    'cart/delete-item',
    async (productVariantId, thunkAPI) => {
      try {
        const response = await cartService.deleteCartItem(productVariantId);
        if(response.status !== 200){
            throw Error("Cart-item is not removed");
        }
        return await cartService.allCartItem();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

const initialState = {
    cartData: [],
    selectedAddress: {},
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
    totalAmount: 0,
    cartTotalQuntity: 0,
    buyNow:{}
}
export const cartReducer = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        ResetCartError(state){
            state.error = null;
            state.isError = false;
            state.isSuccess = false;
        },
        SetSelectedAddress(state, action){
            state.selectedAddress = action.payload;
        },
        SetBuyNow(state, action){
            state.buyNow = action.payload;
        },
        ResetBuyNowOrAddress(state, action){
            state.selectedAddress = {};
            state.buyNow = {};
        },
        calculateTotals:(state)=>{
            let total = 0;
            let quantityTotal = 0;
            let discountTotal = 0;
            state.cartData.forEach((item) => {
                total += parseFloat(item.total_price)
                quantityTotal += parseInt(item.quantity)
                discountTotal += parseFloat(item.discounted_price) * parseInt(item.quantity)
            });
            state.totalAmount = parseFloat(total.toFixed(2));
            state.cartTotalQuntity = quantityTotal;
            state.cartDiscountTotal = discountTotal;
        },
    },
    extraReducers: (builder) => {
        builder
        .addCase(getAllCartItem.pending, (state) => {
            state.isLoading = true
        })
        .addCase(getAllCartItem.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.cartData = action.payload.cart
        })
        .addCase(getAllCartItem.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
        .addCase(createCart.fulfilled, (state, action) => {
            state.cartData = action.payload.cart
        })
        .addCase(createCart.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
        .addCase(deleteACartItem.fulfilled, (state, action) => {
            toast.success('Cart-item removed');
            state.cartData = action.payload.cart
        })
        .addCase(deleteACartItem.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
        .addCase(increaseCartItem.fulfilled, (state, action) => {
            toast.success('Cart-item Quantity Increased');
            state.cartData = action.payload.cart
        })
        .addCase(increaseCartItem.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
        .addCase(decreaseCartItem.fulfilled, (state, action) => {
            toast.success('Cart-item Quantity Decreased');
            state.cartData = action.payload.cart
        })
        .addCase(decreaseCartItem.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
    },
})

export const { ResetCartError, SetSelectedAddress, calculateTotals, SetBuyNow, ResetBuyNowOrAddress } = cartReducer.actions;
export default cartReducer.reducer;