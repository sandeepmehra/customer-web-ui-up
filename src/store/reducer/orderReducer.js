import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import orderService from '../Services/orderService';

// Get all orders list
export const getAllOrders = createAsyncThunk(
    'orders/all',
    async (_, thunkAPI) => {
      try {
        return await orderService.allOrders();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
       
        return thunkAPI.rejectWithValue(errorObj);
      }
    }
)

// Get a order-detail
export const getOrderDetail = createAsyncThunk(
    'orders/one',
    async (order_item, thunkAPI) => {
      try {
        return await orderService.oneOrderDetail(order_item);
      } catch (error) {
        const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

const initialState = {
  orders: [],
  orderDetail:{},
  isError: false,
  isSuccess: false,
  isLoading: true,
  error: null,
}
export const orderReducer = createSlice({
  name: 'order',
  initialState,
  reducers: {
      ResetOrderError(state){
        state.error = null;
        state.isError = false;
        state.isSuccess = false;
      },
      UpdateOrderDetail(state, action){
        state.orderDetail = action.payload
      }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllOrders.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllOrders.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.orders = action.payload
      })
      .addCase(getAllOrders.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
      .addCase(getOrderDetail.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getOrderDetail.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.orderDetail = action.payload
      })
      .addCase(getOrderDetail.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
  },
})

export const { ResetOrderError, UpdateOrderDetail } = orderReducer.actions;
export default orderReducer.reducer