import { createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import Cookies from "js-cookie";
import jwt_decode from 'jwt-decode';
import { isjwt_valid } from '../helper/validation';
import authService from "../Services/authService";
import { toast } from 'react-toastify';


const initState = {
    name: '',
    isAuthenticated: false,
    isError: false,
    isSuccess: false,
    isUpdateSuccess: false,
    isLoading: true,
    error: null,
};
const access_token = Cookies.get('at');
const refresh_token = Cookies.get('rt');
if(access_token && refresh_token){
    isLoggedIn();
}
function isLoggedIn () {
    const decodedAt = jwt_decode(access_token);
    const decodedRt = jwt_decode(refresh_token);
    const rtNotExpired = decodedRt.exp * 1000 > Date.now();
    if((decodedAt !== undefined && decodedRt !== undefined) && isjwt_valid(access_token) && isjwt_valid(refresh_token) &&  rtNotExpired){
        const {sub, name} = decodedAt;
        if (name !== undefined || sub !== undefined) {
            initState.name = name;
            initState.isAuthenticated = true;
        } 
    }
}

// Login with Email and password
export const userLoginByEmail = createAsyncThunk(
    'login', 
    async (formValue, thunkAPI) => {
    try{
        const {data: {refresh_token}, headers:{authorization} } = await authService.userLogin(formValue);
        Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
        Cookies.set('rt', refresh_token, { sameSite: 'strict', secure: true });
        return { at: authorization.split(' ')[1] }; 
    }catch(error){
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj);
    }
});

// SignUp with Email and password
export const userSignUpByEmail = createAsyncThunk(
    '/signup/email', 
    async (formValue, thunkAPI) => {
    try{
        const { email, password } = formValue;
        const data = await authService.userSignUp(formValue);
        if(data.status === 201){
            const {data: {refresh_token}, headers:{authorization} } = await authService.userLogin({email, password});
            Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
            Cookies.set('rt', refresh_token, { sameSite: 'strict', secure: true });
            return { at: authorization.split(' ')[1] }; 
        }
    }catch(error){
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj);
    }
});

// Forget password with Email 
export const forgetPasswordEmail = createAsyncThunk(
    'forget-password', 
    async(email, thunkAPI) => {
    try{
        const response = await authService.forgetPassword(email);
        return response;
    }catch(error){
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj);
    }
})

// 
export const emailLoginVerifyAction = createAsyncThunk( 
    'verify-email', 
    async(formValue, thunkAPI) => {
        try{
            const data = await authService.verifyEmail(formValue);
            return data;
        }catch(error){
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }  
})

// Login with Mobile Number
export const loginMobileNumber = createAsyncThunk(
    'login/mobile-number',
    async (value, thunkAPI) => {
      try {
        const response = await authService.loginWithMobile(value);
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Mobile-number otp-verify 
export const mobileNumberOTPVerify = createAsyncThunk(
    'login/mobile-number-otp-verify',
    async (value, thunkAPI) => {
      try {
        const {data: {refresh_token}, headers:{authorization} } = await authService.mobileOTPVerify(value);
        Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
        Cookies.set('rt', refresh_token, { sameSite: 'strict', secure: true });
        return { at: authorization.split(' ')[1] }; 
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
const authReducer = createSlice({
    name: 'auth',
    initialState: initState,
    reducers: {
        ResetError(state){
            state.error = null;
            state.isError = false;
            state.isSuccess = false;
        },
        ResetAuth(state){
            state.name = '';
            state.isAuthenticated = false;
        },
        SetAuth(state, action){
            const decoded = jwt_decode(action.payload.at);
            const { name } = decoded;
            state.name = name;
            state.isLoading = false;
            state.isSuccess = true;
            state.isAuthenticated=true;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(userLoginByEmail.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(userLoginByEmail.fulfilled, (state, action) => {
                const decoded = jwt_decode(action.payload.at);
                const { name } = decoded;
                state.name = name;
                state.isLoading = false;
                state.isSuccess = true;
                state.isAuthenticated=true;
                toast.success('You are loggedIn.');
            }) 
            .addCase(userLoginByEmail.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
                toast.error(action.payload.message);
            })
            .addCase(userSignUpByEmail.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(userSignUpByEmail.fulfilled, (state, action) => {
                const decoded = jwt_decode(action.payload.at);
                const { name } = decoded;
                state.name = name;
                state.isLoading = false;
                state.isSuccess = true;
                state.isAuthenticated=true;
                toast.success('Your Account Created 👍');
            })
            .addCase(userSignUpByEmail.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
            })
            .addCase(forgetPasswordEmail.rejected, (state, action)  =>{
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
            })
            .addCase(loginMobileNumber.fulfilled, (state, action) => {
                state.isLoading = false;
                state.isSuccess = true;
                toast.success('Mobile OTP sent.');
            })
            .addCase(loginMobileNumber.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
            })
            .addCase(emailLoginVerifyAction.fulfilled, (state, action)=>{
                state.isUpdatePasswordSuccess = true;
                toast.success('Password Updated');
            })
            .addCase(emailLoginVerifyAction.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
            })
    },
});


export const { ResetError, ResetAuth, SetAuth } = authReducer.actions;
export default authReducer.reducer;