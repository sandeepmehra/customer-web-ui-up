import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import orderService from '../Services/orderService';
import ratingService from '../Services/ratingService';
import { UpdateOrderDetail } from './orderReducer';


// post a rating
export const createRating = createAsyncThunk(
    'rating/create',
    async ({orderItemId, review, rating}, thunkAPI) => {
      try {
        const response = await ratingService.postARating(orderItemId, review, rating);
        thunkAPI.dispatch(UpdateOrderDetail(await orderService.oneOrderDetail(orderItemId)));
        return response
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// post Rating Images
export const createImages = createAsyncThunk(
    'images/create',
    async ({orderItemId, formData}, thunkAPI) => {
      try {
        const response = await ratingService.postAImage(orderItemId, formData);
        thunkAPI.dispatch(UpdateOrderDetail(await orderService.oneOrderDetail(orderItemId)));
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// post Rating Images
export const createRatingOrImages = createAsyncThunk(
    'rating-images/create',
    async ({orderItemId, formData}, thunkAPI) => {
      try {
        const response = await ratingService.postRatingOrImage(orderItemId, formData);
        thunkAPI.dispatch(UpdateOrderDetail(await orderService.oneOrderDetail(orderItemId)));
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)


const initialState = {
    isError: false,
    isSuccess: false,
    isLoading: false,
    error: null,
}
export const ratingReducer = createSlice({
    name: 'review',
    initialState,
    reducers: {
      ResetReviewError(state){
        state.error = null;
        state.isError = false
        state.isSuccess = false
      }
    },
    extraReducers: (builder) => {
        builder
        .addCase(createRating.pending, (state, action) => {
          state.isLoading = true
        })
        .addCase(createRating.fulfilled, (state, action) => {
            state.isSuccess = true
            state.isLoading = false
            toast.success('Rating added');
        })
        .addCase(createRating.rejected, (state, action) => {
            state.isError = true
            state.isLoading = false
            state.error = action.payload
        })
        .addCase(createImages.pending, (state, action) => {
          state.isLoading = true
        })
        .addCase(createImages.fulfilled, (state, action) => {
            state.isSuccess = true
            state.isLoading = false
            toast.success('Images added');
        })
        .addCase(createImages.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
        .addCase(createRatingOrImages.pending, (state, action) => {
          state.isLoading = true
        })
        .addCase(createRatingOrImages.fulfilled, (state, action) => {
            state.isSuccess = true
            state.isLoading = false
            toast.success('Review added');
        })
        .addCase(createRatingOrImages.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
    },
})

export const { ResetReviewError } = ratingReducer.actions;
export default ratingReducer.reducer;