import { createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import Cookies from "js-cookie";
import { toast } from "react-toastify";
import categoryService from "../Services/categoryService";
import userService from "../Services/userService";
import { SetAuth } from "./authReducer";
import { SetCategoryProduct } from "./categoryReducer";

// Get a Account-detail
export const getAccoutDetailInfo = createAsyncThunk(
    'account',
    async (_, thunkAPI) => {
        try {
            return await userService.getAccoutDetail();
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }
    }
)

// Change Account Detail
export const changeAccountDetailInfo = createAsyncThunk(
    'account/change-detail',
    async (value, thunkAPI) => {
      try {
        const {data: {refresh_token}, headers:{authorization}} = await userService.changeAccountDetail(value);
        Cookies.remove('at');
        Cookies.remove('rt');
        Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
        Cookies.set('rt', refresh_token, { sameSite: 'strict', secure: true });
        thunkAPI.dispatch(SetAuth({ at: authorization.split(' ')[1] }));
        return ;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Change Account password
export const changeAccountPassword = createAsyncThunk(
    'account/change-password',
    async (value, thunkAPI) => {
      try {
        const response = await userService.changePassword(value);
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

//Get a Wishlist Lists
export const getWishlistLists = createAsyncThunk(
    'wishlist',
    async (_, thunkAPI) => {
        try {
            return await userService.getWishlist();
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }
    }
)

//Create a Wishlist Item
export const addToWishlistItem = createAsyncThunk(
    'wishlist/create',
    async ({id, category, isProductDetail}, thunkAPI) => { 
      try {
        const response = await userService.addToWishlist(id);
        if(response.status !== 204){
            throw Error("Item not added");
        } 
        if(isProductDetail){
            thunkAPI.dispatch(SetCategoryProduct(await categoryService.fetchProductsByCategoryWithoutVariant(category)));
        }else{
            const data = await categoryService.fetchProductsByCategory(category, thunkAPI.getState().auth.isAuthenticated);
            thunkAPI.dispatch(SetCategoryProduct(data));
        }   
        return await userService.getWishlist();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

//Remove a Wishlist Item
export const removeWishlistItem = createAsyncThunk(
    'wishlist/remove',
    async ({id, category, isProductDetail}, thunkAPI) => {
        console.log("thunkAPI: ", category, id, isProductDetail, category !== undefined && !(isProductDetail), category !== undefined && isProductDetail);
      try {
        const response = await userService.removeToWishlist(id);
        if(response.status !== 204){
            throw Error("Item not removed")
        }
        if(category !== undefined && !(isProductDetail)){
            console.log("fisrt if called: ", category, !(isProductDetail));
            const data = await categoryService.fetchProductsByCategory(category, thunkAPI.getState().auth.isAuthenticated);
            thunkAPI.dispatch(SetCategoryProduct(data));
        }
        if(category !== undefined && isProductDetail){
            console.log("second if called: ", category, isProductDetail);
            const data = await categoryService.fetchProductsByCategoryWithoutVariant(category);
            thunkAPI.dispatch(SetCategoryProduct(data)); 
        }
        return await userService.getWishlist();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

const initState = {
    userInfo: {},
    addressInfo: [],
    wishlist: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
};
    
const userReducer = createSlice({
    name: 'userReducer',
    initialState: initState,
    reducers: {
        ResetUserError(state, action){
            state.error = null;
            state.isError = false;
            state.isSuccess = false;
        },
        SetUserData(state, action){
            state.name = action.payload;
            state.isLoading = false;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getAccoutDetailInfo.pending, (state, action) => {
                state.isLoading = true
            })
            .addCase(getAccoutDetailInfo.fulfilled, (state, action) => {
                state.userInfo = action.payload;
                state.isLoading = false
                state.isSuccess = true
            }) 
            .addCase(getAccoutDetailInfo.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.error = action.payload
            })
            .addCase(changeAccountDetailInfo.pending, (state, action)=>{
                state.isUpdateLoading = true
            })
            .addCase(changeAccountDetailInfo.fulfilled, (state, action)=>{
                state.isUpdateLoading = false
                toast.success('User Detail Updated');
            })
            .addCase(changeAccountDetailInfo.rejected, (state, action) => {
                state.isUpdateLoading = false
                state.isError = true
                state.error = action.payload
            })
            .addCase(changeAccountPassword.pending, (state)=>{
                state.isUpdateLoading = true
            })
            .addCase(changeAccountPassword.fulfilled, (state, action)=>{
                state.isUpdateLoading = false
                toast.success('Password Updated');
            })
            .addCase(changeAccountPassword.rejected, (state, action) => {
                state.isUpdateLoading = false
                state.isError = true
                state.error = action.payload
            })
            .addCase(getWishlistLists.pending, (state, action)=>{
                state.isLoading = true
            })
            .addCase(getWishlistLists.fulfilled, (state, action)=>{
                state.isLoading = false
                state.wishlist = action.payload.data.wishlist
            })
            .addCase(getWishlistLists.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.error = action.payload
            })
            .addCase(addToWishlistItem.fulfilled, (state, action)=>{
                toast.success('Wistlist item added');
            })
            .addCase(addToWishlistItem.rejected, (state, action) => {
                state.isError = true
                state.error = action.payload
            })
            .addCase(removeWishlistItem.fulfilled, (state, action)=>{
                state.wishlist = action.payload.data.wishlist;
                toast.success('Wistlist item removed');
            })
            .addCase(removeWishlistItem.rejected, (state, action) => {
                state.isError = true
                state.error = action.payload
            })
    },
});


export const { ResetUserError, SetUserData } = userReducer.actions;
export default userReducer.reducer;