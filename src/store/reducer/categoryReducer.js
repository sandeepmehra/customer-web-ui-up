import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import categoryService from '../Services/categoryService'

// Get all Categories
export const getAllCategories = createAsyncThunk(
    'categories/all',
    async (_, thunkAPI) => {
      try {
        return await categoryService.allCategories()
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Get all product-variants by its category Name
export const getProductsByCategory = createAsyncThunk(
    'product-categories',
    async (category, thunkAPI) => {
      try {
        return await categoryService.fetchProductsByCategory(category, thunkAPI.getState().auth.isAuthenticated);
      } catch (error) {
        const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
export const getAlsoLikeCartProducts = createAsyncThunk(
  'categories/alsoLikeCartProducts',
  async (_, thunkAPI) => {
    try {
      return await categoryService.fetchProductsByCategoryWithoutVariant('Plants');
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

// Get Plants, Pots, Tools
export const getPlantsPotsToolsData = createAsyncThunk(
  'categories/Platns-Pots-Tools',
  async (_, thunkAPI) => {
    try {
      const data = await categoryService.allCategories();
      thunkAPI.dispatch(SetCategories(data));
      return await categoryService.plantsPotsToolsCategories();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

const initialState = {
  categories: [],
  categoryProduct:{},
  plantsData:[],
  potsData:[],
  toolsData: [],
  alsoLikeInCartProduct: [],
  isError: false,
  isSuccess: false,
  isLoading: true,
  isHomeLoading: true,
  error: null,
}

export const categoryReducer = createSlice({
  name: 'category',
  initialState,
  reducers: {
    SetCategoryProduct(state, action){
      state.categoryProduct = action.payload
    },
    SetCategories(state, action){
      state.categories = action.payload
    },
    ResetCategoryError(state){
      state.error = null;
      state.isError = false;
      state.isSuccess = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllCategories.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllCategories.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.categories = action.payload
      })
      .addCase(getAllCategories.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
      .addCase(getProductsByCategory.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getProductsByCategory.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        console.log("getProductsByCategory action.payload:", action.payload);
        state.categoryProduct = action.payload
      })
      .addCase(getProductsByCategory.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
      .addCase(getPlantsPotsToolsData.pending, (state) => {
        state.isHomeLoading = true
      })
      .addCase(getPlantsPotsToolsData.fulfilled, (state, action) => {
        state.isHomeLoading = false
        state.isSuccess = true;
        action.payload.forEach((item, i)=>{
          const category = {
            0: 'plantsData',
            1: 'potsData',
            2: 'toolsData'
          }
          if(item.status === 'fulfilled'){
            state[category[i]] = item.value.data
          }else{
            state[category[i]] = [];
          }
        })
      })
      .addCase(getPlantsPotsToolsData.rejected, (state, action) => {
        state.isHomeLoading = false
        state.isError = true
        state.error = action.payload
      })
      .addCase(getAlsoLikeCartProducts.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAlsoLikeCartProducts.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        console.log("getAlsoLike prdocuts action.payload:", action.payload);
        state.alsoLikeInCartProduct = action.payload
      })
      .addCase(getAlsoLikeCartProducts.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
  },
})

export const { SetCategoryProduct, SetCategories, ResetCategoryError } = categoryReducer.actions;
export default categoryReducer.reducer;