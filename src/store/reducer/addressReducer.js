import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { toast } from 'react-toastify';
import addressService from '../Services/addressService';
import { SetSelectedAddress } from './cartReducer';

// Get all address list
export const getAllAddressList = createAsyncThunk(
    'address/all',
    async (_, thunkAPI) => {
        try {
            return await addressService.allAddress();
        } catch (error) {
            const errorObj = {
                message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
                statusCode:(error.response && error.response.status ) || 0
            }
            return thunkAPI.rejectWithValue(errorObj);
        }
    }
)

// create a address
export const createAddress = createAsyncThunk(
    'address/create',
    async (value, thunkAPI) => {
      try {
        const response = await addressService.postAddress(value);
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

//
export const updateAddress = createAsyncThunk(
    'address/edit-item',
    async ({value, id, addressData}, thunkAPI) => {
      try {
        const response = await addressService.changeAddress(value, id);
        if(response.status !== 204){
            throw Error("Address not updated");
        }
        await thunkAPI.dispatch(SetAddressInfo(await addressService.allAddress()));
        await thunkAPI.dispatch(SetSelectedAddress(addressData));
        return response;
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

const initialState = {
    addressInfo:[],
    isError: false,
    isSuccess: false,
    isUpdateSuccess: false,
    isLoading: true,
    error: null,
}
export const addressReducer = createSlice({
    name: 'address',
    initialState,
    reducers: {
        ResetAddressError(state){
            state.error = null;
            state.isError = false;
            state.isSuccess = false;
        },
        SetAddressInfo(state, action){
            state.addressInfo = action.payload;
        },
        ResetAddress(state){
            state.addressInfo = []
            state.isError= false
            state.isSuccess= false
            state.isUpdateSuccess= false
            state.isLoading= true
            state.error= null
        }
    },
    extraReducers: (builder) => {
        builder
        .addCase(getAllAddressList.pending, (state) => {
            state.isLoading = true
        })
        .addCase(getAllAddressList.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.addressInfo = action.payload
        })
        .addCase(getAllAddressList.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
        .addCase(createAddress.fulfilled, (state, action) => {
            toast.success('Address added');
        })
        .addCase(createAddress.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
        .addCase(updateAddress.fulfilled, (state, action) => {
            toast.success('Address updated');
            state.isUpdateSuccess = true;
        })
        .addCase(updateAddress.rejected, (state, action) => {
            state.isError = true
            state.error = action.payload
        })
    },
})

export const { ResetAddressError, SetAddressInfo, ResetAddress } = addressReducer.actions;
export default addressReducer.reducer;