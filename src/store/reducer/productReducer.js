import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import categoryService from '../Services/categoryService';
import productService from '../Services/productService';
import { SetCategoryProduct } from './categoryReducer';

// Get all products by its Id
export const getProductsByIdDetail = createAsyncThunk(
    'product-detail',
    async (productId, thunkAPI) => {
        try {
            const data = await productService.fetchProductsById(productId);
            thunkAPI.dispatch(SetCategoryProduct(await categoryService.fetchProductsByCategoryWithoutVariant(data.product.category)));
            return data
        } catch (error) {
            const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
            return thunkAPI.rejectWithValue(errorObj)
        }
    }
)

const initialState = {
    product:{},
    productVariants: {},
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const productReducer = createSlice({
    name: 'product',
    initialState,
    reducers: {
        ResetProductError(state){
            state.error = null;
            state.isError = false;
            state.isSuccess = false;
        },
        SetProductDetail(state, action) {
            state.product = action.payload.product
            state.productVariants = action.payload.product_variants
            state.isLoading = false
        },
        ResetProductDetail(state){
            state.product = {}
            state.productVariants = {}
            state.isError = false
            state.isSuccess = false
            state.isLoading = true
            state.error = null
        },
    },
    extraReducers: (builder) => {
        builder
        .addCase(getProductsByIdDetail.pending, (state) => {
            state.isLoading = true
        })
        .addCase(getProductsByIdDetail.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.product = action.payload.product
            state.productVariants = action.payload.product_variants
        })
        .addCase(getProductsByIdDetail.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.error = action.payload
        })
    },
})

export const { ResetProductError, ResetProductDetail, SetProductDetail } = productReducer.actions;
export default productReducer.reducer