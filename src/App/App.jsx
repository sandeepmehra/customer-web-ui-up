import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignIn from "../Screens/User/Auth/SignIn";
import ForgetPassword from "../Screens/User/Auth/ForgetPassword";
import SignUp from "../Screens/User/Auth/SignUp";
import { RedirectAuth } from "../Screens/User/Auth/Redirect";
import VerifyNumber from "../Screens/User/Auth/VerifyNumber";
import Navbar from "../Components/Navbar/Navbar";
import Footer from "../Components/Footer/Footer";
import { ProtectedRoute } from "../Components/Routes/ProtectedRoute";
import AccountPage from "../Screens/User/Account/Account";
import AccountDetail from "../Screens/User/Account/BasicAccountDetail";
import ChangePassword from "../Screens/User/Account/ChangePassword";
import CategoryHome from "../Screens/Category/CategoryHome";
import Category from "../Screens/Category/Category";
import Cart from "../Screens/Cart/Cart";
import HomePage from "../Screens/Home/Home";
import Product from "../Screens/Product/Product";
import HelpCenterPage from "../Screens/HelpCenter/HelpCenter";
import ComplaintPage from "../Screens/HelpCenter/Complaint";
import { ComplaintDetail } from "../Screens/HelpCenter/ComplaintDetail";
import OrdersPage from "../Screens/Orders/Orders";
import OrderDetailsPage from "../Screens/Orders/OrderDetail";
import ReviewPage from "../Screens/Review/Review";
import CheckoutPage from "../Screens/Checkout/Checkout";
import ShippingPage from "../Screens/Checkout/Shipping";
import PaymentsPage from "../Screens/Checkout/Payment";
import OrderSuccess from "../Screens/Checkout/OrderSuccess";
import WishList from "../Screens/User/Account/WishList";
import { NotFound } from "../Screens/Error/NotFound";
import ErrorBoundary from "../Screens/Error/ErrorBoundary";
import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import AddressPage from "../Screens/Checkout/Address/AddAddress";
import UpdateAddressPage from "../Screens/Checkout/Address/UpdateAddress";
import GetAllAddress from "../Screens/Checkout/Address/AddressList";
import ChangeAddress from "../Screens/User/Account/ChangeAddress";
import ListAllAddress from "../Screens/User/Account/AddressList";
import AddNewAddress from "../Screens/User/Account/AddNewAddress";
import { WentWrong } from "../Screens/Error/WentWrong";
import OrderFailure from "../Screens/Checkout/OrderFailure";
import ScrollToTop from "../Components/ScrollToTop/ScrollToTop";
import { PrivatePolicy } from "../Screens/Policy/PrivacyPolicy";
import { RefuncPolicy } from "../Screens/Policy/RefuncPolicy";
import { ShippingPolicy } from "../Screens/Policy/ShippingPolicy";
import { TermsOfServices } from "../Screens/Policy/TermsOfServices";
import { Suspended } from "../Screens/Error/Suspended";

const App =()=>{
    return( 
        <BrowserRouter>
        <ScrollToTop>
            <ToastContainer
                className={'toastBody'}
                draggable={false}
                autoClose={1000}
            />
            <Navbar />
            <main style={{"minHeight":"70vh"}}>
                <ErrorBoundary>
                    <Routes>
                        <Route path="/login" element={<SignIn />} />    
                        <Route path="/login/reset-password" element={<ForgetPassword />} />
                        <Route path="/signup" exact element={<SignUp />} />
                        <Route path="/auth/google/success" element={<RedirectAuth />} />
                        <Route path="/verify/mobile" element={<VerifyNumber />} />

                        <Route path="/categories" exact element={<CategoryHome />} />
                        <Route path="/category/:categoryName" exact element={<Category />} />
                        <Route path="/product/:productId" exact element={<Product />} />
                        
                        <Route path="/cart" element={<ProtectedRoute><Cart /></ProtectedRoute>} />
                        <Route path="/checkout" element={<ProtectedRoute><CheckoutPage /></ProtectedRoute>} />
                        <Route path="/address" element={<ProtectedRoute><AddressPage /></ProtectedRoute>} />
                        <Route path="/address/update/:id" element={<ProtectedRoute><UpdateAddressPage /></ProtectedRoute>} />
                        <Route path="/address/change/add" element={<ProtectedRoute><GetAllAddress /></ProtectedRoute>} />
                        <Route path="/shipping" element={<ProtectedRoute><ShippingPage /></ProtectedRoute>} />
                        <Route path="/payment" element={<ProtectedRoute><PaymentsPage /></ProtectedRoute>} />
                        <Route path="/order-success" element={<OrderSuccess />} />
                        <Route path="/order-failure" element={<OrderFailure />} />
                                
                        <Route path="/orders" exact element={<ProtectedRoute><OrdersPage showDetails={true}/></ProtectedRoute>} />
                        <Route path="/order/:orderId" exact element={<ProtectedRoute><OrderDetailsPage /></ProtectedRoute>} />
                        <Route path="/review/:orderItemId" exact element={<ProtectedRoute><ReviewPage /></ProtectedRoute>} /> 
                        <Route path="/help-center" exact element={<ProtectedRoute><HelpCenterPage /></ProtectedRoute>} >
                            <Route path=":id" element={<ProtectedRoute><ComplaintDetail /></ProtectedRoute>} />
                        </Route>
                        <Route path="/complaint" exact element={<ProtectedRoute><ComplaintPage /></ProtectedRoute>} />
                        <Route path="/account" element={<ProtectedRoute><AccountPage /></ProtectedRoute>} >
                            <Route index element={<AccountDetail />} />
                            <Route path="wishlist" element={<WishList />} />
                            <Route path="change/password" element={<ChangePassword />} />
                            <Route path="address" >
                                <Route path="all" element={<ListAllAddress />} />
                                <Route path="add" element={<AddNewAddress />} />
                            </Route>
                            <Route path="change/address/:id" element={<ChangeAddress />} />
                            
                        </Route>
                        <Route path="/something-went-wrong" exact element={<WentWrong />} />
                        <Route path="/privacy-policy" exact element={<PrivatePolicy />} />
                        <Route path="/refund-policy" exact element={<RefuncPolicy />} />
                        <Route path="/shipping-policy" exact element={<ShippingPolicy />} />
                        <Route path="/terms-of-services" exact element={<TermsOfServices />} />
                        <Route path="/suspended-account" exact element={<Suspended />} />
                        <Route path="/" exact element={<HomePage />} />
                        <Route path="*" element={<NotFound />} />
                    </Routes>  
                </ErrorBoundary>
            </main>
            <Footer />  
            </ScrollToTop>      
        </BrowserRouter> 
    )
}

export default App;