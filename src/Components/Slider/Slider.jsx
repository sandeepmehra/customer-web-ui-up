import React, {useState} from 'react';
import style from "./Slider.module.css";

const Slider = ({images, isHome}) => {
    const [hover, setHover] = useState(0);
    const [imageIndex, setImageIndex] = useState(0);
    const [touchPosition, setTouchPosition] = useState(null);

    const next = () => {
        setImageIndex(prevState => prevState === images.length - 1 ? 0 : prevState + 1)
    }
    
    const prev = () => {
        setImageIndex(prevState => prevState === 0 ? images.length - 1 : prevState - 1)
    }
    const handleTouchStart = (e) => {
        const touchDown = e.touches[0].clientX
        setTouchPosition(touchDown)
    }
    const handleTouchMove = (e) => {
        const touchDown = touchPosition
        if(touchDown === null) {
            return
        }
        const currentTouch = e.touches[0].clientX
        const diff = touchDown - currentTouch
    
        if (diff > 5) {
            next()
        }
    
        if (diff < -5) {
            prev()
        }
        setTouchPosition(null)
    }
    return (
        <div className={style.wrapper}>
            <div className={style.slider_web_view}>
                <div className={isHome ? style.thumbnail_wrapper_home : style.thumbnail_wrapper}>
                    {images !== undefined && images.length !== 0 && images.map((url, i)=>(
                        <div 
                            className={i === (imageIndex || hover) ? `${isHome ?  `${style.thumbnail_home_image} ${style.active}`: `${style.thumbnail_image} ${style.active}`} `:  `${isHome ?  `${style.thumbnail_home_image}` : `${style.thumbnail_image}`}`} 
                            key={i}
                            onClick={() => setImageIndex(i)}
                            onMouseEnter={() => setHover(i)}
                        >
                            <img src={url} alt={url} />
                        </div>
                        ))
                    }
                </div>
                <div className={isHome ? style.preview_image_home : style.preview_image}>
                    <img src={images[imageIndex || hover]} alt={images[imageIndex || hover]} />
                </div>
            </div>
            <div className={style.slider_mobile_view}>
                <div className={style.slideshow}>
                    <div className={style.slideshowSlider} style={{transform: `translate3d(${-imageIndex * 100}%, 0, 0)` }}>
                    {images !== undefined && images.length !== 0 && images.map((url, i)=>(
                        <div className={style.slide} key={i} onTouchStart={handleTouchStart} onTouchMove={handleTouchMove}>
                            <img src={url} alt={url} />
                        </div>))}
                    </div>
                    <div className={style.slideshowDots}>
                    {images !== undefined && images.length !== 0 && images.map((url, i)=>(
                        <div 
                            className={i === imageIndex ? `${style.slideshowDot} ${style.active}`: `${style.slideshowDot}`} 
                            onClick={() => {setImageIndex(i)}}
                            key={i}
                        >
                        </div>))
                    }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Slider;