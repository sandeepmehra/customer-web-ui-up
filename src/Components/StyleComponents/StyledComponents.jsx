import styled from "styled-components";

export const Title = styled.div`
font-family:Roboto;
color:#1F553E;
font-weight: 400;
font-size: 1.375rem;
line-height: 1.625rem;
`
export const Header = styled.div`
color:#1F553E;
font-size:18px;
font-family:Roboto;
font-weight:400;
text-transform: none;
`
export const BoldTitle = styled.div`
color:#1F553E;
font-size:18px;
font-family:Roboto;
font-weight:500;
text-transform: capitalize;
font-style:Medium;
`

export const Label = styled.div`
color:#1F553E;
font-size:1.09rem;
font-family:Roboto;
font-weight:400;
line-height: 1.165rem;
text-transform: capitalize;
`

export const SmallLabel = styled.div`
color:#1F553E;
font-size:11px;
font-family:Roboto;
font-weight:400;
text-transform: none;
`

export const MediumLabel = styled.div`
color:#1F553E;
font-size:12px;
font-family:Roboto;
font-weight:400;
text-transform: none;
`
