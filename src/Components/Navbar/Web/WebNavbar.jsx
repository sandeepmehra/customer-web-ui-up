import React, { useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import style from "../Navbar.module.css";
import CartOrder from '../CartOrder';
import Dropdown from '../Dropdown/Dropdown';

const WebNavbar = ({iconStyles, isAuthenticated, cartData, cartTotalQuntity}) => {
    const navigate = useNavigate();
    const [isOpen, setIsOpen] = useState(true);
    const handleCart = (e) => {
        e.preventDefault();
        if(isAuthenticated) navigate('/cart', { replace: true })
        else navigate('/login')
    }
    const handleClick = () => {
        window.open("https://www.linkedin.com/company/urbanplants/");
    };
    return (
        <div className={style.menu_options}>
            <div className={style.menu_option}>
                <Link to="/">Home</Link>
            </div>
            <div className={style.menu_option}>
                <Link to="/account">Account</Link>
            </div>
            <div className={style.menu_option}>
                <Link to="/categories">All Categories</Link>
            </div>
            <div className={style.menu_option}>
                <Link to="/orders" >My Orders</Link>
            </div>
            <div className={style.menu_option_notification}>
                <div className={style.notification_text}><div>My Notifications</div>
                    {isAuthenticated ? <span className={style.icon_button_text_right}>4</span>:null}
                    {isAuthenticated && <div className={style.dropdown}><Dropdown isOpen={isOpen} setIsOpen={setIsOpen}/></div>}
                </div>
            </div>
            <div className={style.menu_option}>
                <Link to="/help-center">Help Center</Link>
            </div>
            <div className={style.menu_option}>
                <span onClick={handleClick}>About Us</span>
            </div>
            <div className={style.menu_option} onClick={handleCart} >
                <CartOrder 
                    iconStyles={iconStyles} 
                    cartData={cartData} 
                    isAuthenticated={isAuthenticated} 
                    cartTotalQuntity={cartTotalQuntity} 
                />
            </div>
        </div>
    )
}

export default WebNavbar;