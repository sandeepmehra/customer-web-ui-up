import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import style from "./Navbar.module.css";
import { ReactComponent as Logo } from '../../assets/images/urban_logo.svg';
import { GiHamburgerMenu } from "react-icons/gi";
import MobileNavbar from "./Mobile/MobileNavbar";
import { ImCancelCircle } from 'react-icons/im';
import WebNavbar from "./Web/WebNavbar";
import { useSelector, useDispatch } from "react-redux";
import CartOrder from "./CartOrder";
import { getAllCartItem } from "../../store/reducer/cartReducer";
import { MdNotifications } from "react-icons/md";
import Dropdown from "./Dropdown/Dropdown";

const Navbar = () => {
    const {name, isAuthenticated} = useSelector((state)=> state.auth);
    const {cartData} = useSelector(state => state.cartSlice);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenNotification, setIsOpenNotification] = useState(true);
    const navigate = useNavigate();
    const handleCart = (e) => {
        e.preventDefault();
        if(isAuthenticated) navigate('/cart', { replace: true })
        else navigate('/login')
    }
    let iconStyles = { color: "#1f553e", fontSize: "1.5em" };
    useEffect(()=>{
        if(isAuthenticated){
          dispatch(getAllCartItem());
        }
      },[dispatch, isAuthenticated]);
    return (
        <>
            <div className={style.navbar_container}>
                <div className={style.header}>
                    <div className={style.header_message}></div>
                    <div className={style.menu}>
                        <div className={style.logo} onClick={()=>navigate('/')}>
                            <Logo className={style.header_logo} fill='#f3f3f3' />
                        </div>
                        {/* Dsipaly Web Menu */}
                        <div className={style.web_menu}>
                            <WebNavbar 
                                iconStyles={iconStyles} 
                                cartData={cartData} 
                                isAuthenticated={isAuthenticated} 
                            />
                        </div>

                        {/* Display Mobile  */}
                        <div className={style.mobile_menu_icon}>
                            <div className={style.margin_right}>
                                Hi! {name !== undefined && name !== ''? name.split(" ")[0]:'There'}
                            </div>
                            <div className={style.margin_right} onClick={handleCart}>
                                <CartOrder 
                                    iconStyles={iconStyles} 
                                    cartData={cartData} 
                                    isAuthenticated={isAuthenticated} 
                                />
                            </div>
                            <div className={style.margin_right} onClick={()=>setIsOpenNotification(!isOpenNotification)}>
                                <div className={style.notification_container}>
                                    {isAuthenticated ? 
                                    <>
                                        <MdNotifications style={iconStyles} />
                                        <span className={style.icon_button_text_right}>4</span>
                                    </>
                                        :
                                        <MdNotifications style={iconStyles} />
                                    }
                                </div>
                            </div>
                            <div onClick={() => setIsOpen(!isOpen)}>
                                {!isOpen && <GiHamburgerMenu style={iconStyles} />}
                                {isOpen && <ImCancelCircle style={iconStyles} />}
                            </div>
                        </div>
                    </div>
                    {isOpen && <div className={style.mobile_menu}><MobileNavbar setIsOpen={setIsOpen}/></div>}
                    {!isOpenNotification && isAuthenticated && <div className={style.mobile_menu_dropdown}><Dropdown isOpen={isOpenNotification} setIsOpen={setIsOpenNotification}/></div>}
                </div>
            </div>
        </>
    )
}

export default Navbar;