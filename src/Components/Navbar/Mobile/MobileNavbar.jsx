import React from 'react';
import { Link } from "react-router-dom";
import style from "./MobileNavbar.module.css";
import { AiOutlineRight } from 'react-icons/ai';

const MobileNavbar = ({setIsOpen }) => {
    const handleClick = ()=>{
        setIsOpen(false);
    }
    const handleClickAbout = () => {
        window.open("https://www.linkedin.com/company/urbanplants/");
    };
    return (
        <div className={`${style.mobile_menu_options}`}>
            <Link to="/login" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}>Sign In / Sign Up</label>
                    <AiOutlineRight />
                </div>
            </Link>
            <Link to="/" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}> Home</label>
                    <AiOutlineRight />
                </div>
            </Link>
            <Link to="/categories" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}>All Categories</label>
                    <AiOutlineRight />
                </div>
            </Link>


            <Link to="/orders" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}>My Orders</label>
                    <AiOutlineRight />
                </div>
            </Link>

            <Link to="/account" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}>My account</label>
                    <AiOutlineRight />
                </div>
            </Link>
            <Link to="/help-center" onClick={handleClick}>
                <div className={`${style.mobile_option}`}>
                    <label className={style.width}>Help Center</label>
                    <AiOutlineRight />
                </div>
            </Link>
            <div className={`${style.mobile_option}`} onClick={handleClick}>
                <label className={style.width}  onClick={handleClickAbout}>About Us</label>
                <AiOutlineRight />
            </div>
        </div>
    )
}

export default MobileNavbar;