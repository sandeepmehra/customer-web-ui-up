import React from "react";
import style from "./Dropdown.module.css";
import { VscTriangleDown } from 'react-icons/vsc';

const Dropdown = ({isOpen, setIsOpen}) => {
    return (
        <div className={style.dropdown_container}>
            <div className={style.dropdown_card}>
                <div className={style.dropdown_item}>
                    <span className={style.dropdown_heading}>Orders:</span><span className={style.dropdown_text}>Your order #43395 has been shipped...</span>
                </div>
                <div className={style.dropdown_item}>
                    <span className={style.dropdown_heading}>Complaints:</span><span className={style.dropdown_text}>A new response on your compaint...</span>
                </div>
                <div className={style.dropdown_item}>
                    <span className={style.dropdown_heading}>Offers:</span><span className={style.dropdown_text}>Here is an exclussive deal on flower bulbs...</span>
                </div>
                <div className={style.dropdown_item}>
                    <span className={style.dropdown_heading}>Orders:</span><span className={style.dropdown_text}>Your order #43395 has been shipped...</span>
                </div>
                <div className={style.dropdown_icon}>
                    <VscTriangleDown color={"#1F553E"} onClick={()=>setIsOpen(!isOpen)}/>
                </div>
            </div>          
        </div>
    )
}

export default Dropdown;