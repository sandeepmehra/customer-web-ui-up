import React from 'react';
import style from './CartOrder.module.css';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';

const CartShow = ({iconStyles, isAuthenticated, cartData}) => {
    return (
      <div className={style.cart_container}>
          {isAuthenticated ? 
          <>
            <ShoppingCartOutlinedIcon style={iconStyles} />
            {
                cartData !== undefined  && cartData.length > 0  ?
                <span className={style.icon_button_text_right}>{cartData.length}</span>
              : null
            }
          </>
            :
            <ShoppingCartOutlinedIcon style={iconStyles} />
          }
      </div>
    )
}

export default CartShow;