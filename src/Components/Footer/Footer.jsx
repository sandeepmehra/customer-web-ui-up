import React from 'react';
import { Link } from 'react-router-dom';
import { SocialIcon } from 'react-social-icons';
import style from "./Footer.module.css";

const Footer = () => {
    const handleClickLinkedin = () => {
        window.open("https://www.linkedin.com/company/urbanplants/");
    };
    return (
        <div className={style.main_container}>
             <div className={style.footer_section}>
                <div className={style.footer_social_links}>
                    <div>
                        <SocialIcon network="facebook" bgColor="#ffff" fgColor="#1f553e" className={style.social_links} />
                        <SocialIcon network="youtube" bgColor="#ffff" fgColor="#1f553e" className={style.social_links} />
                        <SocialIcon network="pinterest" bgColor="#ffff" fgColor="#1f553e" className={style.social_links} />
                        <SocialIcon network="twitter" bgColor="#ffff" fgColor="#1f553e" className={style.social_links} />
                        <SocialIcon network="linkedin" bgColor="#ffff" fgColor="#1f553e" className={style.social_links} onClick={handleClickLinkedin}/>
                    </div>
                </div>
                <div className={style.touch_label}>
                    <b>Get in touch</b>
                </div>

                <div className={style.footer_links}>
                    <div className={style.footer_copy}> &copy; </div>
                    <div className={style.footer_links_card}>
                        <div>
                            2021 Urban Plants | Sadabahar Greens Private Limited
                        </div>
                        <div>
                            <Link to='/privacy-policy'>
                                <u>Privacy Policy</u>
                            </Link>
                        </div>
                        <div>
                            <Link to='/refund-policy'>
                                <u> Refund Policy
                                </u>
                            </Link>
                        </div>
                        <div>
                            <Link to='/shipping-policy'>
                                <u>Shipping Policy</u>
                            </Link>
                        </div>
                        <div>
                            <Link to='/terms-of-services'>
                                <u>Terms & Conditions</u>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;