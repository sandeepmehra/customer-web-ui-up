import React, { useEffect } from 'react'
import style from './AddressForm.module.css';

const AddressForm = ({addressData, inputHandler, handleClick, diff, handleBackbutton, addressInfo, id, setAddressData}) => {
    useEffect(()=>{
        if(addressInfo !== undefined && addressInfo.lenght !== 0){
            setAddressData(addressInfo.find(item => item.id === id));
        }
    }, [addressInfo, id, setAddressData]);
    console.log("addressInfo in addressform component: ", addressInfo, addressData);
    return (
        <div className={style.address_form}>
            <div className={style.address_form_inputs}>
                <input 
                    placeholder={'Name'}
                    type={"text"}
                    name={"name"}
                    defaultValue={addressData.name}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
                <input 
                    placeholder={'Phone Number'}
                    type={"text"}
                    name={"mobile_number"}
                    defaultValue={addressData.mobile_number}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
            </div>
            <div className={style.address_form_inputs}>
                <textarea 
                    rows="4"   
                    className='width-50' 
                    placeholder={'Address'}
                    type={"text"}
                    name={"street"}
                    defaultValue={addressData.street}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
                <div className={style.address_form_inputs_column}>
                    <input 
                        placeholder={'City/District/Town'}
                        type={"text"}
                        name={"city"}
                        defaultValue={addressData.city}
                        autoComplete="off"
                        onChange={inputHandler} 
                        required 
                        className={style.input_address} 
                    />
                    <input 
                        placeholder={'State'}
                        type={"text"}
                        name={"state"}
                        defaultValue={addressData.state}
                        autoComplete="off"
                        onChange={inputHandler} 
                        required 
                        className={style.input_address}
                    />
                </div>
            </div>
            <div className={style.address_form_inputs}>
                <input 
                    placeholder={'PIN Code'}
                    type={"text"}
                    name={"pincode"}
                    defaultValue={addressData.pincode !== '0' ? addressData.pincode : ''}
                    autoComplete="off"
                    onChange={inputHandler} 
                    required 
                />
            </div>
            <div className={style.btn_div}>
                <div className={style.update_btn}>
                    <button 
                        onClick={(e)=>handleClick(e, diff(addressInfo.find(item => item.id === id), addressData))}
                    >Save</button>
                </div>
                <div className={style.cancel_btn}>
                    <button onClick={handleBackbutton}>Cancel</button>
                </div>
                
            </div>
        </div>
    )
}

export default AddressForm;