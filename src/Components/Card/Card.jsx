import React from 'react';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { createCart, SetBuyNow } from '../../store/reducer/cartReducer';
import style from "./Card.module.css";

const Card = ({ product, index, handleWishlist, handleRemoveWishlist }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const {isAuthenticated} = useSelector((state)=> state.auth);
    const addToCart = async(e, productVariantId) => {
        e.preventDefault();
        e.stopPropagation();
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(createCart({productVariantId, quantity: 1}));  
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    };
    const handleBuyNow = async(e, productVariantId, price, discounted_price) => {
        e.preventDefault();
        e.stopPropagation();
        if(productVariantId !== null && productVariantId !== false && productVariantId !== undefined && isAuthenticated){
            await dispatch(SetBuyNow({productVariantId, quantity: 1, total_price:Number(price) * 1, discount_price:Number(discounted_price)}));
            navigate('/checkout');
        }else { 
            navigate('/login', {replace: true,  state:{ from: {pathname: location} }});
        }
    }
    // const clickHandle = (productId) => {
    //     navigate(`/product/${productId}`);
    // }
    return (
        <Link to={`/product/${product.id}`} className={style.container} key={index} >
            <div className={style.card_container}>
            <div className={style.product_image_container}>
                <img src={product.image} alt={"product"}/>
            </div>
            <div className={style.card_content}>
                <div className={style.card_price_container}>
                    <div className={style.card_price_card}>
                        <label className={style.price}>₹{product.price}</label>
                        <label className={style.product_old_price}> 
                            <strike>₹{Number(product.price) + Number(product.discounted_price)} </strike>
                        </label>
                    </div>
                   {isAuthenticated ? <div className={style.like_btn}>
                        {product.wishlist !== false && product.wishlist_id !== "undefined"?
                            <AiFillHeart color='red' size={24} onClick={(e)=>handleRemoveWishlist(e, product.wishlist_id, product.category)}/>:
                            <AiOutlineHeart size={24} onClick={(e)=>handleWishlist(e, product.product_variant_id, product.category)} />
                        }
                    </div>: null}
                </div>
                <label className={style.card_discount}>
                    {/*  Discount % = (Discount/List Price) × 100 */}
                    Save {(Number(product.discounted_price)/(Number(product.price) + Number(product.discounted_price)) * 100).toFixed(2)}%
                </label>
                <div className={style.product_name}>
                    <p>{product.product_name}</p>
                </div>
                <div className={style.product_shop_name}>
                    {product.shop_name}
                </div>
                <div className={style.card_btns}>
                    <button onClick={(e)=>addToCart(e, product.product_variant_id)}>
                        Add to Cart
                    </button>                    
                    <button onClick={(e)=>handleBuyNow(e, product.product_variant_id, product.price, product.discounted_price)}>
                        Buy now
                    </button>
                </div>
            </div>
            </div>
        </Link>
    )
}

export default Card;