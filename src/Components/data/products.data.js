import Image from '../../assets/images/image.jpg';
export const products_data = [
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        liked:"No"
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Urban Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    },
    {
        name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image
    }
]

export const products_data_home = [
    
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        liked:"No",
        shop_name: "Urban Plant"
    },
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Urban Plants",
        discount:"33%",
        image:Image,
        shop_name: "Urban Plant"
    },
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        shop_name: "Urban Plant"
    },
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        shop_name: "Urban Plant"
    },
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        shop_name: "Urban"
    },
    {
        product_name:"Parrot Plant",
        old_price:"899.00",
        price :"599.00",
        category:"Plants",
        discount:"33%",
        image:Image,
        shop_name: "Urban Plant"
    },
    
  
    
]

const DummyData = {
    "product":{
        "product_name": "Product plant",
        "description": "This is product dummy data",
        "category": "Plants",
        "shop_name": "Shop name",
        "colors_available": {
            "FFFF00": {
                "12 Ltr": "100001",
                "15 Ltr": "100005"
            },
            "FF0000": {
                "10 Ltr": "100003",
                "15 Ltr": "100004"
            },
            "0000FF": {
                "5 Ltr": "100002"
            }
        },
        "sizes_available": {
            "12 Ltr": {
                "FFFF00": "100001"
            },
            "5 Ltr": {
                "0000FF": "100002"
            },
            "10 Ltr": {
                "FF0000": "100003"
            },
            "15 Ltr": {
                "FF0000": "100004",
                "FFFF00": "100005"
            }
        }
    },
    "product_variants": {
      "100001": {
        "price": "12.3",
        "discounted_price": "1.5",
        "sku": "sku 1",
        "country_of_origin": "India",
        "color": "FFFF00",
        "measurements": "12 Ltr",
        "images": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/B3BsS4qtRO_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/BLWkRB04J0_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/IMG_20220122_120711_489_280x.webp"
        ],
        "videos": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Planter-33833.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Rose-3654.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Strawberries-109233.mp4"
        ]
      },
      "100002": {
        "price": "100",
        "discounted_price": "25",
        "sku": "sku 2",
        "country_of_origin": "India",
        "color": "0000FF",
        "measurements": "5 Ltr",
        "images": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/B3BsS4qtRO_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/BLWkRB04J0_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/IMG_20220122_120711_489_280x.webp"
        ],
        "videos": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Planter-33833.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Rose-3654.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Strawberries-109233.mp4"
        ]
      },
      "100003": {
        "price": "500",
        "discounted_price": "50",
        "sku": "sku 3",
        "country_of_origin": "India",
        "color": "FF0000",
        "measurements": "10 Ltr",
        "images": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/B3BsS4qtRO_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/BLWkRB04J0_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/IMG_20220122_120711_489_280x.webp"
        ],
        "videos": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Planter-33833.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Rose-3654.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Strawberries-109233.mp4"
        ]
      },
      "100004": {
        "price": "200",
        "discounted_price": "40",
        "sku": "sku 4",
        "country_of_origin": "India",
        "color": "FF0000",
        "measurements": "15 Ltr",
        "images": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/B3BsS4qtRO_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/BLWkRB04J0_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/IMG_20220122_120711_489_280x.webp"
        ],
        "videos": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Planter-33833.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Rose-3654.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Strawberries-109233.mp4"
        ]
      },
      "100005": {
        "price": "2000",
        "discounted_price": "400",
        "sku": "sku 5",
        "country_of_origin": "India",
        "color": "FFFF00",
        "measurements": "15 Ltr",
        "images": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/B3BsS4qtRO_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/BLWkRB04J0_280x.webp",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/inventory/IMG_20220122_120711_489_280x.webp"
        ],
        "videos": [
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Planter-33833.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Rose-3654.mp4",
            "https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/video/inventory/Strawberries-109233.mp4"
        ]
      }
    }
  }


