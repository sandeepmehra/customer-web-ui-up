import plantsImage from '../../assets/images/Categories_Plants.jpg';
import inputsImage from '../../assets/images/Categories_Inputs.jpg';
import othersImage from '../../assets/images/Categories_Others.png';
import plantersImage from '../../assets/images/Categories_Pots-Planters.jpg';
import seedsImage from '../../assets/images/Categories_Seeds.jpg';
import toolsImage from '../../assets/images/Categories_Tools.jpg';
import LogoImage from '../../assets/images/categories-logo.PNG';
const potslabel = "Pots"
const plantslabel = "Plants"
const inputlabel = "Inputs"
const seedslabel = "Seeds"
const toolslabel = "Tools"
const logolabel = "Urban Plants"
const otherslabel = "Others"

export const categories_data = [
    {
    id:1,
    name:"Plants",
    icon:plantsImage
    },
    {
        id:2,
        name:"Pots",
        icon:plantersImage
    },
    {
        id:3,
        name:"Seeds",
        icon:seedsImage
    },
    {
        id:4,
        name:"Tools",
        icon:toolsImage
    },
    {
        id:5,
        name:"Inputs",
        icon:inputsImage
    },
    {
        id:6,
        name:"Urban Plants",
        icon:LogoImage
    },

    {
        id:7,
        name:"Others",
        icon:othersImage
    }
    ]


export const categories_obj ={
    plantersImage,
    othersImage,
    LogoImage,
    plantsImage,
    inputsImage,
    seedsImage,
    toolsImage,
    potslabel,
    plantslabel,
    seedslabel,
    toolslabel,
    inputlabel,
    logolabel,
    otherslabel



}