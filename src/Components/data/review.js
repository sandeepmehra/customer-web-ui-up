export const reviewInfo = [{
    header:'Have you used this product?',
    label:'Your review should be about your experience with the product.'

},
{
    header:'Why review a product?',
    label:'Your valuable feedback will help fellow shoppers decide!'

},
{
    header:'How to review a product?',
    label:'Your review should include facts. An honest opinion is always appreciated. If you have an issue with the product or service please contact us from the help centre.'

}]