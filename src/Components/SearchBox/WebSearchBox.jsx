import React from 'react';
import SearchField from 'react-search-field';
import style from "./Search.module.css";

const WebSearchBar = () => {
    return (
        <div className={style.web_searchbar}>
            <SearchField
                placeholder="Search..."
                searchText=""
                classNames={style.searchbox}
            />
        </div>
    )
}

export default WebSearchBar;