import React from 'react';
import SearchField from 'react-search-field';
import style from "./Search.module.css";
const MobileSearchBar = () => {
    return (
        <div className={style.mobile_searchbar}>
            <SearchField
                placeholder="Search Mobile..."
                searchText=""
                classNames={style.searchbox_mobile}
            />
        </div>
    )
}

export default MobileSearchBar;