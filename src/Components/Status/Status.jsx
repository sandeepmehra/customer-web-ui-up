import "./Status.styles.css";

const DisplayStatus = ({status}) => {
    let orderStatus = ['Rejected', 'Placed', 'Shipped', 'Delivered'];
    let indexStatus;
    if(status !== undefined){
        indexStatus = orderStatus.findIndex(item => item === status);
    }

    const displayStatusMobile = () => (
        <div className='status-mobile'>
             <div className="steps status_steps">
                <div className={ -1 || 0 <= indexStatus ?`step status_step completed`: 'step status_step'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Ordered</h4>
                        <h4 className='step-title'>Monday Sept 20th '21</h4>
                    </div>
                </div>
                <div className={1 <= indexStatus ?`step status_step completed`: 'step status_step'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Placed</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div> 
                </div>
                <div className={2 <= indexStatus ?`step status_step completed`: 'step status_step'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Shipped</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div> 
                </div>
                <div className={3 <= indexStatus ?`step status_step completed`: 'step status_step'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Delivered</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div>
                </div>
            </div>
        </div >
    )    
    const displayStatusWeb = () => (
            <div className="status-web">
            <div className="steps status_container">
                <div className={ -1 || 0 <= indexStatus ?`step status_card completed`: 'step status_card'}>
                    {/* completed */}
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Ordered</h4>
                        <h4 className='step-title'>Monday Sept 20th '21</h4>
                    </div>
                </div>
                <div className={1 <= indexStatus ?`step status_card completed`: 'step status_card'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Placed</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div> 
                </div>
                <div className={2 <= indexStatus ?`step status_card completed`: 'step status_card'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Shipped</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div> 
                </div>
                <div className={3 <= indexStatus ?`step status_card completed`: 'step status_card'}>
                    <div className="step-icon-wrap">
                        <div className="step-icon"></div>
                    </div>
                    <div>
                        <h4 className="step-title">Delivered</h4>
                        <h4 className="step-title">Expected by Wednesday 22nd Sept'21</h4>
                    </div>
                </div>
            </div>
            </div>
        )
    return (
        <>
            {displayStatusWeb()}
            {displayStatusMobile()}
        </>

    )
}


export default DisplayStatus;