import React from "react";
import style from"./Loader.module.css";
import ReactLoading from "react-loading";

const Loader = () => {
  return (
    <div className={style.loading}>
        <ReactLoading type="spinningBubbles" color="#1f553e" height={100} width={50} />
    </div>
  );
};

export default Loader;