import React, { useState } from 'react';
import { AiFillStar } from 'react-icons/ai';
import Next from '../../assets/images/product/next.svg';
import Prev from '../../assets/images/product/prev.svg';
import Progressbar from './Progressbar';
import style from './Tabs.module.css';
const TabsCard = ({productDescription}) => {
    const [currentTab, setCurrentTab] = useState('tab1');
    const [tabIndex, setTabIndex] = useState(0);
    const [touchPosition, setTouchPosition] = useState(null);
    const tabList = [
        {
        name: 'tab1',
        label: 'Highlights',
        content: (
            <div className={style.tab_content}>
            <h2>Tab content 1</h2>
            <p>Here is your tab content. You can separate this as a component.</p>
            <p>Lorem ipsum dolor sit amet ...</p>
            </div>
        )
        },
        {
        name: 'tab2',
        label: 'Product Information',
        content: (
            <div className={style.tab_content} dangerouslySetInnerHTML={{__html: productDescription.replace(/<br\s*\\?>/g, "\r\n").replace(/\r?\\n|\r/g, " ")}}></div>
        )
        },
        {
            name: 'tab3',
            label: 'Seller Information',
            content: (
                <div className={style.tab_content}>
                    <div><b>Seller:</b><span>Urban Plants</span></div>
                    <p> 
                        consectetur adipiscing elit. Ut dolor pellentesque cras sed ipsum urna vulputate proin sagittis. Egestas turpis sit ultrices arcu morbi bibendum amet placerat enim. Sed dolor tellus nec dignissim et. Nisi sodales ipsum ultricies cursus feugiat nisl etiam nam ultrices. A semper cursus diam magna. Massa, venenatis, molestie dignissim aliquam eget felis, blandit est, sit. Pharetra et enim eget donec tincidunt turpis ac eleifend.
                    </p>
                </div>
            )
        },
        {
            name: 'tab4',
            label: 'Reviews',
            content: (
                <div className={style.tab_content}>
                    <div className={style.review_card}>
                        <div className={style.review_avg_card}>
                            <div className={style.review_avg_val}>4.5  <AiFillStar /></div>
                            <div className={style.review_avg_des}>
                                <p>4,000 Ratings</p>
                                <p>&</p>
                                <p>2,000 Reviews</p>
                            </div>  
                        </div>
                        <div className={style.review_des}>
                            <div className={style.review_line}>
                                <span className={style.star}><div>5</div><AiFillStar /></span>
                                <span className={style.progress_line}><Progressbar bgcolor="#26A541" progress='60'  height={5} /></span>
                                <span className={style.review_count}>2,500</span>    
                            </div>
                            <div className={style.review_line}>
                                <span className={style.star}><div>4</div><AiFillStar /></span>
                                <span className={style.progress_line}><Progressbar bgcolor="#26A541" progress='45'  height={5} /></span>
                                <span className={style.review_count}>1,000</span> 
                            </div>
                            <div className={style.review_line}>
                                <span className={style.star}><div>3</div><AiFillStar /></span>
                                <span className={style.progress_line}><Progressbar bgcolor="#26A541" progress='30'  height={5} /></span>
                                <span className={style.review_count}>300</span> 
                            </div>
                            <div className={style.review_line}>
                                <span className={style.star}><div>2</div><AiFillStar /></span>
                                <span className={style.progress_line}><Progressbar bgcolor="#E0AA07" progress='15'  height={5} /></span>
                                <span className={style.review_count}>150</span> 
                            </div>
                            <div className={style.review_line}>
                                <span className={style.star}><div>1</div><AiFillStar /></span>
                                <span className={style.progress_line}><Progressbar bgcolor="#FF4E3F" progress='5'  height={5} /></span>
                                <span className={style.review_count}>50</span> 
                            </div>
                        </div>
                    </div>
                </div> 
            )
        }
    ];
    const next = () => {
        setTabIndex(prevState => prevState === tabList.length - 1 ? 0 : prevState + 1)
    }
    
    const prev = () => {
        setTabIndex(prevState => prevState === 0 ? tabList.length - 1 : prevState - 1)
    }
    const handleTouchStart = (e) => {
        const touchDown = e.touches[0].clientX
        setTouchPosition(touchDown)
    }
    const handleTouchMove = (e) => {
        const touchDown = touchPosition
        if(touchDown === null) {
            return
        }
        const currentTouch = e.touches[0].clientX
        const diff = touchDown - currentTouch
    
        if (diff > 5) {
            next()
        }
    
        if (diff < -5) {
            prev()
        }
        setTouchPosition(null)
    }
    let iconStyles = { color: "#fff", fontSize: "1.5em" };
    return (
        <>
            <div className={style.tabs_web_card}>
                <div className={style.tabs}>
                        
                    {
                        tabList.map((tab, i) => (
                            <div 
                                key={i}
                                onClick={() => setCurrentTab(tab.name)} 
                                className={(tab.name === currentTab) ? style.active : ''}>
                                    <b className={style.left_curve}></b>
                                    <b className={style.right_curve}></b>
                                    {tab.label}
                            </div>
                        ))
                    }
                </div>
                {
                    tabList.map((tab, i) => {
                    if(tab.name === currentTab) {
                        return <div key={i}>{tab.content}</div>;
                    } else {
                        return null;
                    }
                    })
                }
            </div>
            <div className={style.tabs_mobile_view_card}>
                <div className={style.slideshow}>
                    {tabList.map((tab, i)=>{
                        if(i === tabIndex) {
                            return <div className={style.tabs_header} key={i}>
                                    <div className={style.prev} onClick={() => {setTabIndex(prevState => prevState === 0 ? tabList.length - 1 : prevState - 1)}}>
                                        <img src={Prev} alt="prev" style={iconStyles} />
                                    </div>
                                    <div className={style.tabs_header_label}>{tab.label}</div>
                                    <div className={style.next} onClick={() => {setTabIndex(prevState => prevState === tabList.length - 1 ? 0 : prevState + 1)}}>
                                        <img src={Next} alt="next" style={iconStyles} />
                                    </div>
                                </div>
                        } else {
                            return null;
                        }
                        })
                    }
                    <div className={style.slideshowSlider} style={{transform: `translate3d(${-tabIndex * 0}%, 0, 0)` }}>
                    {
                        tabList.map((tab, i) => {
                        if(i === tabIndex) {
                            return <div key={i} onTouchStart={handleTouchStart} onTouchMove={handleTouchMove}>{tab.content}</div>;
                        } else {
                            return null;
                        }
                        })
                    }
                    </div>
                </div>
            </div>
        </>
    )
}
export default TabsCard;